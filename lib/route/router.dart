import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:SmartLockerApp/pages/booking/booking_locker_room_select/locker_room_select.dart';
import 'package:SmartLockerApp/pages/pages.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/transfer_update_page.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case routePath.PrepareDataRoute:
      return MaterialPageRoute(builder: (context) => PrepareDataPage());
      break;
    case routePath.ProfileRoute:
      return MaterialPageRoute(builder: (context) => ProfilePage());
      break;
    case routePath.SearchRoute:
      return MaterialPageRoute(builder: (context) => SearchingPage());
      break;
    case routePath.MainRoute:
      return MaterialPageRoute(builder: (context) => MainPage());
      break;
    case routePath.LoginRoute:
      return MaterialPageRoute(builder: (context) => LoginPage());
      break;
    case routePath.ServiCeRateRoute:
      return MaterialPageRoute(builder: (context) => ServiceRatePage());
      break;
    case routePath.ContractRoute:
      return MaterialPageRoute(builder: (context) => ContractPage());
      break;
    case routePath.SearchingOptionRoute:
      return MaterialPageRoute(builder: (context) => SearchingOptionPage());
      break;
    case routePath.TextSearchRoute:
      return MaterialPageRoute(builder: (context) => TextSearchPage());
      break;
    case routePath.PaymentSelectRoute:
      return MaterialPageRoute(
          builder: (context) => PaymentSelectPage(
                bookingDetail: settings.arguments,
              ));
      break;
    case routePath.BookingLockerRoomRoute:
      return MaterialPageRoute(
          builder: (context) => LockerRoomSelect(
                lockerChoice: settings.arguments,
              ));
      break;
    case routePath.RegisterRoute:
      return MaterialPageRoute(builder: (context) => RegisterPage());
      break;
    case routePath.LocationDetailRoute:
      return MaterialPageRoute(
          builder: (context) => LocationDetailPage(
                objLocate: settings.arguments,
              ));
      break;
    case routePath.FinishBookingRequestRoute:
      return MaterialPageRoute(
          builder: (context) => FinishBookingDetailPage(
                bookingDetail: settings.arguments,
              ));
      break;
    case routePath.BookingDetailRoute:
      return MaterialPageRoute(
          builder: (context) => BookingDetailPage(
                bookingdetail: settings.arguments,
              ));
      break;
    case routePath.PaymentInfoAddRoute:
      return MaterialPageRoute(builder: (context) => PaymentInfoAddPage());
      break;
    case routePath.ResultRoute:
      return MaterialPageRoute(
          builder: (context) => ResultPage(
                bookingModel: settings.arguments,
              ));
      break;
    case routePath.TransferCompleteRoute:
      return MaterialPageRoute(
          builder: (context) => TransferCompletePage(
                transferDetailModel: settings.arguments,
              ));
    case routePath.TransferRoute:
      return MaterialPageRoute(
          builder: (context) => TransferFormPage(
                bookingHistory: settings.arguments,
              ));
      break;
    case routePath.TransferUpdateRoute:
      return MaterialPageRoute(
          builder: (context) => TransferUpdatePage(
                transferUpdateModel: settings.arguments,
              ));
      break;
    default:
      return MaterialPageRoute(
        builder: (context) => Scaffold(
          body: Center(
            child: Text('No path for ${settings.name}'),
          ),
        ),
      );
      break;
  }
}
