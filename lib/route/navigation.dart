import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:flutter/material.dart';

class NavigationRoute {
  static GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  static String route = routePath.PrepareDataRoute;
  static Future<void> navigateTo(String routeName, {dynamic arguments}) {
    route = routeName;
    return navigatorKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  static void navigateAndRemoveUntil(String routeName,
      {String routeEnd, dynamic arguments}) {
    route = routeName;
    navigatorKey.currentState.pushNamedAndRemoveUntil(
        routeName, (route) => false,
        arguments: arguments);
  }

  static Future<void> navigateReplacement(String routeName,
      {dynamic arguments}) {
    route = routeName;
    return navigatorKey.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  static void goBack() {
    return navigatorKey.currentState.pop();
  }
}
