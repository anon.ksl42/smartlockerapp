import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bl/event_type_bl.dart';
import 'package:SmartLockerApp/bussiness-logic/bl/notification_bl.dart';
import 'package:SmartLockerApp/bussiness-logic/bl/rate_type_bl.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/component/my_alert.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/data/localstorage/account_localstorage.dart';

class PrepareDataBussinessLogic {
  MyAlert myAlert = MyAlert();
  AccountLocalStorage accountLocalStorage = AccountLocalStorage();
  EventTypeBussinessLogic eventTypeBussinessLogic = EventTypeBussinessLogic();
  RateTypeBussinessLogic rateTypeBussinessLogic = RateTypeBussinessLogic();
  NotificationBussinessLogic notificationBussinessLogic =
      NotificationBussinessLogic();

  Future<void> prepareData() async {
    try {
      await accountLocalStorage.init();
      var eventTypeList =
          await eventTypeBussinessLogic.getAllEventType().catchError((error) {
        errorResponse(error);
      });
      var rateTypeList =
          await rateTypeBussinessLogic.getAllRateType().catchError((error) {
        errorResponse(error);
      });

      EventType.initial(eventTypeList);
      RateType.initial(rateTypeList);

      await Future.delayed(Duration(seconds: 2));
      if (accountLocalStorage.getAccountId() == null) {
        throw UnauthorisedException();
      }
      await notificationBussinessLogic.getAllNotificationByAccountId(
          isInitial: true);
    } on TimeoutException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } on UnauthorisedException {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on BadRequestException {
      return Future.error(APIStatus.BAD_REQUEST);
    } on ConflitException {
      return Future.error(APIStatus.CONFLICT);
    } on Exception catch (e) {
      print(e);
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  void errorResponse(error) {
    if (error == APIStatus.UNAUTHRIZE) {
      throw UnauthorisedException();
    } else if (error == APIStatus.CONFLICT) {
      throw ConflitException();
    } else if (error == APIStatus.NOCONTENT) {
      throw NoContentException();
    } else if (error == APIStatus.BAD_REQUEST) {
      throw BadRequestException();
    } else {
      throw InternalServerException();
    }
  }

}
