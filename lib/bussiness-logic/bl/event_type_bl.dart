import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bl/base_bussiness_logic.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/models/event_type_model.dart';
import 'package:SmartLockerApp/repositories/event_type_repository.dart';

class EventTypeBussinessLogic extends BaseBussinessLogic {
  EventTypeRepository _eventTypeRepository = EventTypeRepository();
  Future<List<EventTypeModel>> getAllEventType() async {
    try {
      var response = await _eventTypeRepository.getAllEventType();
      if (response.statusCode != 200) {
        errorResponse(response);
      }
      Iterable<dynamic> resultList = getContentList(response);
      List<EventTypeModel> eventTypeList =
          resultList.map((model) => EventTypeModel.fromJson(model)).toList();
      return eventTypeList;
    } on UnauthorisedException {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on ConflitException {
      return Future.error(APIStatus.CONFLICT);
    } on BadRequestException {
      return Future.error(APIStatus.BAD_REQUEST);
    } on InternalServerException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } on TimeoutException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } catch (e) {
      print(e);
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
