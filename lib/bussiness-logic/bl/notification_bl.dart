import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bl/base_bussiness_logic.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/data/notification.dart';
import 'package:SmartLockerApp/models/notification_model.dart';
import 'package:SmartLockerApp/repositories/notification_repository.dart';

class NotificationBussinessLogic extends BaseBussinessLogic {
  NotificationRepository _notificationRepository = NotificationRepository();
  Future<void> getAllNotificationByAccountId({bool isInitial = false}) async {
    try {
      if (MyNotification.isUpdate || isInitial) {
        var response = await _notificationRepository.getAllNotificationByAccountId();
      if (response.statusCode != 200) {
        errorResponse(response);
      }
      Iterable<dynamic> resultList = getContentList(response);
      List<NotificationModel> notificationList =
          resultList.map((model) => NotificationModel.fromJson(model)).toList();
        MyNotification.listAllNoti = notificationList;
        MyNotification.isUpdate = false;
      }
    } on UnauthorisedException {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on ConflitException {
      return Future.error(APIStatus.CONFLICT);
    } on InternalServerException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } on BadRequestException {
      return Future.error(APIStatus.BAD_REQUEST);
    } on TimeoutException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } catch (e) {
      print(e);
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
