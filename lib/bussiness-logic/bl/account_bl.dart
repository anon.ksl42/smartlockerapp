import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bl/base_bussiness_logic.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/component/my_alert.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/textinput_pattern.dart';
import 'package:SmartLockerApp/data/localstorage/account_localstorage.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/repositories/account_repository.dart';
import 'package:flutter/material.dart';

class AccountBussinessLogic extends BaseBussinessLogic {
  MyAlert _myAlert = MyAlert();
  AccountRepository _accountRepository = AccountRepository();
  AccountLocalStorage _accountLocalStorage = AccountLocalStorage();

  Future<void> loginAccount({@required AccountModel loginForm}) async {
    try {
      var response =
          await _accountRepository.loginAccount(loginForm: loginForm);
      if (response.statusCode != 200) {
        errorResponse(response);
      }
      var result = getContent(response);
      AccountModel accountResult = AccountModel.formJson(result);

      await _accountLocalStorage.init();
      _accountLocalStorage.setToken(accountResult.token);
      _accountLocalStorage.setFirstName(accountResult.firstName);
      _accountLocalStorage.setLastName(accountResult.lastName);
      _accountLocalStorage.setAccountCode(accountResult.accountCode);
      _accountLocalStorage.setAccountId(accountResult.accountId);
      
    } on UnauthorisedException {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on ConflitException {
      return Future.error(APIStatus.CONFLICT);
    } on BadRequestException {
      return Future.error(APIStatus.BAD_REQUEST);
    } on InternalServerException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } on TimeoutException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } catch (e) {
      print(e);
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  void loginErrorResponse(APIStatus apiStatus) {
    if (apiStatus == APIStatus.UNAUTHRIZE) {
      _myAlert.errorAlert(wording: 'Email หรือ Password ไม่ถูกต้อง');
    } else if (apiStatus == APIStatus.BAD_REQUEST) {
      _myAlert.errorAlert(wording: 'เกิดข้อผิดพลาดเกี่ยวกันระบบ');
    } else {
      _myAlert.errorAlert(
          wording: 'ไม่สามารถใช้งานได้ในขณะนี้ ขออภัยในความไม่สะดวก');
    }
  }

  bool isEmailPattern({String email}) {
    return TextInputPattern.emailPattern.hasMatch(email.replaceAll(' ', ''));
  }

  bool isPasswordPattern({String password}) {
    password = password.replaceAll(' ', '');
    return password.isNotEmpty &&
        password.toString().length >= 6 &&
        password.toString().length <= 18;
  }
}
