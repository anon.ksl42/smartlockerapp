import 'dart:convert';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/responseheader.dart';
import 'package:http/http.dart';

class BaseBussinessLogic {
  dynamic getContent(Response response) {
    ResponseHeader responseHeader =
        ResponseHeader.formJson(json.decode(response.body));
    return responseHeader.content;
  }

  Iterable<dynamic> getContentList(Response response) {
    ResponseHeader responseHeader =
        ResponseHeader.formJson(json.decode(response.body));
    Iterable contentList = responseHeader.content as List;
    return contentList;
  }

  void errorResponse(Response response) {
    if (response.statusCode == 401) {
      throw UnauthorisedException();
    } else if (response.statusCode == 409) {
      throw ConflitException();
    } else if (response.statusCode == 204) {
      throw NoContentException();
    } else if (response.statusCode == 400) {
      throw BadRequestException();
    } else {
      throw InternalServerException();
    }
  }
}
