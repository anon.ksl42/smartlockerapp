import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bl/base_bussiness_logic.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/models/booking_type_model.dart';
import 'package:SmartLockerApp/repositories/rate_type_repository.dart';

class RateTypeBussinessLogic extends BaseBussinessLogic {
  RateTypeRepository _rateTypeRepository = RateTypeRepository();
  Future<List<RateTypeModel>> getAllRateType() async {
    try {
      var response = await _rateTypeRepository.getAllRateType();
      if (response.statusCode != 200) {
        errorResponse(response);
      }
      Iterable<dynamic> resultList = getContentList(response);
      List<RateTypeModel> rateTypeList =
          resultList.map((model) => RateTypeModel.fromJson(model)).toList();
      return rateTypeList;
    } on UnauthorisedException {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on ConflitException {
      return Future.error(APIStatus.CONFLICT);
    } on InternalServerException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } on BadRequestException {
      return Future.error(APIStatus.BAD_REQUEST);
    } on TimeoutException {
      return Future.error(APIStatus.SYETEM_ERROR);
    } catch (e) {
      print(e);
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
