import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/schedule/validate_auto.dart';
import 'package:SmartLockerApp/services/service.dart';
import 'package:flutter/material.dart';

class Booking {
  Future<List<BookingModel>> getAllBookingByAccountId() async {
    try {
      List<BookingModel> bookingList =
          await BookingService.getAllBookingByAccountId();
      var refreshKey = GlobalKey<RefreshIndicatorState>();
      refreshKey.currentState?.show(atTop: false);

      for (BookingModel booking in bookingList) {
        if (booking.status == "P" &&
            ValidateBookingAuto.timerList[booking.bookingId] == null) {
          ValidateBookingAuto.addUse30MinutesLeftNotification(booking);
        }
      }
      return bookingList;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  Future<BookingModel> addBooking(BookingModel bookingDetail) async {
    try {
      BookingModel bookingAfterAdd =
          await BookingService.addBooking(bookingDetail);
      return bookingAfterAdd;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  Future<BookingModel> updateBooking(int bookingId, String status) async {
    try {
      BookingModel updateBooking =
          BookingModel(bookingId: bookingId, status: status);
      BookingModel bookingAfterUpdate =
          await BookingService.updateBooking(updateBooking);

      return bookingAfterUpdate;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
