import 'dart:async';

import 'package:SmartLockerApp/data/notification.dart';
import 'package:SmartLockerApp/services/service.dart';

class NotificationData {
  Future<int> updateNotification({bool initial = false}) async {
      if (MyNotification.isUpdate || initial) {
        var notiResult =
            await NotificationService.getAllNotificationByAccountId(1, 20);
        MyNotification.listAllNoti = notiResult;
        MyNotification.isUpdate = false;
      }
      return 200;
  }
}
