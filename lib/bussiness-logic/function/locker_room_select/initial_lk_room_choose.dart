import 'package:SmartLockerApp/models/models.dart';

Map<int, bool> initialIsLockerRoomChoose(List<LockerRoomModel> lockerRoomList) {
  Map<int, bool> isLockerRoomChoose = new Map<int, bool>();
    for (LockerRoomModel lockerRoom in lockerRoomList) {
      isLockerRoomChoose[lockerRoom.lkRoomId] = false;
    }
    return isLockerRoomChoose;
  }