import 'package:SmartLockerApp/component/error-alert.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/constant/constant.dart' as routePath;

void errorFunction(error) {
  if (error == 401) {
    errorAlert(
        wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
        function: () =>
            NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
  } else if (error == 408) {
    errorAlert(
        wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
        function: () =>
            NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
  } else {
    errorAlert(
        wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        function: () =>
            NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
  }
}
