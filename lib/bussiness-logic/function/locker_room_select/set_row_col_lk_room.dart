import 'package:SmartLockerApp/models/models.dart';

List<LockerRoomModel> setIndexLockerRoom(List<LockerRoomModel> lockerRoomList) {
  List<LockerRoomModel> lockerRoomListForGenerate = [];

  //get row max
  lockerRoomList.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
  int getRowMax = lockerRoomList.last.rowPosition;

  //create array to check can create empty block in each column
  lockerRoomList.sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
  List<int> addRowIndex = List.filled(lockerRoomList.last.columnPosition + 1 , 0) ;
  // List<int> addRowIndex =  new List(lockerRoomList.last.columnPosition + 1);

  //for loop each row
  for (int row = 0; row <= getRowMax; row++) {
    //for loop each column
    for (int col = 0; col <= lockerRoomList.last.columnPosition; col++) {
      //sort  by column
      lockerRoomList
          .sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
      LockerRoomModel result;
      try {
        //find locker room in col and row position
        result = lockerRoomList.firstWhere((element) =>
            element.columnPosition == col && element.rowPosition == row);
      } catch (e) {
        //if not found it is error than set null
        result = null;
      }

      if (result == null && (addRowIndex[col] ?? 0) == 0) {
        //if not found and no value addRow in this col so it can generate empty block
        lockerRoomListForGenerate
            .add(new LockerRoomModel(columnPosition: col, rowPosition: row));
        continue;
      } else if (result != null) {
        //if found add block to generate
        if ((addRowIndex[col] ?? 0) > 0) {
          addRowIndex[col]--;
        }
        lockerRoomListForGenerate.add(result);
      } else {
        //if not found and have more than 0 addRow in this col that continue to next col
        if ((addRowIndex[col] ?? 0) > 0) {
          addRowIndex[col]--;
        }
        continue;
      }
      //check to add row index
      if (result.lkSizeName.toUpperCase() == 'M') {
        try {
          addRowIndex[col] += 1;
        } catch (_) {
          addRowIndex[col] = 1;
        }
      } else if (result.lkSizeName.toUpperCase() == 'L') {
        try {
          addRowIndex[col] += 2;
        } catch (_) {
          addRowIndex[col] = 2;
        }
      }
    }
  }
  return lockerRoomListForGenerate;
  // for (int i = 0; i <= getRowMax; i++) {
  //   List<LockerRoomModel> lockerOnlyRow =
  //       lockerRoomList.where((lk) => lk.rowPosition == i).toList();

  //   if (lockerOnlyRow.isEmpty) continue;

  //   lockerOnlyRow
  //       .sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
  //   int firstPosition = lockerOnlyRow.first.columnPosition;
  //   for (int j = 0; j < lockerOnlyRow.last.columnPosition; j++) {

  //     lockerOnlyRow[j].rowPosition = lockerOnlyRow[j].rowPosition +
  //         ((addRowIndex[j] ?? 0) > 0 ? addRowIndex[firstPosition + j]-- : 0);
  //     lockerRoomListForGenerate.add(lockerOnlyRow[j]);

  //     //check to add row index
  //     if (lockerOnlyRow[j].lkSizeName.toUpperCase() == 'M') {
  //       try {
  //         addRowIndex[firstPosition + j] += 1;
  //       } catch (_) {
  //         addRowIndex[firstPosition + j] = 1;
  //       }
  //     } else if (lockerOnlyRow[j].lkSizeName.toUpperCase() == 'L') {
  //       try {
  //         addRowIndex[firstPosition + j] += 2;
  //       } catch (_) {
  //         addRowIndex[firstPosition + j] = 2;
  //       }
  //     }
  //   }
  // }
}
