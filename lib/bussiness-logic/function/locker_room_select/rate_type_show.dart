import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/service.dart';

class RateTypeShow {
  List<String> typeListForShow = [];
  Map<String, int> rateTypeForData = new Map<String, int>();

  Future<void> initialType() async {
    try {
      List<RateTypeModel> rateTypeList =
          await ServiceRateService.getAllRateType();
      for (RateTypeModel rateType in rateTypeList) {
        typeListForShow.add(rateType.typeName);
        rateTypeForData[rateType.typeName] = rateType.typeId;
      }
    } on TimeoutException catch (_) {
      return Future.error(408);
    } on UnauthorisedException catch (_) {
      return Future.error(401);
    } on Exception catch (_) {
      return Future.error(500);
    }
  }
}