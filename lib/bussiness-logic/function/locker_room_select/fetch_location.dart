import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/service.dart';

Future<LocationModel> fetchLocation(int locationId) async {
  try {
      return await LocationService.getLocationByLocationId(locationId);
    } on TimeoutException catch (_) {
      return Future.error(408);
      // errorAlert(
      //     wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    } on UnauthorisedException catch (_) {
      return Future.error(401);
      // errorAlert(
      //     wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
    } on Exception catch (_) {
      return Future.error(500);
      // errorAlert(
      //     wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    }
  }
