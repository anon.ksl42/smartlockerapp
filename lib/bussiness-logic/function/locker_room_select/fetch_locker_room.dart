import 'dart:async';

import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/locker_service.dart';

Future<List<LockerRoomModel>> fetchLockerRoom(int lockerId) async {
  try {
      return await LockerService.getAllLockerRoomByLockerId(lockerId);
    } on TimeoutException catch (_) {
      return Future.error(408);
      // errorAlert(
      //     wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    } on UnauthorisedException catch (_) {
      return Future.error(401);
      // errorAlert(
      //     wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
    } on Exception catch (_) {
      return Future.error(500);
      // errorAlert(
      //     wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
      //     function: () =>
      //         NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    }
  }
