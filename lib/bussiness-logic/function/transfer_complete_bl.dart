import 'dart:async';

import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/schedule/validate_auto.dart';
import 'package:SmartLockerApp/services/service.dart';

class TransferComplete {
  bool isCard;
  bool isLoading = true;
  DateTime createDate;
  bool isError = false;

  String postDetail;
  String titleName;
  String titleHeader;

  Future<void> updateTransferRequest(
      TransferDetailModel transferDetailModel) async {
    try {
      isCard = transferDetailModel.status == 'NR' ? false : true;
      BookingModel transferResult;
      BookingModel bookingModel = BookingModel(
          accountId: transferDetailModel.accountId,
          bookingId: transferDetailModel.bookingId,
          status: transferDetailModel.status ?? '');
      if (transferDetailModel.status == "WT") {
        transferResult = await BookingService.transfer(bookingModel);
        ValidateBookingAuto.transferTimer(transferDetailModel.bookingId);
      } else if (transferDetailModel.status == "CT") {
        transferResult = await BookingService.cancelTransfer(bookingModel);
      } else if (transferDetailModel.status == "R" ||
          transferDetailModel.status == 'NR') {
        transferResult = await BookingService.updateBooking(bookingModel);
      }
      createDate = transferResult.updateDate ?? DateTime.now();
      isLoading = false;
    } on TimeoutException catch (_) {
      isLoading = false;
      isError = true;
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      isLoading = false;
      isError = true;
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      isLoading = false;
      isError = true;
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
