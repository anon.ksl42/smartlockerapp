import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/location_service.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class Location {
  Future<LocationModel> getLocationByLocationId(int locationId) async {
    try {
      LocationModel location =
          await LocationService.getLocationByLocationId(locationId);
      return location;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  Future<void> scanQR() async {
    var barcodeScanRes;
    Location location = Location();
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      int locationId = int.parse(barcodeScanRes);
      if (barcodeScanRes != "-1") {
        location.getLocationByLocationId(locationId).then((value) {
          if (value == null) return Future.error(APIStatus.NOCONTENT);
          NavigationRoute.navigateTo(LocationDetailRoute, arguments: value);
        }).catchError((error) => {
              if (error == APIStatus.UNAUTHRIZE)
                {
                  errorAlert(
                      wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                      function: () =>
                          NavigationRoute.navigateAndRemoveUntil(LoginRoute))
                }
              else if (error == APIStatus.TIME_OUT)
                {
                  errorAlert(
                    wording:
                        'ไม่สามารถอัพเดทข้อมูลได้เนื่องจากการเชื่อมต่อกับระบบผิดพลาด',
                  )
                }
              else if (error == APIStatus.SYETEM_ERROR)
                {
                  errorAlert(
                    wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                  )
                }
              else if (error == APIStatus.NOCONTENT)
                {
                  errorAlert(
                    wording: 'ข้อมูล QR CODE ไม่ถูกต้อง',
                  )
                }
            });
      }
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    } on FormatException {
      errorAlert(wording: 'ข้อมูล QR CODE ไม่ถูกต้อง');
    }
  }
}
