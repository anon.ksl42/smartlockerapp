import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/service.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Payment {
  List<PaymentInfoModel> bookingInfoList = [];
  Future<void> getAllPaymentByAccountId() async {
    try {
      var paymentInfoList = await PaymentService.getAllPaymentInfoByAccountId();
      bookingInfoList = paymentInfoList;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  Future<String> payment(PaymentInfoModel paymentInfoModel) async {
    try {
      return await PaymentService.payment(paymentInfoModel);
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  void paymentError(APIStatus error) {
    if (error == APIStatus.TIME_OUT) {
      errorAlert(wording: 'ไม่สามารถชำระเงินได้เนื่องจากขาดการเชื่อมต่อ');
    } else if (error == APIStatus.SYETEM_ERROR) {
      errorAlert(wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้');
    } else if (error == APIStatus.UNAUTHRIZE) {
      errorAlert(
          wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
    }
  }
}

class PaymentInfoValidate {
  TextEditingController nameController = new TextEditingController();
  TextEditingController cardNumController = new TextEditingController();
  TextEditingController expiredController = new TextEditingController();
  TextEditingController cvvController = new TextEditingController();
  CardCharacter character = CardCharacter.mastercard;
  bool isNameInvalid = false;
  bool isCardNumInvalid = false;
  bool isExpiredInvalid = false;
  bool isCvvInvalid = false;
  bool isNameNotPattern = false;
  bool isCardNumNotPattern = false;
  bool isExpiredNotPattern = false;
  bool isCvvNotPattern = false;

  bool isAllCardDetailVerify() {
    isNameInvalid = nameController.text.isEmpty;
    isCardNumInvalid = cardNumController.text.isEmpty;
    isExpiredInvalid = expiredController.text.isEmpty;
    isCvvInvalid = cvvController.text.isEmpty;
    isNameNotPattern = false;
    if (character == CardCharacter.mastercard) {
      isCardNumNotPattern =
          !TextInputPattern.mastercardPattern.hasMatch(cardNumController.text);
    } else if (character == CardCharacter.visa) {
      isCardNumNotPattern =
          !TextInputPattern.visaPattern.hasMatch(cardNumController.text);
    }
    isExpiredNotPattern =
        !TextInputPattern.expDatePattern.hasMatch(expiredController.text);
    isCvvNotPattern = !(cvvController.text.length == 3 || cvvController.text.length == 4); 
    return !(isNameInvalid ||
        isCardNumInvalid ||
        isExpiredInvalid ||
        isCvvInvalid ||
        isNameNotPattern ||
        isCardNumNotPattern ||
        isExpiredNotPattern ||
        isCvvNotPattern);
  }

  Future<void> addPaymentInfo() async {
    try {
      var dateConvert =
          DateFormat("MM/yyyy", "en_US").parse(expiredController.text);
      dateConvert = DateFormat("dd/MM/yyyy")
          .parse(DateFormat("dd/MM/yyyy").format(dateConvert));
      PaymentInfoModel paymentInfoModel = new PaymentInfoModel(
          cardName: nameController.text,
          cardTypeId: character == CardCharacter.mastercard ? 1 : 3,
          expDate: dateConvert,
          cardNumber: cardNumController.text,
          accountId: AccountGlobal.getAccountId());
      await PaymentService.addPaymentInfo(paymentInfoModel);
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
