import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/service.dart';
import 'package:flutter/material.dart';

class FinishBookingBL {
  Future<ServiceRateModel> getServiceChargeByLockerRoomIdAndTypeId(
      {@required int lkRoomId, @required int typeId}) async {
    try {
      ServiceRateModel serviceRate =
          await ServiceRateService.getServiceChargeByLockerRoomIdAndTypeId(
              lkRoomId: lkRoomId, typeId: typeId);
      return serviceRate;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
