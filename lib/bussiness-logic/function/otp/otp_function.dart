import 'package:firebase_auth/firebase_auth.dart';

FirebaseAuth _auth = FirebaseAuth.instance;
String _verificationId;
bool statusBool;
requestVerifyCode(phoneController) {
  _auth.verifyPhoneNumber(
      phoneNumber: phoneController,
      timeout: const Duration(seconds: 5),
      verificationCompleted: (firebaseUser) {
        //
      },
      verificationFailed: (error) {
        print(
            'Phone number verification failed. Code: ${error.code}. Message: ${error.message}');
      },
      codeSent: (verificationId, [forceResendingToken]) {
        _verificationId = verificationId;
        print(_verificationId);
        return _verificationId;
      },
      codeAutoRetrievalTimeout: (verificationId) {
        //
      });
}

Future<bool> verifyPhone(String smsController) async {
  try {
    await _auth
        .signInWithCredential(PhoneAuthProvider.credential(
            verificationId: _verificationId, smsCode: smsController))
        .then((value) => statusBool = true);
    statusBool = true;
    return statusBool;
  } catch (e) {
    statusBool = false;
    return statusBool;
  }
}
