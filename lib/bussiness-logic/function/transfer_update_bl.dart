import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/services/account_service.dart';

class TransferUpdate {
  AccountModel transfersAccount = AccountModel();
  String titleName;
  bool isLoading = true;
  bool isError = true;

  TransferUpdate(String status) {
    if (status == 'C') {
      titleName = 'ยกเลิกการโอนสิทธิ์';
    } else if (status == 'A') {
      titleName = 'ยืนยันการรับสิทธิ';
    }
  }

  Future<void> getBookingByBookingId(String status, int bookingId) async {
    try {
      if (status == 'C') {
        transfersAccount = await AccountService.getAccountByResBookingId(
            resBookingId: bookingId);
      } else if (status == 'A') {
        transfersAccount = await AccountService.getAccountByTransfersBookingId(
            tranBookingId: bookingId);
      }
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
