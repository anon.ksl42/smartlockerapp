import 'package:SmartLockerApp/pages/main/dashboard/dashboard-page.dart';
import 'package:SmartLockerApp/pages/main/home/home_page/home_page.dart';
import 'package:SmartLockerApp/pages/main/my_booking/my_booking_page.dart';
import 'package:SmartLockerApp/pages/main/notification/notification_page.dart';
import 'package:SmartLockerApp/pages/main/searching/searching_page.dart';
import 'package:flutter/widgets.dart';

class MainNav {
  Widget pageNav(int selectedIndex) {
    switch (selectedIndex) {
      case 0:
        return HomePage();
        break;
      case 1:
        return MyBookingPage();
        break;
      case 2:
        return SearchingPage();
        break;
      case 3:
        return NotificationPage();
        break;
      case 4:
        return DashBoardPage();
        break;

      default:
        return Center(
          child: Text('404 Page Not Found'),
        );
    }
  }
}
