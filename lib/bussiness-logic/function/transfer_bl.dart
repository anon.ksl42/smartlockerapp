import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/account_service.dart';

class Transfer {
  bool isSearchComplete = false;
  bool isSearchNotFound = false;
  AccountModel account = new AccountModel();

  Future<void> searchUser(String accountCode) async {
    if (accountCode.isEmpty) {
      isSearchNotFound = true;
      isSearchComplete = false;
      return null;
    }

    try {
      String myAccountId = AccountGlobal.getAccountCode();
      if (accountCode == myAccountId) {
        return Future.error(APIStatus.CONFLICT);
      }
      account = await AccountService.getAcccountByAccountCode(
          accountCode: accountCode);
      if (account == null) {
        isSearchNotFound = true;
        isSearchComplete = false;
        return Future.error(APIStatus.NOCONTENT);
      }
      isSearchNotFound = false;
      isSearchComplete = true;
    } on TimeoutException catch (_) {
      account = null;
      isSearchNotFound = true;
      isSearchComplete = false;
      return Future.error(APIStatus.TIME_OUT);
    } on UnauthorisedException catch (_) {
      account = null;

      isSearchNotFound = true;
      isSearchComplete = false;
      return Future.error(APIStatus.UNAUTHRIZE);
    } on Exception catch (_) {
      account = null;

      isSearchNotFound = true;
      isSearchComplete = false;
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
