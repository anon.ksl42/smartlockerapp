import 'dart:async';

import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/error-alert.dart';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/global/global.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/account_service.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class AccountValidate {
  bool isIdCitizenValidateNotPattern(String idCitizen) {
    idCitizen = idCitizen.replaceAll(' ', '');
    if (idCitizen == null || idCitizen.length != 13) {
      return true;
    }
    var i = 0;
    var sum = 0;
    for (; i < 12; i++) {
      sum += int.parse(idCitizen[i]) * (13 - i);
    }
    var check = (11 - sum % 11) % 10;

    if (check == int.parse(idCitizen[12])) {
      return false;
    }
    return true;
  }

  bool isemailNotPattern(String email) {
    email = email.replaceAll(' ', '');

    if (!TextInputPattern.emailPattern.hasMatch(email)) {
      return true;
    } else {
      return false;
    }
  }

  Future<int> isemailDuplicate(String email) async {
    try {
      SweetAlert.show(NavigationRoute.navigatorKey.currentContext,
          subtitle: 'กำลังตรวจสอบข้อมูล', style: SweetAlertStyle.loading);
      var account = await AccountService.getAccountByEmail(email: email);
      if (account != null) {
        return 200;
      } else {
        return 204;
      }
    } on TimeoutException catch (_) {
      return 408;
    } on UnauthorisedException catch (_) {
      return 401;
    } on Exception catch (_) {
      return 500;
    }
  }

  bool emailDuplicateResponseAction(int response) {
    if (response == 204) {
      return false;
    } else if (response == 200) {
      errorAlert(wording: 'มีผู้ใช้อื่นใช้อีเมลนี้แล้ว');
    } else if (response == 408) {
      errorAlert(wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้');
    } else {
      errorAlert(wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้');
    }
    return true;
  }

  bool ispasswordNotPattern(String password) {
    password = password.replaceAll(' ', '');
    if (password.isEmpty ||
        password.toString().length < 6 ||
        password.toString().length > 18) {
      return true;
    } else {
      return false;
    }
  }

  bool isConfirmPasswordNotPattern(String password, String confirmPassword) {
    password = password.replaceAll(' ', '');
    confirmPassword = confirmPassword.replaceAll(' ', '');

    if (password.compareTo(confirmPassword) != 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isTelNotPattern(String tel) {
    tel = tel.replaceAll(' ', '');
    return !TextInputPattern.phonePattern.hasMatch(tel);
  }

  bool isOTPNotPattern(String otp) {
    otp = otp.replaceAll(' ', '');
    return !TextInputPattern.otpPattern.hasMatch(otp);
  }
}

class Account {
  Future<int> login({@required String email, @required String password}) async {
    try {
      NotificationData notificationData = NotificationData();
      AccountModel loginForm =
          new AccountModel(email: email, password: password);
      SweetAlert.show(NavigationRoute.navigatorKey.currentContext,
          subtitle: 'กำลังล็อคอิน', style: SweetAlertStyle.loading);
      final account = await AccountService.login(loginForm: loginForm);
      AccountGlobal.setToken(account.token);
      AccountGlobal.setFirstName(account.firstName);
      AccountGlobal.setLastName(account.lastName);
      AccountGlobal.setAccountCode(account.accountCode);
      AccountGlobal.setAccountId(account.accountId);
      await notificationData.updateNotification(initial: true);

      return 200;
    } on TimeoutException catch (_) {
      return 408;
    } on UnauthorisedException catch (_) {
      return 401;
    } on Exception catch (_) {
      return 500;
    }
  }

  void loginResponseAction(int response) {
    if (response == 200) {
      NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute);
    } else if (response == 408) {
      errorAlert(wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้');
    } else if (response == 401) {
      errorAlert(wording: 'Email หรือ Password ไม่ถูกต้อง');
    } else if (response == 500) {
      errorAlert(wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้');
    }
  }

  Future<int> register(
      {@required
          Map<RegisterTopic, TextEditingController> registerController}) async {
    try {
      AccountModel registerForm = new AccountModel(
        firstName: registerController[RegisterTopic.FIRSTNAME]
            .text
            .replaceAll(' ', ''),
        lastName:
            registerController[RegisterTopic.LASTNAME].text.replaceAll(' ', ''),
        idCard: registerController[RegisterTopic.IDCITIZEN]
            .text
            .replaceAll(' ', ''),
        email: registerController[RegisterTopic.EMAIL].text.replaceAll(' ', ''),
        password:
            registerController[RegisterTopic.PASSWORD].text.replaceAll(' ', ''),
      );
      await AccountService.register(registerForm: registerForm);
      return 200;
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
      
    }
  }

  Future<void> updateFullName(
      {@required AccountModel account,
      bool isWillPopScope = false,
      bool isLogout = false}) async {
    try {
      if (account.firstName != AccountGlobal.getFirstName() ||
          account.lastName != AccountGlobal.getLastName()) {
        await AccountService.updateFullName(account: account);
        AccountGlobal.setFirstName(account.firstName);
        AccountGlobal.setLastName(account.lastName);
      }
      if (isLogout) {
        AccountGlobal.logoutRemoveToken();
      } else {
        NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute);
      }
    } on TimeoutException catch (_) {
      errorAlert(
          wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    } on UnauthorisedException catch (_) {
      errorAlert(
          wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
    } on Exception catch (_) {
      errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    }
  }
}
