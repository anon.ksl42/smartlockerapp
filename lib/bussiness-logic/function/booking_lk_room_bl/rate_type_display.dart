import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart' as routePath;
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/service.dart';

class RateTypeDisplay {
  List<String> typeListForShow = [];
  Map<String, int> rateTypeForData = new Map<String, int>();

  Future<void> initialType() async {
    try {
      List<RateTypeModel> rateTypeList =
          await ServiceRateService.getAllRateType();
      for (RateTypeModel rateType in rateTypeList) {
        typeListForShow.add(rateType.typeName);
        rateTypeForData[rateType.typeName] = rateType.typeId;
      }
    } on TimeoutException catch (_) {
      errorAlert(
          wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    } on UnauthorisedException catch (_) {
      errorAlert(
          wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
    } on Exception catch (_) {
      errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
          function: () =>
              NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
    }
  }
}
