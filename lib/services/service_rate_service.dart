import 'dart:async';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/datapattern.dart';
import 'package:SmartLockerApp/constant/path_api/path_api.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';
import 'base_service.dart';

class ServiceRateService {
  static Future<List<RateTypeModel>> getAllRateType() async {
    String path = ServiceRatePathAPI.RATE_TYPE;
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));

    if (result.statusCode != 200) {
      BaseService.errorHandle(errorStatus: result.statusCode);
      throw Exception("System Error");
    } else {
      Iterable<dynamic> resultList = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAPLIST);
      List<RateTypeModel> bookingTypeList =
          resultList.map((model) => RateTypeModel.fromJson(model)).toList();
      return bookingTypeList;
    }
  }

  static Future<List<ServiceRateModel>> getAllServiceRate() async {
    String path = ServiceRatePathAPI.SERVICE_CHARGE;
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));

    if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else if (result.statusCode != 200) {
      throw Exception();
    } else {
      Iterable<dynamic> resultList = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAPLIST);
      List<ServiceRateModel> bookingTypeList =
          resultList.map((model) => ServiceRateModel.fromJson(model)).toList();
      return bookingTypeList;
    }
  }

  static Future<ServiceRateModel> getServiceChargeByLockerRoomIdAndTypeId(
      {@required int lkRoomId, @required int typeId}) async {
    String path =
        ServiceRatePathAPI.getServiceChargeByLockerRoomIdAndTypeIdPath(
            lkRoomId: lkRoomId, typeId: typeId);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));

    if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else if (result.statusCode != 200) {
      throw Exception();
    } else {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      ServiceRateModel serviceRate = ServiceRateModel.fromJson(content);
      return serviceRate;
    }
  }
}
