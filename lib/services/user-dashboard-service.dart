import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/user-dashboard-model.dart';
import 'package:SmartLockerApp/repositories/user-dashboard-repository.dart';
import 'package:SmartLockerApp/services/base_service.dart';

class UserDashboardService extends BaseService {
  Future<UserDashboard> getDashboard(int accountId, String rangeType) async {
    UserDashboardRepository userDashboardRepository = UserDashboardRepository();
    var result =
        await userDashboardRepository.getUserDashboard(accountId, rangeType);
    if (result.statusCode == 401) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } else if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      UserDashboard dashboard = UserDashboard.fromJson(content);
      return dashboard;
    }else{
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
