import 'dart:async';
import 'dart:convert';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/service.dart';

class PaymentService {
  static Future<List<PaymentInfoModel>> getAllPaymentInfoByAccountId() async {
    String path = PaymentPathAPi.getAllPaymentInfoByAccountIdPath();
    try {
      var response =
          await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
      if (response.statusCode == 401) {
        throw UnauthorisedException("Unauthorize");
      }

      if (response.statusCode != 200) {
        throw Exception("System Error");
      } else {
        Iterable<dynamic> resultList = BaseService.getContent(
            response: response, dataPattern: DataPattern.MAPLIST);
        List<PaymentInfoModel> paymentInfoList = resultList
            .map((model) => PaymentInfoModel.fromJson(model))
            .toList();
        return paymentInfoList;
      }
    } on TimeoutException catch (e) {
      throw TimeoutException(e.message);
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<String> payment(PaymentInfoModel paymentInfoModel) async {
    String path = PaymentPathAPi.PAYMENT;
    var paymentInfoBody = jsonEncode(paymentInfoModel.toJson());
    try {
      var response =
          await BaseService.sendAPIPOST(path: path, body: paymentInfoBody)
              .timeout(Duration(seconds: 10));
      if (response.statusCode == 401) {
        throw UnauthorisedException("Unauthorize");
      }

      if (response.statusCode != 200) {
        throw Exception("System Error");
      } else {
        return BaseService.getContent(
            response: response, dataPattern: DataPattern.MAP);
      }
    } on TimeoutException catch (e) {
      throw TimeoutException(e.message);
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<int> addPaymentInfo(PaymentInfoModel paymentInfoModel) async {
    String path = PaymentPathAPi.PAYMENT_INFO;
    var paymentInfoBody = jsonEncode(paymentInfoModel.toJson());
    try {
      var response =
          await BaseService.sendAPIPOST(path: path, body: paymentInfoBody)
              .timeout(Duration(seconds: 10));
      if (response.statusCode == 401) {
        throw UnauthorisedException("Unauthorize");
      }

      if (response.statusCode != 201) {
        throw Exception();
      } 
      else {
        return response.statusCode;
      }
    } on TimeoutException catch (e) {
      throw TimeoutException(e.message);
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }
}
