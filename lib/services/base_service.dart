import 'dart:convert';

import 'package:SmartLockerApp/constant/datapattern.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:SmartLockerApp/data/global/global.dart';
import 'package:SmartLockerApp/models/responseheader.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:sweetalert/sweetalert.dart';

class BaseService {
  static const String BASE_URL = 'https://webservice-locker.ml/api';
  // static const String BASE_URL = 'https://fc40a115505b.ngrok.io/api';
  static Map<String, String> getHeaderWithAuth() {
    return {
      "Content-type": "application/json; charset=utf-8 ",
      "Authorization": "Bearer ${AccountGlobal.getToken() ?? ''}"
    };
  }

  static Future<http.Response> sendAPIGET(String path) async {
    String baseUrl = BaseService.BASE_URL;
    Map<String, String> headers = BaseService.getHeaderWithAuth();
    return await http.get(
      '$baseUrl$path',
      headers: headers,
    );
  }

  static Future<http.Response> sendAPIPOST(
      {@required String path, @required String body}) async {
    String baseUrl = BaseService.BASE_URL;
    Map<String, String> headers = BaseService.getHeaderWithAuth();
    return await http.post('$baseUrl$path', headers: headers, body: body);
  }

  static Future<http.Response> sendAPIPUT(
      {@required String path, @required String body}) async {
    String baseUrl = BaseService.BASE_URL;
    Map<String, String> headers = BaseService.getHeaderWithAuth();
    return await http.put('$baseUrl$path', headers: headers, body: body);
  }

  static dynamic getContent(
      {@required http.Response response, @required DataPattern dataPattern}) {
    ResponseHeader responseHeader =
        ResponseHeader.formJson(json.decode(response.body));
    if (dataPattern == DataPattern.MAP) {
      return responseHeader.content;
    } else if (dataPattern == DataPattern.MAPLIST) {
      Iterable contentList = responseHeader.content as List;
      return contentList;
    }
  }

  static void errorHandle({@required int errorStatus}) {
    print(errorStatus);
    String wording;
    // String route;
    if (errorStatus == 401) {
      wording = 'หมดอายุการใช้งาน กรุณาล็อคอินใหม่อีกครั้ง';
      // route = routePath.LoginRoute;
      AccountGlobal.logoutRemoveToken();
    } else if (errorStatus == 500) {
      wording = 'System error';
      // route = routePath.MainRoute;
    } else if (errorStatus == 204) {
      wording = 'ไม่พบผู้ใช้';
    } else if (errorStatus == 408) {
      wording = 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้';
    }
    SweetAlert.show(
      NavigationRoute.navigatorKey.currentContext,
      subtitle: wording,
      style: SweetAlertStyle.error,
      onPress: (isConfirm) {
        var route = NavigationRoute.route;
        if (route != routePath.PrepareDataRoute) {
          NavigationRoute.goBack();
        } else {
          SystemNavigator.pop();
        }
        return false;
      },
    );
  }
}
