import 'dart:async';
import 'dart:convert';

import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:flutter/material.dart';
import 'base_service.dart';

class AccountService {
  static Future<AccountModel> login({@required AccountModel loginForm}) async {
    String path = AccountPathAPI.ACCOUNT_LOGIN;
    var loginformBody = jsonEncode(loginForm.toJson());
    try {
      var result =
          await BaseService.sendAPIPOST(body: loginformBody, path: path)
              .timeout(Duration(seconds: 10));
      if (result.statusCode == 401) {
        throw UnauthorisedException(APIStatus.UNAUTHRIZE);
      } else if (result.statusCode == 200) {
        var content = BaseService.getContent(
            response: result, dataPattern: DataPattern.MAP);
        AccountModel accountResult = AccountModel.formJson(content);
        return accountResult;
      } else {
        throw Exception(APIStatus.SYETEM_ERROR);
      }
    } on TimeoutException catch (_) {
      throw TimeoutException(APIStatus.TIME_OUT.toString());
    }
  }

  static Future<AccountModel> getAccountByEmail(
      {@required String email}) async {
    String path = AccountPathAPI.getAccountByEmailPath(email);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      AccountModel accountResult = AccountModel.formJson(content);
      return accountResult;
    } else if (result.statusCode == 204) {
      return null;
    } else {
      throw Exception();
    }
  }

  static Future<AccountModel> getAcccountByAccountCode(
      {@required String accountCode}) async {
    String path = AccountPathAPI.getAcccountByAccountCodePath(accountCode);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      AccountModel accountResult = AccountModel.formJson(content);
      return accountResult;
    } else if (result.statusCode == 204) {
      return null;
    } else {
      throw Exception();
    }
  }

  static Future<AccountModel> getAccountByResBookingId(
      {@required int resBookingId}) async {
    String path = AccountPathAPI.getAccountByResBookingIdPath(resBookingId);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      AccountModel accountResult = AccountModel.formJson(content);
      return accountResult;
    } else if (result.statusCode == 204) {
      return null;
    } else {
      throw Exception();
    }
  }

  static Future<AccountModel> getAccountByTransfersBookingId(
      {@required int tranBookingId}) async {
    String path =
        AccountPathAPI.getAccountByTransfersBookingIdPath(tranBookingId);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      AccountModel accountResult = AccountModel.formJson(content);
      return accountResult;
    } else if (result.statusCode == 204) {
      return null;
    } else {
      throw Exception();
    }
  }

  static Future<void> register({@required AccountModel registerForm}) async {
    String path = AccountPathAPI.ACCOUNT_REGISTER;
    var registerformBody = jsonEncode(registerForm.toJson());
    var result =
        await BaseService.sendAPIPOST(body: registerformBody, path: path)
            .timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
    } else {
      throw Exception();
    }
  }

  static Future<void> updateFullName({@required AccountModel account}) async {
    String path = AccountPathAPI.ACCOUNT;
    var registerformBody = jsonEncode(account.toJson());
    var result =
        await BaseService.sendAPIPUT(body: registerformBody, path: path)
            .timeout(Duration(seconds: 10));
    if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else if (result.statusCode != 200) {
      throw Exception();
    }
  }
}
