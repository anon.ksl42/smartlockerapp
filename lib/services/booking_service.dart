import 'dart:async';
import 'dart:convert';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/constant/datapattern.dart';
import 'package:SmartLockerApp/constant/path_api/booking_path.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/base_service.dart';

class BookingService {
  static Future<List<BookingModel>> getAllBookingByAccountId() async {
    String path = BookingPathAPI.getAllBookingByAccountIdPath();
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 200) {
      Iterable<dynamic> contentList = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAPLIST);
      List<BookingModel> bookingList =
          contentList.map((model) => BookingModel.fromJson(model)).toList();

      return bookingList;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }

  static Future<BookingModel> addBooking(BookingModel bookingModel) async {
    String path = BookingPathAPI.BOOKING;
    String bookingJson = jsonEncode(bookingModel.toJson());
    var result = await BaseService.sendAPIPOST(path: path, body: bookingJson)
        .timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      BookingModel booking = BookingModel.fromJson(content);

      return booking;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }

  static Future<BookingModel> updateBooking(BookingModel bookingModel) async {
    String path = BookingPathAPI.BOOKING;
    String bookingJson = jsonEncode(bookingModel.toJson());
    var result = await BaseService.sendAPIPUT(path: path, body: bookingJson)
        .timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      BookingModel booking = BookingModel();
      if (content != null) {
        booking = BookingModel.fromJson(content);
      }

      return booking;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }

  static Future<BookingModel> transfer(BookingModel transferDetail) async {
    String path = BookingPathAPI.BOOKING_TRANSFER;
    String transferJson = jsonEncode(transferDetail.toJson());
    var result = await BaseService.sendAPIPUT(path: path, body: transferJson)
        .timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      BookingModel booking = BookingModel.fromJson(content);

      return booking;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }

  static Future<BookingModel> cancelTransfer(BookingModel bookingModel) async {
    String path = BookingPathAPI.BOOKING_TRANSFER_CANCEL;
    String bookingJson = jsonEncode(bookingModel.toJson());
    var result = await BaseService.sendAPIPUT(path: path, body: bookingJson)
        .timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      BookingModel booking = BookingModel.fromJson(content);

      return booking;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }

  static Future<BookingModel> getBookingByBookingId(int bookingId) async {
    String path = BookingPathAPI.getAllBookingByBookingIdPath(bookingId);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      BookingModel booking = BookingModel.fromJson(content);

      return booking;
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else {
      throw Exception();
    }
  }
}
