import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/locker_model.dart';
import 'package:SmartLockerApp/models/locker_room_model.dart';
import 'package:SmartLockerApp/services/service.dart';

class LockerService {
  static Future<List<LockerModel>> getLockerByLocateId(int locateId) async {
    String path = LockerPathAPI.getLockerByLocateIdPath(locateId);
    var response =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (response.statusCode == 401) {
      throw UnauthorisedException();
    } else if (response.statusCode == 204) {
      return null;
    } else if (response.statusCode == 200) {
      Iterable<dynamic> content = BaseService.getContent(
          response: response, dataPattern: DataPattern.MAPLIST);
      List<LockerModel> lockerRoomList =
          content.map((model) => LockerModel.fromJson(model)).toList();
      return lockerRoomList;
    } else {
      throw Exception();
    }
  }

  static Future<List<LockerRoomModel>> getAllLockerRoomByLockerId(
      int lockerId) async {
    String path = LockerPathAPI.getAllLockerRoomByLockerId(lockerId);
    var response =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (response.statusCode == 401) {
      throw UnauthorisedException();
    } else if (response.statusCode == 204) {
      return null;
    } else if (response.statusCode == 200) {
      Iterable<dynamic> contentList = BaseService.getContent(
          response: response, dataPattern: DataPattern.MAPLIST);
      List<LockerRoomModel> lockerRoomList =
          contentList.map((model) => LockerRoomModel.fromJson(model)).toList();
      return lockerRoomList;
    } else {
      throw Exception();
    }
  }

  static Future<void> validateLkRoom(int lkRoomId) async {
    String path = '/locker/validateroom?lkroomId=$lkRoomId';
    var response =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (response.statusCode == 200) {
      return ;
    }else if (response.statusCode == 401) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } else if (response.statusCode == 409) {
      return Future.error(APIStatus.CONFLICT);
    } else {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
