import 'dart:async';

import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/datapattern.dart';
import 'package:SmartLockerApp/constant/path_api/path_api.dart';
import 'package:SmartLockerApp/models/models.dart';

import 'base_service.dart';

class LocationService {
  static Future<List<LocationModel>> getLocations() async {
    String path = LocationPathAPi.LOCATION;
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));

    if (result.statusCode == 200) {
      Iterable<dynamic> contentList = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAPLIST);
      List<LocationModel> locationList =
          contentList.map((e) => LocationModel.fromJson(e)).toList();
      return locationList;
    } else if (result.statusCode == 401) {
      return Future.error(APIStatus.UNAUTHRIZE);
    } else {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }

  static Future<LocationModel> getLocationByLocationId(int locationId) async {
    String path = LocationPathAPi.getLocationByLocationIdPath(locationId);
    var result =
        await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
    if (result.statusCode == 401) {
      throw UnauthorisedException();
    } else if (result.statusCode == 204) {
      return null;
    } else if (result.statusCode == 200) {
      var content = BaseService.getContent(
          response: result, dataPattern: DataPattern.MAP);
      LocationModel location = LocationModel.fromJson(content);
      return location;
    } else {
      throw Exception();
    }
  }
}
