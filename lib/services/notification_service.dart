import 'dart:async';
import 'dart:convert';
import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/datapattern.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/base_service.dart';
import 'package:SmartLockerApp/constant/path_api/path_api.dart';

class NotificationService {
  static Future<List<EventTypeModel>> getAllEventType() async {
    String path = NotificationPathAPI.EVENT_TYPE;
    try {
      var response =
          await BaseService.sendAPIGET(path).timeout(Duration(seconds: 60));
      if (response.statusCode == 401) {
        throw UnauthorisedException();
      } else if (response.statusCode != 200) {
        throw Exception("System Error");
      } else {
        Iterable<dynamic> resultList = BaseService.getContent(
            response: response, dataPattern: DataPattern.MAPLIST);
        List<EventTypeModel> eventTypeList =
            resultList.map((model) => EventTypeModel.fromJson(model)).toList();
        return eventTypeList;
      }
    } on TimeoutException catch (e) {
      throw TimeoutException(e.message);
    } catch (e) {
      throw Exception(e);
    }
  }

  static Future<List<NotificationModel>> getAllNotificationByAccountId(
      int page, int perPage,
      {bool isInitial = false}) async {
    String path = NotificationPathAPI.getNotificationPath(1, 20);
      var response =
          await BaseService.sendAPIGET(path).timeout(Duration(seconds: 10));
      if (response.statusCode == 401) {
        throw UnauthorisedException("Unauthorize");
      }

      if (response.statusCode != 200) {
        throw Exception("System Error");
      } else {
        Iterable<dynamic> resultList = BaseService.getContent(
            response: response, dataPattern: DataPattern.MAPLIST);
        List<NotificationModel> eventTypeList = resultList
            .map((model) => NotificationModel.fromJson(model))
            .toList();
        return eventTypeList;
      }
  }

  static Future<void> addNotification(NotificationModel notification) async {
    String path = NotificationPathAPI.getNotificationPath(1, 20);
    String notificationJson = jsonEncode(notification.toJson());
    try {
      var response =
          await BaseService.sendAPIPOST(path: path, body: notificationJson)
              .timeout(Duration(seconds: 10));
      if (response.statusCode == 401) {
        return Future.error(APIStatus.UNAUTHRIZE);
      }

      if (response.statusCode != 200) {
        return Future.error(APIStatus.SYETEM_ERROR);
      } else {}
    } on TimeoutException catch (_) {
      return Future.error(APIStatus.TIME_OUT);
    } on Exception catch (_) {
      return Future.error(APIStatus.SYETEM_ERROR);
    }
  }
}
