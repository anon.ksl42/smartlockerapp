class EventTypeModel {
  int eventId;
  String eventName;

  EventTypeModel(this.eventId, this.eventName);

  EventTypeModel.fromJson(Map<String, dynamic> json) {
    eventId = json['eventId'];
    eventName = json['eventName'];
  }

  EventTypeModel.toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['eventId'] = this.eventId;
    data['eventName'] = this.eventName;
  }
}