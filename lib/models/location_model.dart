class LocationModel {
  int locateId;
  String locateName;
  String longtitude;
  String latitude;
  int addressId;
  String status;
  String province;
  String postalCode;
  String district;
  String createDate;
  String updateDate;
  String subDistrict;

  LocationModel(
      {this.locateId,
      this.locateName,
      this.longtitude,
      this.latitude,
      this.addressId,
      this.status,
      this.province,
      this.postalCode,
      this.district,
      this.createDate,
      this.updateDate,
      this.subDistrict});

  factory LocationModel.fromJson(Map<String, dynamic> json) {
    return new LocationModel(
        locateId: json['locateId'],
        locateName: json['locateName'],
        longtitude: json['longtitude'],
        latitude: json['latitude'],
        addressId: json['addressId'],
        status: json['status'],
        province: json['province'],
        postalCode: json['postalCode'],
        district: json['district'],
        createDate: json['createDate'],
        updateDate: json['updateDate'],
        subDistrict: json['subDistrict']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['locateId'] = this.locateId;
    data['locateName'] = this.locateName;
    data['longtitude'] = this.longtitude;
    data['latitude'] = this.latitude;
    data['addressId'] = this.addressId;
    data['status'] = this.status;
    data['province'] = this.province;
    data['postalCode'] = this.postalCode;
    data['district'] = this.district;
    data['createDate'] = this.createDate;
    data['updateDate'] = this.updateDate;
    return data;
  }
}
