import 'package:SmartLockerApp/models/booking_model.dart';

class TransferUpdateModel {
  BookingModel bookingHistory;
  String status;

  TransferUpdateModel(this.bookingHistory, this.status);
}
