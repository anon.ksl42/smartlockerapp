class ResponseHeader {
  String status;
  String message;
  dynamic content;

  int page;
  int perPage;
  int totalElement;

  ResponseHeader(
      {this.status,
      this.message,
      this.content,
      this.page,
      this.perPage,
      this.totalElement});

  factory ResponseHeader.formJson(Map<String, dynamic> json) {
    return ResponseHeader(
        status: json['status'],
        message: json['message'],
        content: json['content'],
        page: json['page'],
        perPage: json['perPage'],
        totalElement: json['totalElement']);
  }
}
