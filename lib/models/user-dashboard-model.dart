class UserDashboard {
  int finishUsing;
  int totalTransfer;
  int successTransfer;
  int rejectTransfer;

  UserDashboard(
      {this.finishUsing,
      this.totalTransfer,
      this.successTransfer,
      this.rejectTransfer});

  UserDashboard.fromJson(Map<String, dynamic> json) {
    finishUsing = json['finishUsing'];
    totalTransfer = json['totalTransfer'];
    successTransfer = json['successTransfer'];
    rejectTransfer = json['rejectTransfer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['finishUsing'] = this.finishUsing;
    data['totalTransfer'] = this.totalTransfer;
    data['successTransfer'] = this.successTransfer;
    data['rejectTransfer'] = this.rejectTransfer;
    return data;
  }
}