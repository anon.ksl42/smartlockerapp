class LockerRoomModel {
  int lkRoomId;
  String lkRoomCode;
  int lockerId;
  String lkSizeName;
  int columnPosition;
  int rowPosition;
  String status;

  LockerRoomModel(
      {this.lkRoomId,
      this.lkRoomCode,
      this.lockerId,
      this.lkSizeName,
      this.columnPosition,
      this.rowPosition,
      this.status});

  LockerRoomModel.fromJson(Map<String, dynamic> json) {
    lkRoomId = json['lkRoomId'];
    lkRoomCode = json['lkRoomCode'];
    lockerId = json['lockerId'];
    lkSizeName = json['lkSizeName'];
    columnPosition = json['columnPosition'];
    rowPosition = json['rowPosition'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lkRoomId'] = this.lkRoomId;
    data['lkRoomCode'] = this.lkRoomCode;
    data['lockerId'] = this.lockerId;
    data['lkSizeName'] = this.lkSizeName;
    data['rowPosition'] = this.rowPosition;
    data['columnPosition'] = this.columnPosition;
    data['status'] = this.status;
    return data;
  }
}
