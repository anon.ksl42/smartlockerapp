class PaymentInfoModel {
  int infoId;
  String cardName;
  String cardNumber;
  int cardTypeId;
  DateTime expDate;
  String status;
  String createDate;
  DateTime updateDate;
  int accountId;
  String cvv;
  double amount;
  bool selected = false;

  int rateTypeId;
  int lkRoomId;

  PaymentInfoModel(
      {this.infoId,
      this.cardName,
      this.cardNumber,
      this.cardTypeId,
      this.expDate,
      this.status,
      this.createDate,
      this.updateDate,
      this.accountId,
      this.rateTypeId,
      this.lkRoomId});

  PaymentInfoModel.fromJson(Map<String, dynamic> json) {
    infoId = json['infoId'];
    cardName = json['cardName'];
    cardNumber = json['cardNumber'];
    cardTypeId = json['cardTypeId'];
    expDate = DateTime.parse(json['expDate']);
    status = json['status'];
    createDate = json['createDate'];
    updateDate = json['updateDate'];
    accountId = json['accountId'];
    rateTypeId = json['rateTypeId'];
    lkRoomId = json['lkRoomId'];
    cvv = json['cvv'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['infoId'] = this.infoId;
    data['cardName'] = this.cardName;
    data['cardNumber'] = this.cardNumber;
    data['cardTypeId'] = this.cardTypeId;
    data['expDate'] = this.expDate.toString();
    data['status'] = this.status;
    data['createDate'] = this.createDate;
    data['updateDate'] = this.updateDate;
    data['accountId'] = this.accountId;
    data['amount'] = this.amount;
    data['rateTypeId'] = this.rateTypeId;
    data['lkRoomId'] = this.lkRoomId;
    data['cvv'] = this.cvv;
    return data;
  }
}
