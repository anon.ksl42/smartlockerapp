class AccountModel {
  int accountId;
  String firstName;
  String lastName;
  String token;
  String idCard;
  String email;
  String accountCode;
  String password;
  String roleUser = 'User';

  AccountModel(
      {this.firstName,
      this.accountId,
      this.lastName,
      this.idCard,
      this.email,
      this.password,
      this.token,
      this.accountCode});

  AccountModel.formJson(Map<String, dynamic> json)
      : email = json['email'],
        accountId = json['accountId'],
        password = json['password'],
        firstName = json['firstName'],
        lastName = json['lastName'],
        idCard = json['idCard'],
        token = json['token'],
        accountCode = json['accountCode'],
        roleUser = json['roleUser'];

  Map<String, dynamic> toJson() => {
        'firstName': firstName,
        'lastName': lastName,
        'idCard': idCard,
        'email': email,
        'password': password,
        'roleUser': roleUser,
        'token': token,
        'accountId': accountId,
        'accountCode': accountCode
      };
}
