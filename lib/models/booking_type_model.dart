class RateTypeModel {
  int typeId;
  String typeName;

  RateTypeModel({this.typeId, this.typeName});

  RateTypeModel.fromJson(Map<String, dynamic> json) {
    typeId = json['typeId'];
    typeName = json['typeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['typeId'] = this.typeId;
    data['typeName'] = this.typeName;
    return data;
  }
}
