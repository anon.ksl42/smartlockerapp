import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/models/booking_model.dart';

class TransferDetailModel {
  int accountId;
  String status;
  int bookingId;
  AccountModel receiverAccount;
  BookingModel bookingFromTransfer;

  TransferDetailModel(
      {this.accountId,
      this.status,
      this.bookingId,
      this.receiverAccount,
      this.bookingFromTransfer});

  TransferDetailModel.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    bookingId = json['bookingId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['bookingId'] = this.bookingId;
    return data;
  }
}
