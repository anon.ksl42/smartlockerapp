class BookingModel {
  int bookingId;
  int overTime;
  int lkRoomId;
  int accountId;
  String status;
  DateTime createDate;
  DateTime updateDate;
  DateTime startDate;
  DateTime endDate;
  String passcode;
  int rateTypeId;
  String transaction;
  String lockerCode;

  String locateName;
  String lkRoomCode;
  String typeName;
  double price;

  BookingModel(
      {this.bookingId,
      this.overTime,
      this.lkRoomId,
      this.accountId,
      this.locateName,
      this.lkRoomCode,
      this.price,
      this.typeName,
      this.rateTypeId,
      this.lockerCode,
      this.status});

  BookingModel.fromJson(Map<String, dynamic> json) {
    bookingId = json['bookingId'];
    overTime = json['overTime'];
    lkRoomId = json['lkRoomId'];
    accountId = json['accountId'];
    status = json['status'];
    rateTypeId = json['rateTypeId'];
    createDate =
        json['createDate'] != null ? DateTime.parse(json['createDate']) : null;
    passcode = json['passCode'].toString();
    locateName = json['locateName'];
    lkRoomCode = json['lkRoomCode'];
    startDate = ["", null].contains(json['startDate'])
        ? null
        : DateTime.parse(json['startDate']);
    endDate = ["", null].contains(json['endDate'])
        ? null
        : DateTime.parse(json['endDate']);
    updateDate = ["", null].contains(json['updateDate'])
        ? null
        : DateTime.parse(json['updateDate']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bookingId'] = this.bookingId;
    data['overTime'] = this.overTime;
    data['lkRoomId'] = this.lkRoomId;
    data['accountId'] = this.accountId;
    data['status'] = this.status;
    data['createDate'] =
        this.createDate == null ? '' : this.createDate.toString();
    data['passCode'] = this.passcode;
    data['locateName'] = this.locateName;
    data['lkRoomCode'] = this.lkRoomCode;
    data['startDate'] = this.startDate == null ? '' : this.startDate.toString();
    data['endDate'] = this.endDate == null ? '' : this.endDate.toString();
    data['updateDate'] =
        this.updateDate == null ? '' : this.updateDate.toString();
    data['rateTypeId'] = this.rateTypeId;

    return data;
  }
}
