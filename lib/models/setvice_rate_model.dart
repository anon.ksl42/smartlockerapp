class ServiceRateModel {
  int srid;
  String description;
  int time;
  String status;
  String createDate;
  Null updateDate;
  String typeName;
  String lkSizeName;
  int lkSizeId;
  double price;
  double surplusPrice;

  ServiceRateModel(
      {this.srid,
      this.description,
      this.time,
      this.status,
      this.createDate,
      this.updateDate,
      this.typeName,
      this.lkSizeName,
      this.lkSizeId,
      this.price,
      this.surplusPrice});

  ServiceRateModel.fromJson(Map<String, dynamic> json) {
    srid = json['srid'];
    description = json['description'];
    time = json['time'];
    status = json['status'];
    createDate = json['createDate'];
    updateDate = json['updateDate'];
    typeName = json['typeName'];
    lkSizeName = json['lkSizeName'];
    lkSizeId = json['lkSizeId'];
    price = json['price'];
    surplusPrice = json['surplusPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['srid'] = this.srid;
    data['description'] = this.description;
    data['time'] = this.time;
    data['status'] = this.status;
    data['createDate'] = this.createDate;
    data['updateDate'] = this.updateDate;
    data['typeName'] = this.typeName;
    data['lkSizeName'] = this.lkSizeName;
    data['lkSizeId'] = this.lkSizeId;
    data['price'] = this.price;
    data['surplusPrice'] = this.surplusPrice;
    return data;
  }
}
