class LockerModel {
  int lockerId;
  String lockerCode;
  int locateId;
  String status;

  LockerModel(
      {this.lockerId,
      this.lockerCode,
      this.locateId,
      this.status,});

  LockerModel.fromJson(Map<String, dynamic> json) {
    lockerId = json['lockerId'];
    lockerCode = json['lockerCode'];
    locateId = json['locateId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lockerId'] = this.lockerId;
    data['lockerCode'] = this.lockerCode;
    data['locateId'] = this.locateId;
    data['status'] = this.status;
    return data;
  }
}