class NotificationModel {
  int notiId;
  String description;
  String readStatus;
  String status;
  DateTime createDate;
  int eventId;
  int accountId;
  String eventName;

  NotificationModel(
      {this.notiId,
      this.description,
      this.readStatus,
      this.status,
      this.createDate,
      this.eventId,
      this.accountId,
      this.eventName});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    notiId = json['notiId'];
    description = json['description'];
    readStatus = json['readStatus'];
    status = json['status'];
    createDate =
        json['createDate'] != null ? DateTime.parse(json['createDate']) : null;
    eventId = json['eventId'];
    accountId = json['accountId'];
    eventName = json['eventName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notiId'] = this.notiId;
    data['description'] = this.description;
    data['readStatus'] = this.readStatus;
    data['status'] = this.status;
    data['createDate'] =
        this.createDate == null ? '' : this.createDate.toString();
    data['eventId'] = this.eventId;
    data['accountId'] = this.accountId;
    data['eventName'] = this.eventName;
    return data;
  }
}
