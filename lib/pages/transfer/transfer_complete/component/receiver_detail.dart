import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ReceiverDetail extends StatelessWidget {
  final BookingModel bookingFromTransfer;

  const ReceiverDetail({Key key, @required this.bookingFromTransfer})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#707070'), width: 1)),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Text(
          'รายละเอียดการใช้งาน',
          style: TextStyle(fontSize: 20),
        ),
        Text(
          'ชื่อสถานที่ : ${bookingFromTransfer.locateName}  ',
          style: TextStyle(fontSize: 20),
        ),
        Text(
          'หมายเลขล็อคเกอร์ : ${bookingFromTransfer.lkRoomCode}  ',
          style: TextStyle(fontSize: 20),
        ),
      ]),
    );
  }
}
