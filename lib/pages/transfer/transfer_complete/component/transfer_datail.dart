import 'package:SmartLockerApp/models/account_model.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class TransferDetail extends StatelessWidget {
  final AccountModel receiverAccount;
  final String postDetail;

  const TransferDetail(
      {Key key, @required this.receiverAccount, @required this.postDetail})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#707070'), width: 1)),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Align(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'ระบบจะแจ้งการโอนสิทธิ์ของคุณให้กับ\n${receiverAccount.firstName} ${receiverAccount.lastName}\n$postDetail',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20),
            ),
          ),
        ),
      ]),
    );
  }
}
