import 'package:SmartLockerApp/bussiness-logic/function/transfer_complete_bl.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_complete/component/transfer_datail.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

import 'component/receiver_detail.dart';

class TransferCompletePage extends StatefulWidget {
  final TransferDetailModel transferDetailModel;

  const TransferCompletePage({Key key, @required this.transferDetailModel})
      : super(key: key);
  @override
  _TransferCompletePageState createState() =>
      _TransferCompletePageState(transferDetailModel);
}

class _TransferCompletePageState extends State<TransferCompletePage> {
  final TransferDetailModel transferDetailModel;

  TransferComplete _transferComplete = TransferComplete();

  _TransferCompletePageState(this.transferDetailModel);

  @override
  void initState() {
    _transferComplete.updateTransferRequest(transferDetailModel).then((value) {
      setState(() {});
    }).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
        );
      } else {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
      setState(() {
        _transferComplete.isError = true;
        _transferComplete.isLoading = false;
      });
    });
    templateWording();
    super.initState();
  }

  templateWording() {
    if (transferDetailModel.status == "WT") {
      _transferComplete.titleHeader = 'จองล็อคเกอร์';
      _transferComplete.titleName = 'ทำเรื่องโอนสิทธิ์เรียบร้อย';
      _transferComplete.postDetail =
          'เพื่อให้ยืนยันการรับสิทธื์การใช้งานภายใน 10 นาที';
    } else if (transferDetailModel.status == "CT") {
      _transferComplete.titleHeader = 'ยกเลิกการโอนสิทธิ์';
      _transferComplete.titleName = 'ยกเลิกโอนสิทธิ์เรียบร้อย';
      _transferComplete.postDetail = 'เพื่อให้รับทราบการยกเลิกโอนสิทธิ์';
    } else if (transferDetailModel.status == "R" ||
        transferDetailModel.status == 'NR') {
      _transferComplete.titleHeader = 'ยืนยันการรับสิทธิ';
      _transferComplete.titleName = transferDetailModel.status == "R"
          ? 'รับสิทธิ์เรียบร้อย'
          : 'ปฎิเสธการรับรับสิทธิ์เรียบร้อย';
      _transferComplete.postDetail = '';
    }
  }

  _getDetailFromStatus() {
    if (transferDetailModel.status == "WT" ||
        transferDetailModel.status == "CT") {
      return TransferDetail(
        postDetail: _transferComplete.postDetail,
        receiverAccount: transferDetailModel.receiverAccount,
      );
    } else if (transferDetailModel.status == "R") {
      return ReceiverDetail(
        bookingFromTransfer: transferDetailModel.bookingFromTransfer,
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationRoute.navigateAndRemoveUntil(MainRoute);
        return false;
      },
      child: Scaffold(
        appBar:
            AppbarCommon(title: _transferComplete.titleHeader).build(context),
        body: _transferComplete.isLoading
            ? Loader()
            : _transferComplete.isError
                ? Container()
                : Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    top: 10, left: 20, right: 20, bottom: 10),
                                width: 150,
                                child: Image.asset(
                                  'assets/images/done.png',
                                ),
                              ),
                              Text(
                                _transferComplete.titleName,
                                style: TextStyle(fontSize: 30),
                              ),
                              _transferComplete.isCard
                                  ? _getDetailFromStatus()
                                  : Container(),
                              Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Text(
                                  'วันที่ดำเนินการ : ${dateTimeToThai(_transferComplete.createDate)}',
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
        floatingActionButton: _transferComplete.isLoading
            ? null
            : Padding(
                padding: EdgeInsets.only(bottom: 30),
                child: FloatingActionButton(
                  onPressed: () {
                    NavigationRoute.navigateAndRemoveUntil(MainRoute);
                  },
                  child: Icon(Icons.home),
                ),
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
