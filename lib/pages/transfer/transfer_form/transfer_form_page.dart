import 'package:SmartLockerApp/bussiness-logic/function/transfer_bl.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_form/component/cover_card.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class TransferFormPage extends StatefulWidget {
  final BookingModel bookingHistory;

  const TransferFormPage({Key key, this.bookingHistory}) : super(key: key);
  @override
  _TransferFormPageState createState() =>
      _TransferFormPageState(bookingHistory);
}

class _TransferFormPageState extends State<TransferFormPage> {
  final BookingModel bookingHistory;

  _TransferFormPageState(this.bookingHistory);
  TextEditingController _accountCodeController = new TextEditingController();
  // bool _isAccountInvalid = false;
  Transfer _transfer = Transfer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'โอนสิทธิ์การใช้งาน',
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CoverCard(
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextInformation(
                      detail: 'ชื่อสถานที่ : ${bookingHistory.locateName}',
                    ),
                    TextInformation(
                      detail:
                          'หมายเลขล็อคเกอร์ : ${bookingHistory.lkRoomCode}  ',
                    ),
                  ],
                ),
              ),
              CoverCard(
                body: Column(
                  children: [
                    TextInformation(
                      detail: 'UserId ของผู้ที่จะทำการโอนสิทธิ์ ',
                    ),
                    Container(
                      height: 100,
                      width: 200,
                      child: TextInputTemplate(
                          controller: _accountCodeController,
                          errorTextPattern: null,
                          errorTextInvalid: 'UserId ไม่ถูกต้อง',
                          labelText: null,
                          icon: null,
                          validatePattern: false,
                          validatevalid: _transfer.isSearchNotFound),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: HexColor('#26B73F'),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      onPressed: () async {
                        await _transfer
                            .searchUser(_accountCodeController.text)
                            .catchError((error) {
                          if (error == APIStatus.UNAUTHRIZE) {
                            errorAlert(
                                wording:
                                    'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                                function: () =>
                                    NavigationRoute.navigateAndRemoveUntil(
                                        LoginRoute));
                          } else if (error == APIStatus.TIME_OUT) {
                            errorAlert(
                              wording:
                                  'ไม่สามารถค้นหาได้เนื่องจากขาดการเชื่อมต่อ',
                            );
                          } else if (error == APIStatus.CONFLICT) {
                            errorAlert(
                              wording:
                                  'ไม่สามารถค้นหา userId ของตัวเองได้',
                            );
                          } else if (error == APIStatus.SYETEM_ERROR) {
                            errorAlert(
                              wording:
                                  'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                            );
                          } else if (error == APIStatus.NOCONTENT) {
                            errorAlert(
                              wording: 'ไม่พบผู้ใช้งาน',
                            );
                          }
                        });
                        setState(() {});
                      },
                      child: Container(
                          child: TextInformation(
                              detail: 'ค้นหา', color: Colors.white)),
                    )
                  ],
                ),
              ),
              _transfer.isSearchComplete
                  ? CoverCard(
                      body: Column(
                        children: [
                          TextInformation(detail: 'รายละเอียดผู้ใช้งาน '),
                          TextInformation(
                              detail:
                                  'UserId : ${_transfer.account.accountCode}'),
                          TextInformation(
                              detail:
                                  'ชื่อ-สกุล : ${_transfer.account.firstName}  ${_transfer.account.lastName}'),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: HexColor('#F2B741'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            onPressed: () {
                              TransferDetailModel _tranferDetail =
                                  TransferDetailModel(
                                      receiverAccount: _transfer.account,
                                      accountId: _transfer.account.accountId,
                                      bookingId: bookingHistory.bookingId,
                                      status: 'WT');
                              NavigationRoute.navigateAndRemoveUntil(
                                  TransferCompleteRoute,
                                  arguments: _tranferDetail);
                              MyNotification.transferNotification();
                            },
                            child: Container(
                              child: TextInformation(
                                detail: 'โอนสิทธิ์',
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
