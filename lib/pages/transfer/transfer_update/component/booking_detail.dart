import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:flutter/material.dart';

class BookingDetail extends StatelessWidget {
  final BookingModel bookingHistory;

  const BookingDetail({Key key, @required this.bookingHistory})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.only(bottom: 10),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextInformation(
            detail: 'ชื่อสถานที่ : ${bookingHistory.locateName}',
          ),
          TextInformation(
            detail: 'หมายเลขล็อคเกอร์ : ${bookingHistory.lkRoomCode}  ',
          )
        ],
      ),
    );
  }
}
