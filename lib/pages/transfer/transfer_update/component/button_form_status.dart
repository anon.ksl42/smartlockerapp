import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/route-paths.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/data/notification.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ButtonFromStatus extends StatelessWidget {
  final AccountModel account;
  final BookingModel bookingHistory;
  final String transferStatus;

  const ButtonFromStatus({
    Key key,
    @required this.account,
    @required this.bookingHistory,
    @required this.transferStatus,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String textHeader;
    String textbutton;
    String textDetail;
    int eventId;
    String hexColor;
    if (transferStatus == 'CT') {
      hexColor = '#F31515';
      textHeader = EventType.cancelTransfer;
      textbutton = 'ยกเลิกการโอนสิทธิ์';
      textDetail =
          'ยกเลิกการโอนสิทธิ์ของช่องล็อคเกอร์ ${bookingHistory.lkRoomCode}';
      eventId = EventType.eventTypeForData[EventType.cancelTransfer];
    } else if (transferStatus == 'R') {
      hexColor = '#26B73F';
      textHeader = EventType.approveTransfer;
      textbutton = 'ยืนยันการรับสิทธิ';
      textDetail =
          'ยืนยันการรับสิทธิของช่องล็อคเกอร์ ${bookingHistory.lkRoomCode}';
      eventId = EventType.eventTypeForData[EventType.approveTransfer];
    } else if (transferStatus == 'NR') {
      hexColor = '#F31515';
      textHeader = EventType.rejectTransfer;
      textbutton = 'ปฏิเสธสิทธิ์';
      textDetail = 'ปฏิเสธสิทธิ์ของช่องล็อคเกอร์ ${bookingHistory.lkRoomCode}';
      eventId = EventType.eventTypeForData[EventType.cancelTransfer];
    }
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: HexColor(hexColor),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      onPressed: () {
        TransferDetailModel _tranferDetail = TransferDetailModel(
            receiverAccount: account,
            bookingFromTransfer: bookingHistory,
            accountId: account.accountId,
            bookingId: bookingHistory.bookingId,
            status: transferStatus);
        NavigationRoute.navigateAndRemoveUntil(TransferCompleteRoute,
            arguments: _tranferDetail);
        MyNotification.updateTransferNotification(
            textHeader: textHeader, textDetail: textDetail, eventId: eventId);
      },
      child: Container(
        color: HexColor(hexColor),
        child: TextInformation(
          detail: textbutton,
          color: Colors.white,
        ),
      ),
    );
  }
}
