import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/component/transfer_button_bar.dart';
import 'package:flutter/material.dart';

class TransfersAccount extends StatelessWidget {
  final AccountModel account;
  final BookingModel bookingHistory;
  final String status;
  const TransfersAccount(
      {Key key,
      @required this.account,
      @required this.status,
      @required this.bookingHistory})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.only(bottom: 10),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextInformation(
            detail: 'รายละเอียดผู้ใช้งาน',
          ),
          TextInformation(
            detail: 'UserId : ${account.accountCode}',
          ),
          TextInformation(
            detail: 'ชื่อ-สกุล : ${account.firstName}  ${account.lastName}',
          ),
          TransferButtonBar(
              status: status, account: account, bookingHistory: bookingHistory),
        ],
      ),
    );
  }
}
