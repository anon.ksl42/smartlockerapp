import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/component/booking_detail.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/component/transfers_account.dart';
import 'package:flutter/material.dart';

class TransferUpdateBody extends StatelessWidget {
  final BookingModel bookingHistory;
  final AccountModel account;
  final String status;
  const TransferUpdateBody(
      {Key key,
      @required this.bookingHistory,
      @required this.account,
      @required this.status})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          BookingDetail(bookingHistory: bookingHistory),
          TransfersAccount(
            account: account,
            bookingHistory: bookingHistory,
            status: status,
          ),
        ],
      ),
    );
  }
}
