import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/component/button_form_status.dart';
import 'package:flutter/material.dart';

class TransferButtonBar extends StatelessWidget {
  final String status;
  final AccountModel account;
  final BookingModel bookingHistory;

  const TransferButtonBar(
      {Key key,
      @required this.status,
      @required this.account,
      @required this.bookingHistory})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (status == 'C') {
      return ButtonFromStatus(
          account: account,
          bookingHistory: bookingHistory,
          transferStatus: 'CT');
    } else if (status == 'A') {
      return Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ButtonFromStatus(
                account: account,
                bookingHistory: bookingHistory,
                transferStatus: 'R'),
            ButtonFromStatus(
                account: account,
                bookingHistory: bookingHistory,
                transferStatus: 'NR'),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
