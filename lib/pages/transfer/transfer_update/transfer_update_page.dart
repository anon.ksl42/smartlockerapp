import 'package:SmartLockerApp/bussiness-logic/function/transfer_update_bl.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/transfer_update_model.dart';
import 'package:SmartLockerApp/pages/transfer/transfer_update/component/transfer_update_body.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class TransferUpdatePage extends StatefulWidget {
  final TransferUpdateModel transferUpdateModel;

  const TransferUpdatePage({Key key, @required this.transferUpdateModel})
      : super(key: key);
  @override
  _TransferUpdatePageState createState() =>
      _TransferUpdatePageState(transferUpdateModel);
}

class _TransferUpdatePageState extends State<TransferUpdatePage> {
  final TransferUpdateModel transferUpdateModel;

  TransferUpdate transferUpdate;

  _TransferUpdatePageState(this.transferUpdateModel) {
    transferUpdate = TransferUpdate(transferUpdateModel.status);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: transferUpdate.titleName,
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: FutureBuilder(
        future: transferUpdate.getBookingByBookingId(transferUpdateModel.status,
            transferUpdateModel.bookingHistory.bookingId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              if (snapshot.error == APIStatus.UNAUTHRIZE) {
                Future.delayed(Duration.zero).then((value) {
                  errorAlert(
                      wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                      function: () =>
                          NavigationRoute.navigateAndRemoveUntil(LoginRoute));
                });
              } else if (snapshot.error == APIStatus.TIME_OUT) {
                Future.delayed(Duration.zero).then((value) {
                  errorAlert(
                    wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
                  );
                });
              } else if (snapshot.error == APIStatus.SYETEM_ERROR) {
                Future.delayed(Duration.zero).then((value) {});
                errorAlert(
                  wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                );
              }
              return Container();
            } else {
              return TransferUpdateBody(
                  bookingHistory: transferUpdateModel.bookingHistory,
                  account: transferUpdate.transfersAccount,
                  status: transferUpdateModel.status);
            }
          }
          return Container();
        },
      ),
    );
  }
}
