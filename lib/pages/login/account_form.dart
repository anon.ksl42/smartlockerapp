import 'package:SmartLockerApp/component/textinput-template.dart';
import 'package:flutter/material.dart';

class AccountForm extends StatelessWidget {
  final TextEditingController email;
  final TextEditingController password;
  final bool emailValidate;
  final bool passwordValidate;
  final bool emailValid;
  final bool passwordValid;

  const AccountForm({
    Key key,
    @required this.email,
    @required this.password,
    @required this.emailValidate,
    @required this.passwordValidate,
    @required this.emailValid,
    @required this.passwordValid,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // ignore: non_constant_identifier_names

    return Column(
      children: [
        
        TextInputTemplate(
            controller: email,
            errorTextPattern: 'รูปแบบอีเมลไม่ถูกต้อง',
            errorTextInvalid: 'กรุณากรอกอีเมล',
            icon: Icons.account_circle,
            labelText: 'Email Address',
            validatePattern: emailValidate,
            validatevalid: emailValid),
        SizedBox(
          height: 10,
        ),
        TextInputTemplate(
            obscureText: true,
            controller: password,
            errorTextPattern: 'password 6 - 18 ตัวเท่านั้น',
            errorTextInvalid: 'กรุณากรอก password',
            validatevalid: passwordValid,
            icon: Icons.vpn_key,
            labelText: 'Password',
            validatePattern: passwordValidate),
      ],
    );
  }
}
