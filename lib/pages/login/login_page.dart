import 'package:SmartLockerApp/bussiness-logic/bl/account_bl.dart';
import 'package:SmartLockerApp/bussiness-logic/bl/notification_bl.dart';
import 'package:SmartLockerApp/component/my_alert.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/pages/login/account_form.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  MyAlert _myAlert = MyAlert();
  AccountBussinessLogic accountBussinessLogic = AccountBussinessLogic();
  NotificationBussinessLogic _notificationBussinessLogic =
      NotificationBussinessLogic();
  TextEditingController _email = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  bool _emailNotPattern = false;
  bool _passwordNotPattern = false;
  bool _emailInvalid = false;
  bool _passwordInvalid = false;

  Future<void> formValidate() async {
    _emailNotPattern =
        !accountBussinessLogic.isEmailPattern(email: _email.text);
    _passwordNotPattern =
        !accountBussinessLogic.isPasswordPattern(password: _password.text);
    _emailInvalid = _email.text.replaceAll(' ', '').isEmpty;
    _passwordInvalid = _password.text.replaceAll(' ', '').isEmpty;

    if (!_emailNotPattern && !_passwordNotPattern) {
      // _myAlert.errorAlert(wording: wording)
      _myAlert.waitingDialog();
      await accountBussinessLogic
          .loginAccount(
              loginForm:
                  AccountModel(email: _email.text, password: _password.text))
          .then((_) async {
        await _notificationBussinessLogic
            .getAllNotificationByAccountId(isInitial: true)
            .catchError((error) {
          accountBussinessLogic.loginErrorResponse(error);
        }).then((_) =>
                NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
      }).catchError((error) {
        accountBussinessLogic.loginErrorResponse(error);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Image.asset('assets/images/Logo.png'),
            Container(
              padding:
                  EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
              margin: EdgeInsets.only(top: 20, left: 20, right: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: HexColor('#707070'), width: 1)),
              child: Column(
                children: [
                  AccountForm(
                    email: _email,
                    password: _password,
                    emailValidate: _emailNotPattern,
                    passwordValidate: _passwordNotPattern,
                    emailValid: _emailInvalid,
                    passwordValid: _passwordInvalid,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      width: 200,
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.resolveWith<Color>(
                                    (_) => Theme.of(context).primaryColor)),
                        child: Text(
                          'ล็อคอิน',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: () {
                          formValidate();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            TextButton(
              child: Text(
                'สมัครสมาชิก',
                style: TextStyle(color: HexColor('#080808'), fontSize: 18),
              ),
              onPressed: () {
                NavigationRoute.navigateTo(routePath.RegisterRoute);
              },
            ),
          ],
        ),
      ),
    );
  }
}
