import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class BookingDetailPage extends StatelessWidget {
  final BookingModel bookingdetail;

  const BookingDetailPage({Key key, this.bookingdetail}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
          title: 'จองล็อคเกอร์',
          function: () => Navigator.of(context).pop()).build(context),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Container(
                  alignment: Alignment.bottomLeft,
                  child: TextInformation(
                      detail: 'ยืนยันข้อมูลการจอง', fontsize: 22),
                ),
                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.7),
                      width: 2,
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextInformation(
                          detail:
                              'ชื่อสถานที่\t\t\t: ${bookingdetail.locateName}',
                          fontsize: 18,
                        ),
                        Divider(
                          thickness: 1,
                        ),
                        TextInformation(
                          detail:
                              'รหัสล็อคเกอร์\t\t\t: ${bookingdetail.lockerCode}',
                          fontsize: 18,
                        ),
                        Divider(
                          thickness: 1,
                        ),
                        TextInformation(
                          detail:
                              'รหัสล็อคเกอร์ที่จอง\t: ${bookingdetail.lkRoomCode}',
                          fontsize: 18,
                        ),
                        Divider(
                          thickness: 1,
                        ),
                        TextInformation(
                            detail:
                                'ประเภทการจอง\t\t: ${bookingdetail.typeName}',
                            fontsize: 18),
                        Divider(
                          thickness: 1,
                        ),
                        TextInformation(
                            detail:
                                'ราคา\t\t\t: ${bookingdetail.price.toInt()} บาท',
                            fontsize: 18),
                        Divider(
                          thickness: 1,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: 40,
              color: Theme.of(context).primaryColor,
              width: double.infinity,
              child: TextButton(
                onPressed: () {
                  NavigationRoute.navigateTo(PaymentSelectRoute,
                      arguments: bookingdetail);
                },
                child: Text(
                  'ชำระเงิน',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.white70,
                      fontWeight: FontWeight.bold),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
