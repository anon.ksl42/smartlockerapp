import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class LocateCard extends StatelessWidget {
  final LocationModel objLocate;

  const LocateCard({Key key, @required this.objLocate}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Card(
        shadowColor: HexColor("#D4D4D4"),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            children: [
              TextInformation(
                detail: 'ชื่อสถานที่ : ${objLocate.locateName}',
              ),
            ],
          ),
        ),
      )
    ]);
  }
}
