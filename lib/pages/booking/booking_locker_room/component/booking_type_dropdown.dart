import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class BookingTypeDropdown extends StatelessWidget {
  final bool isChoose;
  final String dropdownValue;
  final Function function;
  final List<String> itemList;

  const BookingTypeDropdown(
      {Key key,
      @required this.isChoose,
      @required this.dropdownValue,
      @required this.function,
      @required this.itemList})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              TextInformation(detail: 'ประเภทการจอง'),
              Container(
                height: 50,
              )
            ],
          ),
          Column(
            children: [
              Container(
                width: 200,
                height: 50,
                padding: EdgeInsets.symmetric(horizontal: 10),
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(width: 1.0, style: BorderStyle.solid),
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  ),
                ),
                child: DropdownButton<String>(
                  isExpanded: true,
                  value: dropdownValue,
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 24,
                  elevation: 24,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'DB-Adman-X',
                      fontSize: 20),
                  onChanged: isChoose
                      ? (String newValue) {
                          function(newValue);
                        }
                      : null,
                  items: itemList.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        textAlign: TextAlign.center,
                      ),
                    );
                  }).toList(),
                ),
              ),
              buttonDetail(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget buttonDetail(BuildContext context) {
    return TextButton(
      onPressed: () {
        NavigationRoute.navigateTo(ServiCeRateRoute);
      },
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 4.0),
            child: Icon(Icons.info_outline),
          ),
          Text(
            'รายละเอียดค่าบริการ',
            style: TextStyle(fontSize: 14),
          )
        ],
      ),
    );
  }
}
