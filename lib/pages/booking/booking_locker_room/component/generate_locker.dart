import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';

class GenerateLockerRoom extends StatelessWidget {
  final int maximumColumn;
  final List<StaggeredTile> staggeredTile;
  final List<Widget> lockerRoom;
  final int maxheight;

  const GenerateLockerRoom(
      {Key key,
      @required this.maximumColumn,
      @required this.staggeredTile,
      @required this.lockerRoom,
      @required this.maxheight})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150 * maxheight.toDouble(),
      decoration: BoxDecoration(
          color: HexColor('#F0E76B'),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: StaggeredGridView.count(
        primary: false,
        crossAxisCount: maximumColumn,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        staggeredTiles: staggeredTile,
        children: lockerRoom,
        padding: const EdgeInsets.all(5.0),
      ),
    );
  }
}
