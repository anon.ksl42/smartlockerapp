// import 'dart:async';
// import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
// import 'package:SmartLockerApp/component/component.dart';
// import 'package:SmartLockerApp/constant/constant.dart';
// import 'package:SmartLockerApp/data/datas.dart';
// import 'package:SmartLockerApp/models/models.dart';
// import 'package:SmartLockerApp/pages/booking/booking_locker_room/component/booking_type_dropdown.dart';
// import 'package:SmartLockerApp/pages/booking/booking_locker_room/component/generate_locker.dart';
// import 'package:SmartLockerApp/pages/booking/booking_locker_room/component/locate_card.dart';
// import 'package:SmartLockerApp/pages/booking/booking_locker_room/component/locker_room_detail.dart';
// import 'package:SmartLockerApp/pages/booking/booking_locker_room/component/status_information.dart';
// import 'package:SmartLockerApp/route/navigation.dart';
// import 'package:SmartLockerApp/services/service.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
// import 'package:hexcolor/hexcolor.dart';

// class BookingLockerRoomPage extends StatefulWidget {
//   final LocationModel objLocate;
//   const BookingLockerRoomPage({Key key, this.objLocate}) : super(key: key);
//   @override
//   _BookingLockerRoomPageState createState() => _BookingLockerRoomPageState();
// }

// class _BookingLockerRoomPageState extends State<BookingLockerRoomPage> {
//   String bookingType;
//   String lockerSize;
//   bool _isLoading = true;
//   bool _isChoose = false;

//   int maxHorizontalLockerRoom;
//   int maxHeight = 1;
//   int crossAxisCellCount = 1;
//   int mainAxisCellCount = 1;

//   LocationModel objLocate;
//   BookingModel bookingDetail = new BookingModel();

//   RateTypeDisplay rateTypeDisplay = RateTypeDisplay();
//   List<LockerRoomModel> lockerRoomList = new List<LockerRoomModel>();
//   List<ServiceRateModel> serviceRateList = new List<ServiceRateModel>();
//   List<Widget> lockerRoom = [];
//   List<StaggeredTile> staggeredTile = [];
//   Map<String, bool> lockerChosen = new Map();

//   chooseLockerRoom(String lockerRoomCode) {
//     setState(() {
//       lockerChosen[lockerRoomCode] = !lockerChosen[lockerRoomCode];
//       _isChoose = lockerChosen[lockerRoomCode];
//       lockerSize = lockerRoomList
//           .where((element) => element.lkRoomCode == lockerRoomCode)
//           .first
//           .lkSizeName;
//       bookingDetail.lkRoomId = lockerRoomList
//           .where((element) => element.lkRoomCode == lockerRoomCode)
//           .first
//           .lkRoomId;
//       for (int index = 0; index < lockerRoomList.length; index++) {
//         if (lockerRoomList[index].lkRoomCode != (lockerRoomCode)) {
//           lockerChosen[lockerRoomList[index].lkRoomCode] = false;
//         }
//       }

//       bookingDetail.lkRoomCode = lockerRoomCode;
//       bookingDetail.typeName = bookingType;
//       bookingDetail.rateTypeId = rateTypeDisplay.rateTypeForData[bookingType];
//       if (lockerChosen[lockerRoomCode]) {
//         for (ServiceRateModel serviceRate in serviceRateList) {
//           if (serviceRate.lkSizeName == lockerSize &&
//               serviceRate.typeName == bookingType) {
//             bookingDetail.price = serviceRate.price;
//             bookingDetail.lkRoomCode = lockerRoomCode;
//           }
//         }
//       } else {
//         bookingDetail.price = 0;
//       }
//       lockerRoomCreate(lockerChosenArg: lockerChosen);
//     });
//   }

//   changeBookingType(String newType) {
//     setState(() {
//       bookingType = newType;
//       bookingDetail.typeName = bookingType;
//       bookingDetail.rateTypeId = rateTypeDisplay.rateTypeForData[newType];
//       for (ServiceRateModel serviceRate in serviceRateList) {
//         if (serviceRate.lkSizeName == lockerSize &&
//             serviceRate.typeName == bookingType) {
//           bookingDetail.price = serviceRate.price;
//         }
//       }
//     });
//   }

//   fetchLockerRoom() async {
//     var content =
//         await LockerService.getLockerByLocateId(widget.objLocate.locateId);
//     int lockerId = content['lockerId'];
//     lockerRoomList = await LockerService.getAllLockerRoomByLockerId(lockerId);
//     lockerRoomCreate();
//   }

//   lockerRoomCreate({Map<String, bool> lockerChosenArg}) {
//     lockerRoom = [];
//     staggeredTile = [];
//     lockerRoomList.sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
//     maxHorizontalLockerRoom = lockerRoomList.last.columnPosition;
//     List<int> sizeLState = [0, 0];
//     int sizeLCount = 0;
//     int sizeMCount = 0;
//     int sizeMDelete = 0;
//     int sizeLDelete = 0;
//     int emptyCount = 0;
//     int heightTimes;
//     lockerRoomList.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
//     for (int rowIndex = 0;
//         rowIndex < lockerRoomList.last.rowPosition;
//         rowIndex++) {
//       List<LockerRoomModel> filterRoom = [];
//       int previousError = 0;
//       heightTimes = 1;
//       for (int rowFilterIndex = 0;
//           rowFilterIndex < lockerRoomList.length;
//           rowFilterIndex++) {
//         if (lockerRoomList[rowFilterIndex].rowPosition == rowIndex + 1) {
//           filterRoom.add((lockerRoomList[rowFilterIndex]));
//         }
//       }
//       filterRoom.sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
//       for (int columnIndex = 0;
//           columnIndex <
//               (maxHorizontalLockerRoom -
//                   sizeMDelete -
//                   sizeLDelete -
//                   emptyCount);
//           columnIndex++) {
//         if (columnIndex >= filterRoom.length &&
//             columnIndex < maxHorizontalLockerRoom) {
//           staggeredTile.add(StaggeredTile.count(1, 1));
//           lockerRoom.add(
//             Container(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(24.0),
//                 color: Colors.transparent,
//               ),
//               child: Center(),
//             ),
//           );
//         } else {
//           if (filterRoom[columnIndex].lkSizeName == 'M') {
//             heightTimes = 2;
//             sizeMCount++;
//           } else if (filterRoom[columnIndex].lkSizeName == 'L') {
//             heightTimes = 3;
//             sizeLCount++;
//           }
//           bool roomGenComplete = false;

//           int indexVisual = columnIndex + previousError;
//           while (!roomGenComplete) {
//             if (filterRoom[columnIndex].columnPosition - indexVisual == 1) {
//               roomGenComplete = true;
//               lockerChosen[filterRoom[columnIndex].lkRoomCode] =
//                   lockerChosenArg == null
//                       ? false
//                       : lockerChosenArg[filterRoom[columnIndex].lkRoomCode];
//               maxHeight = filterRoom[columnIndex].columnPosition + heightTimes >
//                       maxHeight
//                   ? filterRoom[columnIndex].columnPosition + heightTimes
//                   : maxHeight;
//               staggeredTile.add(StaggeredTile.count(1, 1 * heightTimes));
//               lockerRoom.add(
//                 GestureDetector(
//                   onTap: () => filterRoom[columnIndex].status == 'A'
//                       ? chooseLockerRoom(filterRoom[columnIndex].lkRoomCode)
//                       : null,
//                   child: Container(
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(10.0),
//                       color: filterRoom[columnIndex].status == 'A'
//                           ? HexColor('#26B73F')
//                           : filterRoom[columnIndex].status == 'B'
//                               ? HexColor('##C22A2A')
//                               : filterRoom[columnIndex].status == 'F'
//                                   ? Colors.black
//                                   : Colors.transparent,
//                     ),
//                     child: Center(
//                       child: lockerChosen[filterRoom[columnIndex].lkRoomCode]
//                           ? Icon(
//                               Icons.check,
//                               color: Colors.white,
//                               size: 40,
//                             )
//                           : Text(
//                               filterRoom[columnIndex].lkRoomCode,
//                               style:
//                                   TextStyle(fontSize: 20, color: Colors.white),
//                             ),
//                     ),
//                   ),
//                 ),
//               );
//             } else {
//               indexVisual++;
//               previousError++;
//               emptyCount++;
//               staggeredTile.add(StaggeredTile.count(1, 1));
//               lockerRoom.add(
//                 Container(
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(24.0),
//                     color: Colors.transparent,
//                   ),
//                   child: Center(),
//                 ),
//               );
//             }
//           }
//         }
//       }
//       //minus sizeLDelete
//       if (sizeLDelete > 0 && sizeLState[1] > 0) {
//         sizeLDelete = sizeLDelete - sizeLState[1];
//       }
//       sizeLState[1] = sizeLState[0];
//       //plus sizeLDelete
//       sizeLState[0] = sizeLCount;
//       sizeLDelete = sizeLDelete + sizeLState[0];

//       //minus sizeMDelete
//       if (sizeMDelete > 0) {
//         sizeMDelete = 0;
//       }
//       //plus sizeLDelete
//       sizeMDelete = sizeMDelete + sizeMCount;
//       sizeLCount = 0;
//       sizeMCount = 0;
//       emptyCount = 0;
//     }
//   }

//   fetchType() async {
//     await rateTypeDisplay.initialType();
//     bookingType = rateTypeDisplay.typeListForShow[0];
//   }

//   fetchServiceCharge() async {
//     await ServiceRate.initialServiceRateData();
//     serviceRateList = ServiceRate.serviceRateList;
//   }

//   void startTimer() {
//     Timer.periodic(const Duration(seconds: 4), (t) {
//       setState(() {
//         _isLoading = false; //set loading to false
//       });
//       t.cancel(); //stops the timer
//     });
//   }

//   @override
//   void initState() {
//     fetchType();
//     fetchLockerRoom();
//     fetchServiceCharge();
//     bookingDetail.locateName = widget.objLocate.locateName;
//     bookingDetail.price = 0;

//     startTimer();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     // final LocationModel objLocate = ModalRoute.of(context).settings.arguments;
//     return Scaffold(
//       appBar: AppbarCommon(
//           title: 'จองล็อคเกอร์',
//           function: () => Navigator.pop(context)).build(context),
//       body: _isLoading
//           ? Loader()
//           : SingleChildScrollView(
//               reverse: _isChoose,
//               child: Column(
//                 children: [
//                   Container(
//                     padding: EdgeInsets.all(15),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         LocateCard(objLocate: widget.objLocate),
//                         Container(
//                           padding: EdgeInsets.symmetric(vertical: 20),
//                           child: Divider(
//                             thickness: 2,
//                             height: 2,
//                           ),
//                         ),
//                         Text(
//                           'เลือกช่องล็อคเกอร์ที่ต้องการจอง',
//                           style: TextStyle(fontSize: 20),
//                         ),
//                         StatusInformation(),
//                         GenerateLockerRoom(
//                           maximumColumn: maxHorizontalLockerRoom,
//                           lockerRoom: lockerRoom,
//                           maxheight: maxHeight,
//                           staggeredTile: staggeredTile,
//                         ),
//                         _isChoose
//                             ? BookingTypeDropdown(
//                                 itemList: rateTypeDisplay.typeListForShow,
//                                 dropdownValue: bookingType,
//                                 function: changeBookingType,
//                                 isChoose: _isChoose,
//                               )
//                             : Container(),
//                         Divider(
//                           thickness: 2,
//                           height: 2,
//                           endIndent: 10,
//                           indent: 10,
//                         ),
//                         _isChoose
//                             ? LockerRoomDetail(
//                                 detail: bookingDetail.lkRoomCode,
//                                 topic: 'ช่องล็อคเกอร์ที่จอง',
//                               )
//                             : Container(),
//                         _isChoose
//                             ? LockerRoomDetail(
//                                 detail: bookingDetail.price.toString() + ' บาท',
//                                 topic: 'ราคา',
//                               )
//                             : Container(),
//                       ],
//                     ),
//                   ),
//                   SizedBox(
//                     height: 15,
//                   ),
//                   _isChoose
//                       ? Container(
//                           height: 40,
//                           color: Theme.of(context).primaryColor,
//                           width: double.infinity,
//                           child: FlatButton(
//                             onPressed: () {
//                               NavigationRoute.navigateTo(BookingDetailRoute,
//                                   arguments: bookingDetail);
//                             },
//                             child: Text(
//                               'ชำระเงิน',
//                               textAlign: TextAlign.center,
//                               style: TextStyle(
//                                   fontSize: 25,
//                                   color: Colors.white70,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                         )
//                       : Container()
//                 ],
//               ),
//             ),
//     );
//   }
// }
