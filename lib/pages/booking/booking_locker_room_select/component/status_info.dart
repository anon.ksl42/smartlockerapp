import 'package:SmartLockerApp/component/component.dart';
import 'package:flutter/material.dart';

class StatusInfo extends StatelessWidget {
  final double width = 15;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          boxInformation(Colors.greenAccent[700]),
          SizedBox(
            width: width,
          ),
          TextInformation(detail: 'ว่าง'),
          SizedBox(
            width: width,
          ),
          boxInformation(Colors.redAccent[700]),
          SizedBox(
            width: width,
          ),
          TextInformation(detail: 'ไม่ว่าง'),
          SizedBox(
            width: width,
          ),
          boxInformation(Colors.black),
          SizedBox(
            width: width,
          ),
          TextInformation(detail: 'ปรับปรุง'),
        ],
      ),
    );
  }

  Widget boxInformation(Color color) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        border: Border.all(width: 1, color: color.withOpacity(0.8)),
      ),
      child: SizedBox(
        width: 40,
        height: 40,
      ),
    );
  }
}
