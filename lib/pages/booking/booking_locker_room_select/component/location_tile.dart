import 'package:SmartLockerApp/component/component.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class LocationTile extends StatelessWidget {
  final String locateName;
  final String lockerCode;

  const LocationTile({Key key, @required this.locateName, @required this.lockerCode}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Card(
        shadowColor: HexColor("#D4D4D4"),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextInformation(
                detail: 'ชื่อสถานที่ : $locateName',
              ),
              TextInformation(
                detail: 'รหัสล็อคเกอร์ : $lockerCode',
              ),
            ],
          ),
        ),
      )
    ]);
  }
}