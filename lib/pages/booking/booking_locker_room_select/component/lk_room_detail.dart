import 'package:SmartLockerApp/component/component.dart';
import 'package:flutter/material.dart';

class LkRoomDetail extends StatelessWidget {
  final String detail;
  final String topic;

  const LkRoomDetail({Key key, @required this.detail, @required this.topic})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextInformation(detail: topic),
          TextInformation(detail: detail),
        ],
      ),
    );
  }
}
