import 'package:SmartLockerApp/models/locker_room_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hexcolor/hexcolor.dart';

class GenerateLockerRoom extends StatelessWidget {
  final List<LockerRoomModel> lockerRoomList;
  final Map<int, bool> isLockerRoomChoose;
  final Function chooseLockerRoomEvent;

  const GenerateLockerRoom({this.lockerRoomList, this.isLockerRoomChoose, this.chooseLockerRoomEvent});

  int getMaxColumn() {
    lockerRoomList.sort((a, b) => a.columnPosition.compareTo(b.columnPosition));
    return lockerRoomList.last.columnPosition;
  }

  int getMaxRow() {
    lockerRoomList.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
    return lockerRoomList.last.rowPosition;
  }

  double getBaseBackgroundHeight(){
     lockerRoomList.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
     int lastRow = lockerRoomList.last.rowPosition;
     List<LockerRoomModel> lockerRoomLastRow = lockerRoomList.where((element) => element.rowPosition == lastRow).toList();
     var result = lockerRoomLastRow.firstWhere((element) => element.lkSizeName == 'L', orElse: () {return null;},);
     if(result != null){
       return 0.29;
     }
     result = lockerRoomLastRow.firstWhere((element) => element.lkSizeName == 'M', orElse: () {return null;},);
     if(result != null){
       return 0.35;
     }
     return 0.44;
  }
  dynamic genBlockAndStaggeredTile(int functionNo) {
    List<Widget> lockerRoomGenerate = [];
    List<StaggeredTile> staggeredTile = [];

    lockerRoomList.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
    int maxheight = lockerRoomList.last.rowPosition;
    for (int row = 0; row <= maxheight; row++) {
      List<LockerRoomModel> lockerOnlyRow = lockerRoomList
          .where((lk) => lk.rowPosition == row)
          .toList();

      lockerOnlyRow
          .sort((a, b) => a.columnPosition.compareTo(b.columnPosition));

      for (int col = 0; col <= getMaxColumn(); col++) {
        var result =
            lockerOnlyRow.where((element) => element.columnPosition == col);
            if(result.isEmpty) continue;

        if (result.first.lkRoomId == null) {
          if (functionNo == 1) {
            lockerRoomGenerate.add(genEmptyBlock());
          } else if (functionNo == 2) {
            staggeredTile.add(StaggeredTile.count(1, 1));
          }
        } else {
          if (functionNo == 1) {
            lockerRoomGenerate.add(genRoomBlock(result.first));
          } else if (functionNo == 2) {
            staggeredTile.add(genStaggeredTile(result.first));
          }
        }
      }
    }

    if (functionNo == 1) {
      return lockerRoomGenerate;
    } else if (functionNo == 2) {
      return staggeredTile;
    } else {
      return null;
    }

    // for (int i = 0; i <= maxColumnLength; i++) {
    //   int emptyBlockIndex = 0; // for minus row's index loop
    //   //get only column
    //   List<LockerRoomModel> lockerOnlyColumn =
    //       lockerRoomList.where((lk) => lk.columnPosition == i).toList();
    //   lockerOnlyColumn.sort((a, b) => a.rowPosition.compareTo(b.rowPosition));
    //   int currentRow = 0; // for check current row
    //   for (int j = 0;
    //       j <= lockerOnlyColumn.last.rowPosition - emptyBlockIndex;
    //       j++) {
    //     //gen block
    //     if (functionNo == 1) {
    //       //if rowPosition is not equal current index
    //       while (lockerOnlyColumn[j].rowPosition != currentRow) {
    //         lockerRoomArray[currentRow][i] = genEmptyBlock();
    //         currentRow++;
    //         emptyBlockIndex++;
    //       }
    //       lockerRoomArray[currentRow][i] = genRoomBlock(lockerOnlyColumn[j]);
    //       currentRow++;
    //     }
    //     //gen staggeredTile
    //     else if (functionNo == 2) {
    //       while (lockerOnlyColumn[j].rowPosition != currentRow) {
    //         staggeredTileArray[currentRow][i] = StaggeredTile.count(1, 1);
    //         currentRow++;
    //         emptyBlockIndex++;
    //       }
    //       staggeredTileArray[currentRow][i] =
    //           genStaggeredTile(lockerOnlyColumn[j]);
    //       currentRow++;
    //     }
    //   }
    // }
    // if (functionNo == 1) {
    //   for (int row = 0; row < lockerRoomArray.length; row++) {
    //     for (int col = 0; col < lockerRoomArray[row].length; col++) {
    //       if (lockerRoomArray[row][col] == null) {
    //         lockerRoomGenerate.add(genEmptyBlock());
    //       } else {
    //         lockerRoomGenerate.add(lockerRoomArray[row][col]);
    //       }
    //     }
    //   }
    //   return lockerRoomGenerate;
    // } else if (functionNo == 2) {
    //   for (int row = 0; row < staggeredTileArray.length; row++) {
    //     for (int col = 0; col < staggeredTileArray[row].length; col++) {
    //       if (staggeredTileArray[row][col] == null) {
    //         staggeredTile.add(StaggeredTile.count(1, 1));
    //       } else {
    //         staggeredTile.add(staggeredTileArray[row][col]);
    //       }
    //     }
    //   }
    //   return staggeredTile;
    // }
  }

  Widget genRoomBlock(LockerRoomModel lockerRoom) {
    return GestureDetector(
      onTap: () => lockerRoom.status == 'A' ? chooseLockerRoomEvent(lockerRoom) : null,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: lockerRoom.status == 'A'
              ? HexColor('#26B73F')
              : lockerRoom.status == 'B'
                  ? HexColor('##C22A2A')
                  : lockerRoom.status == 'F'
                      ? Colors.black
                      : Colors.transparent,
        ),
        child: Center(
          child: isLockerRoomChoose[lockerRoom.lkRoomId]
              ? Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 40,
                )
              : Text(
                  lockerRoom.lkRoomCode,
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
        ),
      ),
    );
  }

  Widget genEmptyBlock() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(24.0),
        color: Colors.transparent,
      ),
      child: Center(),
    );
  }

  StaggeredTile genStaggeredTile(LockerRoomModel lockerRoom) {
    if (lockerRoom.lkSizeName.toUpperCase() == 'S') {
      return StaggeredTile.count(1, 1);
    } else if (lockerRoom.lkSizeName.toUpperCase() == 'M') {
      return StaggeredTile.count(1, 2);
    } else {
      return StaggeredTile.count(1, 3);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (270 * (getMaxRow() + 1).toDouble()) / (getBaseBackgroundHeight() * (getMaxColumn() + 1).toDouble()),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: HexColor('#F0E76B'),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: StaggeredGridView.count(
        physics: ScrollPhysics(parent: (NeverScrollableScrollPhysics())),
        primary: false,
        crossAxisCount: (getMaxColumn() + 1),
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 5.0,
        staggeredTiles: genBlockAndStaggeredTile(2),
        children: genBlockAndStaggeredTile(1),
        padding: const EdgeInsets.all(5.0),
      ),
    );
  }
}

//(185 * (getMaxRow() + 1).toDouble())
