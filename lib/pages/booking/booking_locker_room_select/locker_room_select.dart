import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/error_function.dart';
import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/fetch_locker_room.dart';
import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/fetch_location.dart';
import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/initial_lk_room_choose.dart';
import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/rate_type_show.dart';
import 'package:SmartLockerApp/bussiness-logic/function/locker_room_select/set_row_col_lk_room.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/route-paths.dart';
import 'package:SmartLockerApp/data/service_rate.dart';
import 'package:SmartLockerApp/models/location_model.dart';
import 'package:SmartLockerApp/models/locker_model.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/booking/booking_locker_room_select/component/generate_locker_room.dart';
import 'package:SmartLockerApp/pages/booking/booking_locker_room_select/component/lk_room_detail.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'component/booking_type_select.dart';
import 'component/location_tile.dart';
import 'component/status_info.dart';

class LockerRoomSelect extends StatefulWidget {
  final LockerModel lockerChoice;
  const LockerRoomSelect({Key key, this.lockerChoice}) : super(key: key);
  @override
  _LockerRoomSelectState createState() => _LockerRoomSelectState(lockerChoice);
}

class _LockerRoomSelectState extends State<LockerRoomSelect> {
  final LockerModel lockerChoice;

  BookingModel bookingDetail = new BookingModel();
  LocationModel lockerLocation = new LocationModel();
  List<LockerRoomModel> lockerRoomList = [];
  List<LockerRoomModel> lockerRoomListForGenerate = [];
  Map<int, bool> isLockerRoomChoose = new Map<int, bool>();
  RateTypeShow rateTypeShow = RateTypeShow();

  String bookingType;
  String lockerSize;

  bool isLoading = true;

  _LockerRoomSelectState(this.lockerChoice);

  @override
  void initState() {
    ServiceRate.initialServiceRateData();
    rateTypeShow
        .initialType()
        .then((_) => bookingType = rateTypeShow.typeListForShow[0])
        .catchError((error) {
      errorFunction(error);
    });
    fetchLocation(lockerChoice.locateId).catchError((error) {
      errorFunction(error);
    }).then((value) {
      lockerLocation = value;
      bookingDetail.locateName = lockerLocation.locateName;
      bookingDetail.lockerCode = lockerChoice.lockerCode;
    });
    fetchLockerRoom(lockerChoice.lockerId)
        .catchError((error) {errorFunction(error);} )
        .then((value) {
      setState(() {
        lockerRoomList = value;
        lockerRoomListForGenerate = setIndexLockerRoom(lockerRoomList);
        isLockerRoomChoose = initialIsLockerRoomChoose(lockerRoomList);
      });
      timer();
      super.initState();
    });
  }

  void timer() async {
    await Future.delayed(Duration(seconds: 2)).then((value) {
      setState(() {
        isLoading = false;
      });
    });
  }

  void changeLockerChoose(LockerRoomModel lockerRoom) {
    setState(() {
      //set change locker room's choose
      isLockerRoomChoose.forEach((key, value) {
        if (key != lockerRoom.lkRoomId) {
          isLockerRoomChoose[key] = false;
        } else {
          isLockerRoomChoose[key] = !value;
        }
      });
      //set lockersize and lkroomId
      lockerSize = lockerRoom.lkSizeName;
      bookingDetail.lkRoomId = lockerRoom.lkRoomId;
      bookingDetail.lkRoomCode = lockerRoom.lkRoomCode;
      bookingDetail.typeName = bookingType;
      bookingDetail.rateTypeId = rateTypeShow.rateTypeForData[bookingType];

      if (isLockerRoomChoose[lockerRoom.lkRoomId]) {
        for (ServiceRateModel serviceRate in ServiceRate.serviceRateList) {
          if (serviceRate.lkSizeName == lockerSize &&
              serviceRate.typeName == bookingType) {
            bookingDetail.price = serviceRate.price;
          }
        }
      } else {
        bookingDetail.price = 0;
      }
    });
  }

  bool isChoose() {
    bool _isChoose = false;
    isLockerRoomChoose.forEach((key, value) {
      _isChoose = _isChoose || value;
    });
    return _isChoose;
  }

  changeBookingType(String newType) {
    setState(() {
      bookingType = newType;
      bookingDetail.typeName = bookingType;
      bookingDetail.rateTypeId = rateTypeShow.rateTypeForData[newType];
      for (ServiceRateModel serviceRate in ServiceRate.serviceRateList) {
        if (serviceRate.lkSizeName == lockerSize &&
            serviceRate.typeName == bookingType) {
          bookingDetail.price = serviceRate.price;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
          title: 'จองล็อคเกอร์',
          function: () => Navigator.pop(context)).build(context),
      body: isLoading
          ? Loader()
          : SingleChildScrollView(
              reverse: isChoose(),
              child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LocationTile(
                      lockerCode: lockerChoice.lockerCode,
                      locateName: lockerLocation.locateName,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Divider(
                        thickness: 2,
                        height: 2,
                      ),
                    ),
                    Text(
                      'เลือกช่องล็อคเกอร์ที่ต้องการจอง',
                      style: TextStyle(fontSize: 20),
                    ),
                    StatusInfo(),
                    GenerateLockerRoom(
                      lockerRoomList: lockerRoomListForGenerate,
                      isLockerRoomChoose: isLockerRoomChoose,
                      chooseLockerRoomEvent: changeLockerChoose,
                    ),
                    isChoose()
                        ? BookingTypeSelect(
                            bookingTypeList: rateTypeShow.typeListForShow,
                            function: changeBookingType,
                            isChoose: isChoose(),
                            dropdownValue: bookingType,
                          )
                        : Container(),
                    Divider(
                      thickness: 2,
                      height: 2,
                      endIndent: 10,
                      indent: 10,
                    ),
                    isChoose()
                        ? LkRoomDetail(
                            detail: bookingDetail.lkRoomCode,
                            topic: 'ช่องล็อคเกอร์ที่จอง',
                          )
                        : Container(),
                    isChoose()
                        ? LkRoomDetail(
                            detail: bookingDetail.price.toString() + ' บาท',
                            topic: 'ราคา',
                          )
                        : Container(),
                    SizedBox(
                      height: 15,
                    ),
                    isChoose()
                        ? Container(
                            height: 40,
                            color: Theme.of(context).primaryColor,
                            width: double.infinity,
                            child: TextButton(
                              onPressed: () {
                                NavigationRoute.navigateTo(BookingDetailRoute,
                                    arguments: bookingDetail);
                              },
                              child: Text(
                                'ชำระเงิน',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white70,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
    );
  }
}
