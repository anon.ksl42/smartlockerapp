import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class FinishBody extends StatelessWidget {
  final int surplusHoursResult;
  final int minuteTimeUsage;
  final int surPlusPrice;
  final String typeName;
  final BookingModel bookingDetail;

  const FinishBody(
      {Key key,
      @required this.surplusHoursResult,
      @required this.minuteTimeUsage,
      @required this.surPlusPrice,
      @required this.typeName,
      @required this.bookingDetail})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Container(
                alignment: Alignment.bottomLeft,
                child: TextInformation(
                    detail: 'ยืนยันข้อมูลทำเรื่องปลดล็อค', fontsize: 22),
              ),
              Container(
                width: double.maxFinite,
                margin: EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey.withOpacity(0.7),
                    width: 2,
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextInformation(
                          detail:
                              'ชื่อสถานที่\t\t\t: ${bookingDetail.locateName}',
                          fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                      TextInformation(
                          detail:
                              'รหัสล็อคเกอร์ที่จอง\t: ${bookingDetail.lkRoomCode}',
                          fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                      TextInformation(
                          detail: 'ประเภทการจอง\t\t: $typeName', fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                      TextInformation(
                          detail:
                              'จำนวนเวลาที่ใช้งาน\t\t: ${(minuteTimeUsage / 60).floor()} ชั่วโมง ${minuteTimeUsage % 60} นาที',
                          fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                      TextInformation(
                          detail:
                              'ชั่วโมงส่วนเกิน\t\t: ${surplusHoursResult == 0 ? 'ไม่เกินชั่วโมงเริ่มต้น' : surplusHoursResult.toString() + ' ชั่วโมง'}',
                          fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                      TextInformation(
                          detail:
                              'ราคาจากชั่วโมงส่วนเกิน\t\t\t: ${surPlusPrice == 0 ? 'ไม่คิดค่าบริการ' : surPlusPrice.toString() + ' บาท'}',
                          fontsize: 16),
                      Divider(
                        thickness: 1,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 40,
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            child: TextButton(
              onPressed: () {
                BookingModel bookingdetail = bookingDetail;
                bookingdetail.price = surPlusPrice.ceilToDouble();
                if (surPlusPrice == 0) {
                  bookingdetail.accountId = AccountGlobal.getAccountId();

                  NavigationRoute.navigateAndRemoveUntil(ResultRoute,
                      arguments: bookingdetail);
                } else {
                  NavigationRoute.navigateAndRemoveUntil(PaymentSelectRoute,
                      arguments: bookingdetail);
                }
              },
              child: Text(
                'ทำเรื่องปลดล็อค',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.white70,
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
