import 'package:SmartLockerApp/bussiness-logic/function/finish_booking_bl/finish_booking_bl.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/component/loader.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/booking/finish_booking_detail/component/finish_booking_detail._body.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class FinishBookingDetailPage extends StatefulWidget {
  final BookingModel bookingDetail;

  const FinishBookingDetailPage({Key key, this.bookingDetail})
      : super(key: key);

  @override
  _FinishBookingDetailPageState createState() =>
      _FinishBookingDetailPageState(bookingDetail);
}

class _FinishBookingDetailPageState extends State<FinishBookingDetailPage> {
  final BookingModel bookingDetail;
  FinishBookingBL _finishBookingBL = FinishBookingBL();
  int surplusHoursResult = 0;
  int surPlusPrice = 0;
  int minuteTimeUsage = 0;
  String typeName;

  _FinishBookingDetailPageState(this.bookingDetail);
  _setInitaialData(ServiceRateModel model) {
    DateTime expiredDate =
        bookingDetail.startDate.add(Duration(hours: model.time));
    minuteTimeUsage =
        DateTime.now().difference(bookingDetail.startDate).inMinutes;
    if (DateTime.now().isAfter(expiredDate)) {
      int surplusMinutes = DateTime.now().difference(expiredDate).inMinutes;
      surplusHoursResult = (surplusMinutes / 60).ceil();
    }
    surPlusPrice = (surplusHoursResult * model.surplusPrice).ceil();
    typeName = RateType.findTypeNameByTypeId(bookingDetail.rateTypeId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'ปลดล็อคล็อคเกอร์',
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: FutureBuilder(
        future: _finishBookingBL.getServiceChargeByLockerRoomIdAndTypeId(
            lkRoomId: bookingDetail.lkRoomId, typeId: bookingDetail.rateTypeId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              _setInitaialData(snapshot.data);
              return FinishBody(
                  minuteTimeUsage: minuteTimeUsage,
                  surplusHoursResult: surplusHoursResult,
                  surPlusPrice: surPlusPrice,
                  typeName: typeName,
                  bookingDetail: bookingDetail);
            } else if (snapshot.hasError) {
              if (snapshot.error == APIStatus.UNAUTHRIZE) {
                errorAlert(
                    wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                    function: () =>
                        NavigationRoute.navigateAndRemoveUntil(LoginRoute));
              } else if (snapshot.error == APIStatus.TIME_OUT) {
                errorAlert(
                  wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
                );
              } else if (snapshot.error == APIStatus.SYETEM_ERROR) {
                errorAlert(
                  wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                );
              }
              return Container();
            }
          } else {
            return Container();
          }
          return Container();
        },
      ),
    );
  }
}
