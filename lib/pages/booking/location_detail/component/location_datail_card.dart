import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class LocationCard extends StatelessWidget {
  final LocationModel objLocate;

  const LocationCard({Key key, this.objLocate}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: HexColor("#D4D4D4"),
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            Text(
              "รายละเอียดล็อกเกอร์",
              style: TextStyle(fontSize: 28.0),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ชื่อสถานที่: ${objLocate.locateName}",
                  style: TextStyle(fontSize: 20),
                ),
                Text(
                    "ที่อยู่: ${objLocate.subDistrict} ${objLocate.district} ${objLocate.province} ${objLocate.postalCode}",
                    style: TextStyle(fontSize: 20)),
              ],
            ),
            // SizedBox(
            //   width: 200,
            //   child: FlatButton(
            //     onPressed: () => NavigationRoute.navigateTo(
            //         BookingLockerRoomRoute,
            //         arguments: objLocate),
            //     child: Text(
            //       "ดำเนินการจอง",
            //       style: TextStyle(fontSize: 20),
            //     ),
            //     color: HexColor("#F2B742"),
            //     textColor: Colors.white,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
