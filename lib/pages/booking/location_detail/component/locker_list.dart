import 'package:SmartLockerApp/constant/route-paths.dart';
import 'package:SmartLockerApp/models/locker_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class LockerList extends StatelessWidget {
  final List<LockerModel> lockerChoice;

  const LockerList({Key key, this.lockerChoice}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical:20 , horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('เลือกล็อคเกอร์ที่ต้องการจอง' , style: TextStyle(fontSize: 24),),
          ListView.builder(
            itemCount: lockerChoice.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ListTile(
                leading: Icon(Icons.square_foot),
                title: Text(lockerChoice[index].lockerCode , style:TextStyle(fontSize: 20) ,),
                onTap:() => NavigationRoute.navigateTo(
                    BookingLockerRoomRoute,
                    arguments: lockerChoice[index]),
              );
            },
          ),
        ],
      ),
    );
  }
}
