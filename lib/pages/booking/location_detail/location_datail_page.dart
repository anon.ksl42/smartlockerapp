import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/locker_model.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/booking/location_detail/component/location_datail_card.dart';
import 'package:SmartLockerApp/services/locker_service.dart';
import 'package:flutter/material.dart';

import 'component/locker_list.dart';

class LocationDetailPage extends StatefulWidget {
  final LocationModel objLocate;
  const LocationDetailPage({Key key, @required this.objLocate})
      : super(key: key);

  @override
  _LocationDetailPageState createState() => _LocationDetailPageState(objLocate);
}

class _LocationDetailPageState extends State<LocationDetailPage> {
  final LocationModel objLocate;
  List<LockerModel> lockerChoice = [];

  _LocationDetailPageState(this.objLocate);
  @override
  void initState() {
    super.initState();
    
  }

  Future<List<LockerModel>> fetchLockerList() async {
    return await LockerService.getLockerByLocateId(objLocate.locateId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
          title: 'จองล็อกเกอร์',
          function: () => Navigator.of(context).pop()).build(context),
      body: FutureBuilder(
        future: fetchLockerList() ,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else if (snapshot.hasError) {
            return Container();
          } else if (snapshot.hasData) {
            lockerChoice = snapshot.data;
            return ListView(
              padding: EdgeInsets.all(20.0),
              children: [
                SizedBox(
                  height: 20,
                ),
                LocationCard(objLocate: objLocate),
                LockerList(lockerChoice: lockerChoice)
              ],
            );
          } else {
            return Loader();
          }
        },
      ),
    );
  }
}
