import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/pages/booking/searching_option/component/card.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class SearchingOptionPage extends StatefulWidget {
  @override
  _SearchingOptionPageState createState() => _SearchingOptionPageState();
}

class _SearchingOptionPageState extends State<SearchingOptionPage> {
  Location _location = Location();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#FFFFFF"),
      appBar: AppbarCommon(
          title: 'จองล็อกเกอร์',
          function: () => Navigator.of(context).pop()).build(context),
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: [
          SizedBox(
            height: 20,
          ),
          CardType(
              link: '',
              imageName: 'QRCODE.png',
              title: 'จองผ่าน QR CODE',
              function: _location.scanQR),
          SizedBox(
            height: 10,
          ),
          CardType(
              link: SearchRoute,
              imageName: 'Map.png',
              title: 'ค้นหาล็อกเกอร์',
              function: null)
        ],
      ),
    );
  }
}
