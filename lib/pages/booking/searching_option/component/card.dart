import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class CardType extends StatelessWidget {
  final String link;
  final String imageName;
  final String title;
  final Function function;

  const CardType(
      {Key key,
      @required this.link,
      @required this.imageName,
      @required this.title,
      @required this.function})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: TextButton(
        onPressed: () {
          if (link == '') {
            function();
          } else {
            NavigationRoute.navigateTo(SearchRoute);
          }
        },
        child: Center(
          child: Column(
            children: [
              Container(
                width: 150,
                height: 150,
                child: Image.asset('assets/images/$imageName'),
              ),
              Text(
                title,
                style: TextStyle(fontSize: 30.0 , color: Colors.black),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
