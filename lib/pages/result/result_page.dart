import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/pages/result/component/result_wording.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/schedule/validate_auto.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ResultPage extends StatefulWidget {
  final BookingModel bookingModel;

  const ResultPage({Key key, this.bookingModel}) : super(key: key);
  @override
  _ResultPageState createState() => _ResultPageState(bookingModel);
}

class _ResultPageState extends State<ResultPage> {
  final BookingModel bookingModel;
  BookingModel bookingAfterAction = BookingModel();
  bool _isLoading = true;
  bool _isError = false;
  ResultWording _resultWording;

  Booking _booking = Booking();

  _ResultPageState(this.bookingModel);

  validateFunction() async {
    //add booking
    if (bookingModel.status == null) {
      await _booking.addBooking(bookingModel).then((value) {
        ValidateBookingAuto.bookingTimer(value.bookingId);
        setState(() {
          bookingAfterAction = value;
          _isLoading = false;
          _resultWording = ResultWording(bookingAfterAction);
        });
      }).catchError((error) {
        _isError = true;
        _isLoading = false;
        if (error == APIStatus.UNAUTHRIZE) {
          errorAlert(
              wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
              function: () =>
                  NavigationRoute.navigateAndRemoveUntil(LoginRoute));
        } else if (error == APIStatus.TIME_OUT) {
          errorAlert(
            wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
          );
        } else if (error == APIStatus.SYETEM_ERROR) {
          errorAlert(
            wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
          );
        }

        setState(() {});
      });
      //update booking to waitFinish
    } else if (bookingModel.status == 'P') {
      await _booking.updateBooking(bookingModel.bookingId, 'WF').then((value) {
        ValidateBookingAuto.finishUsingTimer(value.bookingId);
        setState(() {
          bookingAfterAction = value;
          _isLoading = false;
          _resultWording = ResultWording(bookingAfterAction);
        });
      }).catchError((error) {
        _isError = true;
        _isLoading = false;
        if (error == APIStatus.UNAUTHRIZE) {
          errorAlert(
              wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
              function: () =>
                  NavigationRoute.navigateAndRemoveUntil(LoginRoute));
        } else if (error == APIStatus.TIME_OUT) {
          errorAlert(
            wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
          );
        } else if (error == APIStatus.SYETEM_ERROR) {
          errorAlert(
            wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
          );
        }
        setState(() {});
      });
    }
  }

  @override
  void initState() {
    _resultWording = ResultWording(bookingModel);
    validateFunction();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        NavigationRoute.navigateAndRemoveUntil(MainRoute);
        return false;
      },
      child: Scaffold(
        appBar: AppbarCommon(
          title: _resultWording.title,
          function: null,
        ).build(context),
        body: _isError
            ? Container()
            : _isLoading
                ? Loader()
                : Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          child: Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(
                                      top: 10, left: 20, right: 20, bottom: 10),
                                  width: 150,
                                  child: Image.asset(
                                    'assets/images/done.png',
                                  )),
                              Text(
                                bookingModel.transaction == null
                                    ? 'ทำเรื่องปลดล็อคเรียบร้อย'
                                    : ' ชำระเงินเรียบร้อย',
                                style: TextStyle(
                                    fontSize: 25, color: HexColor('#756E6E')),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '${bookingModel.transaction == null ? '' : 'เลขที่่ธุรกรรม : ' + bookingModel.transaction + '\n'}วันที่ดำเนินการ : ${dateTimeToThai(_resultWording.dateTime)} ',
                                style: TextStyle(
                                    fontSize: 14, color: HexColor('#756E6E')),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: HexColor('#707070'), width: 1)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Passcode สำหรับการใช้งาน',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 20),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  '${bookingAfterAction.passcode}',
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 45),
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'สำหรับล็อคเกอร์หมายเลข ${bookingModel.lkRoomCode}',
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'ใช้งานก่อน ${dateTimeToThai(_resultWording.dateTime.add(Duration(minutes: _resultWording.addExpireDate)))}',
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  _resultWording.description,
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
        floatingActionButton: _isLoading
            ? null
            : Padding(
                padding: EdgeInsets.only(bottom: 30),
                child: FloatingActionButton(
                  onPressed: () {
                    NavigationRoute.navigateAndRemoveUntil(MainRoute);
                  },
                  child: Icon(Icons.home),
                ),
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
