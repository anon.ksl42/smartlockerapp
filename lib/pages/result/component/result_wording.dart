import 'package:SmartLockerApp/models/booking_model.dart';

class ResultWording {
  String title;
  int addExpireDate;
  String description;
  DateTime dateTime;

  ResultWording(BookingModel bookingModel) {
    title = bookingModel.status == 'WP' ? 'จองล็อคเกอร์' : 'ปลดล็อคล็อคเกอร์';
    addExpireDate = bookingModel.status == 'WP' ? 60 : 15;
    dateTime = bookingModel.status == 'WP'
        ? bookingModel.createDate
        : bookingModel.updateDate;
    description = bookingModel.status == 'WP'
        ? 'Passcode นี้มีอายุการใช้งาน 60 นาที หากไม่ได้ใช้งานจน Passcode หมดอายุ ระบบจะทำการยกเลิกการจองและจะคืนเงินให้บางส่วน และ Passcode นี้ควรเก็บความลับ'
        : 'Passcode นี้มีอายุการใช้งาน 15 นาที หากไม่ได้ใช้งานจน Passcode หมดอายุ หากจะขอ Passcode ใหม่หลังจาก Passcode เดิมหมดอายุ จะต้องชำระเงินส่วนเกินอีกครั้ง และ Passcode นี้ควรเก็บความลับ';
  }

  void initialWording(BookingModel bookingModel) {}
}
