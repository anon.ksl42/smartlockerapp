import 'package:SmartLockerApp/constant/constant.dart';
import 'package:flutter/material.dart';

class RadioCardSelect extends StatelessWidget {
  final Function newValueFunction;
  final CardCharacter character;

  const RadioCardSelect({Key key, this.newValueFunction, this.character})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'ประเภทบัตร : ',
          style: TextStyle(fontSize: 18),
        ),
        Radio(
          value: CardCharacter.mastercard,
          groupValue: character,
          onChanged: (val) {
            newValueFunction(val);
          },
        ),
        Text(
          'MasterCard',
          style: new TextStyle(fontSize: 17.0),
        ),
        SizedBox(
          width: 20,
        ),
        Radio(
          value: CardCharacter.visa,
          groupValue: character,
          onChanged: (val) {
            newValueFunction(val);
          },
        ),
        Text(
          'Visa',
          style: new TextStyle(fontSize: 17.0),
        ),
      ],
    );
  }
}
