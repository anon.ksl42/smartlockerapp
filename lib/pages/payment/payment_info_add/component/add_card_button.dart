import 'package:flutter/material.dart';

class AddCardButton extends StatelessWidget {
  final String labal;
  final Function function;

  AddCardButton({@required this.labal, @required this.function});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 50,
      color: Theme.of(context).primaryColor,
      child: TextButton(
        onPressed: function,
        child: Text(
          labal,
          style: TextStyle(
              color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
