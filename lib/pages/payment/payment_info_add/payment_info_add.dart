import 'package:SmartLockerApp/bussiness-logic/function/payment_bl.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/pages/payment/payment_info_add/component/add_card_button.dart';
import 'package:SmartLockerApp/pages/payment/payment_info_add/component/radio_card_type.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';

class PaymentInfoAddPage extends StatefulWidget {
  @override
  _PaymentInfoAddPageState createState() => _PaymentInfoAddPageState();
}

class _PaymentInfoAddPageState extends State<PaymentInfoAddPage> {
  PaymentInfoValidate _paymentInfoValidate = PaymentInfoValidate();

  void setNewCard(val) {
    setState(() {
      _paymentInfoValidate.character = val;
    });
  }

  void addCard() async {
    bool isAllvarify = _paymentInfoValidate.isAllCardDetailVerify();
    if (isAllvarify) {
      SweetAlert.show(context,
          subtitle: 'กำลังเพิ่มข้อมูล', style: SweetAlertStyle.loading);

      await _paymentInfoValidate.addPaymentInfo().then((value) {
        SweetAlert.show(
          context,
          subtitle: 'เพิ่มข้อมูลสำเร็จ',
          style: SweetAlertStyle.success,
          confirmButtonText: 'ตกลง',
          onPress: (_) {
            Navigator.of(context).pop();
            return true;
          },
        );
      }).catchError((error) {
        if (error == APIStatus.UNAUTHRIZE) {
          errorAlert(
              wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
              function: () =>
                  NavigationRoute.navigateAndRemoveUntil(LoginRoute));
        } else if (error == APIStatus.TIME_OUT) {
          errorAlert(
            wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
          );
        } else if (error == APIStatus.SYETEM_ERROR) {
          errorAlert(
            wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
          );
        }
      });
    } else {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'เพิ่มบัตรเครดิด / เดบิต',
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: TextInformation(
                  detail: 'กรุณากรอกข้อมูลให้ครบถ้วน',
                  fontsize: 24,
                ),
              ),
              RadioCardSelect(
                character: _paymentInfoValidate.character,
                newValueFunction: setNewCard,
              ),
              TextInputTemplate(
                  versionBorder: 2,
                  controller: _paymentInfoValidate.nameController,
                  errorTextPattern: null,
                  errorTextInvalid: 'กรุณากรอกชื่อ',
                  icon: null,
                  labelText: 'ชื่อ-สกุลตามบัตร',
                  validatePattern: false,
                  validatevalid: _paymentInfoValidate.isNameInvalid),
              TextInputTemplate(
                  versionBorder: 2,
                  controller: _paymentInfoValidate.cardNumController,
                  errorTextPattern: 'รูปแบบเลขบัตรไม่ถูกต้อง',
                  errorTextInvalid: 'กรุณากรอกเลขบัตร',
                  icon: null,
                  labelText: 'เลขบัตร',
                  validatePattern: _paymentInfoValidate.isCardNumNotPattern,
                  validatevalid: _paymentInfoValidate.isCardNumInvalid),
              Row(
                children: [
                  Flexible(
                    child: TextInputTemplate(
                        keyboardType: TextInputType.datetime,
                        hintText: 'e.g. 01/2025',
                        versionBorder: 2,
                        controller: _paymentInfoValidate.expiredController,
                        errorTextPattern: 'รูปแบบไม่ถูกต้อง (01/2025)',
                        errorTextInvalid: 'กรุณากรอกหมดอายุ',
                        icon: null,
                        labelText: 'หมดอายุ',
                        validatePattern:
                            _paymentInfoValidate.isExpiredNotPattern,
                        validatevalid: _paymentInfoValidate.isExpiredInvalid),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Flexible(
                    child: TextInputTemplate(
                      keyboardType: TextInputType.numberWithOptions(signed:false,decimal:true),
                        versionBorder: 2,
                        controller: _paymentInfoValidate.cvvController,
                        errorTextPattern: 'เลข CVV ไม่ถูกต้อง',
                        errorTextInvalid: 'กรุณากรอก CVV',
                        icon: null,
                        labelText: 'CVV',
                        validatePattern: _paymentInfoValidate.isCvvNotPattern,
                        validatevalid:
                            false),
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              AddCardButton(
                labal: 'ยืนยัน',
                function: () => addCard(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
