import 'package:SmartLockerApp/bussiness-logic/function/payment_bl.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/component/divider_custom.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:SmartLockerApp/models/payment_info_model.dart';
import 'package:SmartLockerApp/pages/payment/payment_select/component/add_card_tile.dart';
import 'package:SmartLockerApp/pages/payment/payment_select/component/payment_info_list.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/locker_service.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';

class PaymentSelectPage extends StatefulWidget {
  final BookingModel bookingDetail;

  const PaymentSelectPage({Key key, @required this.bookingDetail})
      : super(key: key);
  @override
  _PaymentSelectPageState createState() =>
      _PaymentSelectPageState(bookingDetail);
}

class _PaymentSelectPageState extends State<PaymentSelectPage> {
  final BookingModel bookingDetail;

  Payment _payment = Payment();
  PaymentInfoModel paymentSelected = PaymentInfoModel();
  TextEditingController _cvvController = TextEditingController();

  bool _isLoading = true;
  bool _isError = false;
  bool _isEnable = false;
  bool _isCVVNotPattern = false;

  selectEvent(int index) {
    setState(() {
      for (int i = 0; i < _payment.bookingInfoList.length; i++) {
        if (i == index) {
          _payment.bookingInfoList[i].selected =
              !_payment.bookingInfoList[i].selected;
          if (_payment.bookingInfoList[i].selected) {
            paymentSelected = _payment.bookingInfoList[i];
          }
        } else {
          _payment.bookingInfoList[i].selected = false;
        }
      }
      if (_payment.bookingInfoList[index].selected) {
        _isEnable = true;
      } else {
        _isEnable = false;
      }
    });
  }

  fetchData() async {
    _isLoading = true;
    await _payment.getAllPaymentByAccountId().then((_) {}).catchError((error) {
      _isError = true;
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () =>
                NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
            wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
            function: () =>
                NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
            wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
            function: () =>
                NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute));
      }
    }).whenComplete(() {
      setState(() {
        _isLoading = false;
      });
    });
  }

  _PaymentSelectPageState(this.bookingDetail);

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
          title: 'ชำระเงิน',
          function: () => NavigationRoute.goBack()).build(context),
      body: _isLoading
          ? Loader()
          : _isError
              ? Container()
              : SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            Container(
                              alignment: Alignment.bottomLeft,
                              child: TextInformation(
                                  detail: 'เลือกวิธีการชำระเงิน', fontsize: 22),
                            ),
                            Container(
                              width: double.maxFinite,
                              height: 400,
                              margin: EdgeInsets.symmetric(vertical: 5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey.withOpacity(0.7),
                                  width: 2,
                                ),
                              ),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 15),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          EdgeInsets.only(top: 5, right: 5),
                                      child: Icon(
                                        Icons.credit_card,
                                        size: 30,
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        TextInformation(
                                          detail: 'บัตรเครดิด / บัตรเดบิต ',
                                          fontsize: 18,
                                        ),
                                        DividerCustom(),
                                        PaymentInfoList(
                                            bookingInfoList:
                                                _payment.bookingInfoList,
                                            onLongPressFunction: selectEvent),
                                        AddCardTile(
                                          function: () {
                                            setState(() {
                                              fetchData();
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Row(
                              children: [
                                Text('CVV : ', style: TextStyle(fontSize: 20)),
                                Container(
                                  height: 30,
                                  width: 100,
                                  padding: EdgeInsets.only(left: 10, top: 10),
                                  child: TextInputTemplate(
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              signed: false),
                                      controller: _cvvController,
                                      errorTextPattern: 'กรุณาใส่เลขให้ถูกต้อง',
                                      errorTextInvalid: 'กรุณากรอกอีเมล',
                                      icon: null,
                                      labelText: '',
                                      validatePattern: false,
                                      validatevalid: false),
                                ),
                              ],
                            ),
                          ),
                        ),
                        _isCVVNotPattern
                            ? Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: 8),
                                  child: Text(
                                    'กรุณาใส่เลข ccv ให้ถูกต้อง',
                                    style: TextStyle(
                                        color: Colors.redAccent, fontSize: 15),
                                  ),
                                ),
                              )
                            : Container(),
                        Container(
                          height: 40,
                          color: _isEnable
                              ? Theme.of(context).primaryColor
                              : Colors.grey,
                          width: double.infinity,
                          child: TextButton(
                            onPressed: () async {
                              if (_isEnable) {
                                if (_cvvController.text.length != 3 &&
                                    !TextInputPattern.cvvpattern
                                        .hasMatch(_cvvController.text)) {
                                  setState(() {
                                    _isCVVNotPattern = true;
                                  });
                                  return;
                                }
                                SweetAlert.show(context,
                                    subtitle: 'กำลังชำระเงิน',
                                    style: SweetAlertStyle.loading);
                                paymentSelected.cvv = _cvvController.text;
                                paymentSelected.amount = bookingDetail.price;
                                paymentSelected.accountId =
                                    AccountGlobal.getAccountId();
                                paymentSelected.rateTypeId =
                                    bookingDetail.rateTypeId;
                                paymentSelected.lkRoomId =
                                    bookingDetail.lkRoomId;
                                var errorResult;
                                if (bookingDetail.status == null) {
                                  await LockerService.validateLkRoom(
                                          bookingDetail.lkRoomId)
                                      .then((value) => null,
                                          onError: (error) {
                                    errorResult = error;
                                    print(error.runtimeType);
                                  });
                                }
                                if (errorResult != null) {
                                  //error
                                  if (errorResult == APIStatus.UNAUTHRIZE) {
                                    errorAlert(
                                        wording:
                                            'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                                        function: () => NavigationRoute
                                            .navigateAndRemoveUntil(
                                                routePath.LoginRoute));
                                  } else if (errorResult ==
                                      APIStatus.TIME_OUT) {
                                    errorAlert(
                                        wording:
                                            'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
                                        function: () => NavigationRoute
                                            .navigateAndRemoveUntil(
                                                routePath.MainRoute));
                                  } else if (errorResult ==
                                      APIStatus.CONFLICT) {
                                    errorAlert(
                                        wording:
                                            'ช่องล็อคเกอร์นี้มีผู้อื่นจองแล้วหรือมีการปรับปรุง',
                                        function: () => NavigationRoute
                                            .navigateAndRemoveUntil(
                                                routePath.MainRoute));
                                  } else if (errorResult ==
                                      APIStatus.SYETEM_ERROR){
                                    errorAlert(
                                        wording:
                                            'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                                        function: () => NavigationRoute
                                            .navigateAndRemoveUntil(
                                                routePath.MainRoute));
                                  }
                                } else {
                                  await _payment.payment(paymentSelected).then(
                                      (value) {
                                    bookingDetail.transaction = value;
                                    bookingDetail.accountId =
                                        AccountGlobal.getAccountId();
                                    if (bookingDetail.status == null) {
                                      MyNotification.bookingNotification(
                                          bookingDetail);
                                    }
                                    NavigationRoute.navigateAndRemoveUntil(
                                        ResultRoute,
                                        arguments: bookingDetail);
                                  }).catchError(
                                      (error) => _payment.paymentError(error));
                                }
                              }
                            },
                            child: Text(
                              'ชำระเงิน',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }
}
