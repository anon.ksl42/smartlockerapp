import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';

class PaymentInfoList extends StatelessWidget {
  final List<PaymentInfoModel> bookingInfoList;
  final Function onLongPressFunction;

  const PaymentInfoList(
      {Key key,
      @required this.bookingInfoList,
      @required this.onLongPressFunction})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: 260,
      child: ListView(
        children: List.generate(bookingInfoList.length, (index) {
          return ListTile(
            onTap: () => onLongPressFunction(index),
            selected: bookingInfoList[index].selected,
            selectedTileColor: Colors.orange.withOpacity(0.3),
            leading: GestureDetector(
              onTap: () => onLongPressFunction(index),
              child: Image.asset(
                'assets/images/Mastercard.png',
                width: 30,
              ),
            ),
            title: TextInformation(
              detail: bookingInfoList[index].cardNumber.substring(0,6) +'xxxxxx'+ bookingInfoList[index].cardNumber.substring(13),
              fontsize: 18,
            ),
          );
        }),
      ),
    );
  }
}
