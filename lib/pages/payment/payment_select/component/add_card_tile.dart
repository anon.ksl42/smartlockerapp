import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class AddCardTile extends StatelessWidget {
  final Function function;

  const AddCardTile({Key key, @required this.function}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () => {
        NavigationRoute.navigateTo(PaymentInfoAddRoute).then((_) => function())
      }, //NavigationService.navigateTo(),
      child: Row(
        children: [
          Icon(Icons.add_circle_outline, color: Colors.black),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: Text(
              'ADD CARD',
              style: TextStyle(color: Colors.black),
            ),
          )
        ],
      ),
    );
  }
}
