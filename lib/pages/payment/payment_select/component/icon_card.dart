import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';

class IconCard extends StatelessWidget {
  final PaymentInfoModel paymentInfoModel;

  const IconCard({Key key, @required this.paymentInfoModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String icon =
        paymentInfoModel.cardTypeId == 1 ? 'Mastercard.png' : 'visa.png';
    return Image.asset(
      'assets/images/$icon',
    );
  }
}
