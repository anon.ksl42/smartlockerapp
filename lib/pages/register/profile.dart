import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class Profile extends StatelessWidget {
  final Map<RegisterTopic, TextEditingController> registerController;
  final Map<RegisterTopic, bool> registerInvalid;
  final Map<RegisterTopic, bool> registerNotPattern;

  Profile(
      this.registerController, this.registerInvalid, this.registerNotPattern);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#707070'), width: 1)),
      child: Column(
        children: [
          Container(
              padding:
                  EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
              child: Column(children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RichText(
                      text: TextSpan(
                          text: '*',
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 18,
                            fontFamily: 'DB-Adman-X',
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: ' จำเป็นต้องใส่',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                            ),
                          ]),
                    ),
                  ),
                ),
                TextInputTemplate(
                    versionBorder: 2,
                    controller: registerController[RegisterTopic.FIRSTNAME],
                    errorTextPattern: null,
                    errorTextInvalid: 'กรุณากรอกชื่อ',
                    icon: null,
                    labelText: 'ชื่อ *',
                    validatePattern:
                        registerNotPattern[RegisterTopic.FIRSTNAME],
                    validatevalid: registerInvalid[RegisterTopic.FIRSTNAME]),
                TextInputTemplate(
                    versionBorder: 2,
                    controller: registerController[RegisterTopic.LASTNAME],
                    errorTextPattern: null,
                    errorTextInvalid: 'กรุณากรอกนามสกุล',
                    icon: null,
                    labelText: 'นามสกุล *',
                    validatePattern: registerNotPattern[RegisterTopic.LASTNAME],
                    validatevalid: registerInvalid[RegisterTopic.LASTNAME]),
                TextInputTemplate(
                    versionBorder: 2,
                    controller: registerController[RegisterTopic.IDCITIZEN],
                    errorTextPattern: 'เลขบัตรประชาชนไม่ถูกต้อง',
                    errorTextInvalid: 'กรุณากรอกบัตรประชาชน',
                    icon: null,
                    labelText: 'เลขบัตรประชาชน *',
                    validatePattern:
                        registerNotPattern[RegisterTopic.IDCITIZEN],
                    validatevalid: registerInvalid[RegisterTopic.IDCITIZEN]),
              ])),
          Container(
            padding: EdgeInsets.only(top: 20, left: 5, right: 5),
            child: Divider(
              color: HexColor('#707070'),
              thickness: 1,
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 35),
            child: Column(children: [
              TextInputTemplate(
                  versionBorder: 2,
                  controller: registerController[RegisterTopic.EMAIL],
                  errorTextPattern: 'รูปแบบอีเมลไม่ถูกต้อง',
                  errorTextInvalid: 'กรุณากรอกอีเมล',
                  icon: null,
                  labelText: 'อีเมล *',
                  validatePattern: registerNotPattern[RegisterTopic.EMAIL],
                  validatevalid: registerInvalid[RegisterTopic.EMAIL]),
              TextInputTemplate(
                  versionBorder: 2,
                  obscureText: true,
                  controller: registerController[RegisterTopic.PASSWORD],
                  errorTextPattern: 'password 6 - 18 ตัวเท่านั้น',
                  errorTextInvalid: 'กรุณากรอก password',
                  icon: null,
                  labelText: 'รหัสผ่าน *',
                  validatePattern: registerNotPattern[RegisterTopic.PASSWORD],
                  validatevalid: registerInvalid[RegisterTopic.PASSWORD]),
              TextInputTemplate(
                  versionBorder: 2,
                  obscureText: true,
                  controller:
                      registerController[RegisterTopic.CONFIRM_PASSWORD],
                  errorTextPattern: 'รหัสผ่านไม่ตรงกัน',
                  errorTextInvalid: 'กรุณากรอก ยืนยันรหัสผ่าน',
                  icon: null,
                  labelText: 'ยืนยันรหัสผ่าน *',
                  validatePattern:
                      registerNotPattern[RegisterTopic.CONFIRM_PASSWORD],
                  validatevalid:
                      registerInvalid[RegisterTopic.CONFIRM_PASSWORD]),
            ]),
          )
        ],
      ),
    );
  }
}
