import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class OTPDestination extends StatelessWidget {
  final Map<RegisterTopic, TextEditingController> registerController;
  final Map<RegisterTopic, bool> registerInvalid;
  final Map<RegisterTopic, bool> registerNotPattern;

  const OTPDestination(
      this.registerController, this.registerInvalid, this.registerNotPattern);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#707070'), width: 1)),
      child: Column(
        children: [
          SafeArea(
            child: Container(
              padding:
                  EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
              margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 40),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    child: Text(
                      'กรุณากรอกเบอร์โทรศัพท์เพื่อรับ\nรหัส OTP เพื่อยืนยันตัวตน',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  TextInputTemplate(
                      keyboardType: TextInputType.phone,
                      versionBorder: 2,
                      controller: registerController[RegisterTopic.TEL],
                      errorTextPattern: 'เบอร์โทรศัพท์ไม่ถูกต้อง',
                      errorTextInvalid: 'กรุณากรอกเบอร์โทรศัพท์',
                      icon: null,
                      labelText: 'เบอร์โทรสำหรับยืนยันตัวตน',
                      validatePattern: registerNotPattern[RegisterTopic.TEL],
                      validatevalid: registerInvalid[RegisterTopic.TEL]),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
