import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/bussiness-logic/function/otp/otp_function.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/pages/register/complete_register.dart';
import 'package:SmartLockerApp/pages/register/otp_destination.dart';
import 'package:SmartLockerApp/pages/register/otp_validation.dart';
import 'package:SmartLockerApp/pages/register/profile.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:fa_stepper/fa_stepper.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

import '../../component/error-alert.dart';

class RegisterStepper extends StatefulWidget {
  @override
  _RegisterStepperState createState() => _RegisterStepperState();
}

class _RegisterStepperState extends State<RegisterStepper> {
  int _currentStep;
  bool _isNotPattern;

  AccountValidate accountValidate = new AccountValidate();
  Map<RegisterTopic, TextEditingController> registerController;
  Map<RegisterTopic, bool> registerInvalid;
  Map<RegisterTopic, bool> registerNotPattern;

  FAStep _buildStep(
      {@required Widget title,
      FAStepstate state = FAStepstate.indexed,
      bool isActive = false,
      @required Widget content}) {
    return FAStep(
      title: title,
      state: state,
      isActive: isActive,
      content: content,
    );
  }

  _formStep() {
    if (_currentStep == 0) {
      return Profile(registerController, registerInvalid, registerNotPattern);
    } else if (_currentStep == 1) {
      return OTPDestination(
          registerController, registerInvalid, registerNotPattern);
    } else if (_currentStep == 2) {
      return OTPValidation(
          registerController, registerInvalid, registerNotPattern);
    } else if (_currentStep == 3) {
      return CompleteRegister(registerController: registerController);
    }
  }

  _notPatternAndInvalidValidate(RegisterTopic topic) {
    registerInvalid[topic] =
        registerController[topic].text.replaceAll(' ', '').isEmpty;

    switch (topic) {
      case RegisterTopic.FIRSTNAME:
      case RegisterTopic.LASTNAME:
        break;
      case RegisterTopic.IDCITIZEN:
        registerNotPattern[topic] = accountValidate
            .isIdCitizenValidateNotPattern(registerController[topic].text);
        break;
      case RegisterTopic.EMAIL:
        registerNotPattern[topic] =
            accountValidate.isemailNotPattern(registerController[topic].text);
        break;
      case RegisterTopic.PASSWORD:
        registerNotPattern[topic] = accountValidate
            .ispasswordNotPattern(registerController[topic].text);
        break;
      case RegisterTopic.CONFIRM_PASSWORD:
        registerNotPattern[topic] = accountValidate.isConfirmPasswordNotPattern(
            registerController[RegisterTopic.PASSWORD].text,
            registerController[topic].text);
        break;
      case RegisterTopic.TEL:
        registerNotPattern[topic] =
            accountValidate.isTelNotPattern(registerController[topic].text);
        break;
      case RegisterTopic.OTP:
        registerNotPattern[topic] =
            accountValidate.isOTPNotPattern(registerController[topic].text);
        break;
    }
    _isNotPattern = _isNotPattern || registerInvalid[topic];
    _isNotPattern = _isNotPattern || registerNotPattern[topic];
  }

  _validateStep() async {
    AccountValidate accountValidate = new AccountValidate();
    _isNotPattern = false;

    if (_currentStep == 0) {
      _notPatternAndInvalidValidate(RegisterTopic.FIRSTNAME);
      _notPatternAndInvalidValidate(RegisterTopic.LASTNAME);
      _notPatternAndInvalidValidate(RegisterTopic.IDCITIZEN);
      _notPatternAndInvalidValidate(RegisterTopic.EMAIL);
      _notPatternAndInvalidValidate(RegisterTopic.PASSWORD);
      _notPatternAndInvalidValidate(RegisterTopic.CONFIRM_PASSWORD);

      if (!_isNotPattern) {
        var response = await accountValidate
            .isemailDuplicate(registerController[RegisterTopic.EMAIL].text);
        var _isEmailDuplicate =
            accountValidate.emailDuplicateResponseAction(response);
        _isNotPattern = _isNotPattern || _isEmailDuplicate;

        if (!_isNotPattern) {
          _currentStep++;
          NavigationRoute.goBack();
        }
      }
      setState(() {});
    } else if (_currentStep == 1) {
      _notPatternAndInvalidValidate(RegisterTopic.TEL);
      if (!_isNotPattern) {
        String newNumber =
            "+66" + registerController[RegisterTopic.TEL].text.substring(1);
        requestVerifyCode(newNumber);
        _currentStep++;
      }
      setState(() {});
    } else if (_currentStep == 2) {
      _notPatternAndInvalidValidate(RegisterTopic.OTP);
      if (!_isNotPattern) {
        bool validate =
            await verifyPhone(registerController[RegisterTopic.OTP].text);
        if (validate) {
          _currentStep++;
        } else {
          errorAlert(wording: 'OTP ไม่ถูกต้อง');
        }
      }

      setState(() {});
    } else if (_currentStep == 3) {
      NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute);
    }

    if (_currentStep != 3) {}
  }

  @override
  void initState() {
    _currentStep = 0;
    registerController = {
      RegisterTopic.FIRSTNAME: new TextEditingController(),
      RegisterTopic.LASTNAME: new TextEditingController(),
      RegisterTopic.IDCITIZEN: new TextEditingController(),
      RegisterTopic.EMAIL: new TextEditingController(),
      RegisterTopic.PASSWORD: new TextEditingController(),
      RegisterTopic.CONFIRM_PASSWORD: new TextEditingController(),
      RegisterTopic.TEL: new TextEditingController(),
      RegisterTopic.OTP: new TextEditingController(),
    };
    registerInvalid = {
      RegisterTopic.FIRSTNAME: false,
      RegisterTopic.LASTNAME: false,
      RegisterTopic.IDCITIZEN: false,
      RegisterTopic.EMAIL: false,
      RegisterTopic.PASSWORD: false,
      RegisterTopic.CONFIRM_PASSWORD: false,
      RegisterTopic.TEL: false,
      RegisterTopic.OTP: false,
    };
    registerNotPattern = {
      RegisterTopic.FIRSTNAME: false,
      RegisterTopic.LASTNAME: false,
      RegisterTopic.IDCITIZEN: false,
      RegisterTopic.EMAIL: false,
      RegisterTopic.PASSWORD: false,
      RegisterTopic.CONFIRM_PASSWORD: false,
      RegisterTopic.TEL: false,
      RegisterTopic.OTP: false,
    };
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final canCancel = _currentStep > 0;
    // final canContinue = _currentStep < 3;

    const List<String> titles = [
      'ส่วนตัว',
      'รับ OTP',
      'ยืนยัน OTP',
      'เรียบร้อย'
    ];
    const List<String> buttonWordStep = [
      'ถัดไป',
      'ถัดไป',
      'ส่ง OTP',
      'หน้าแรก'
    ];

    return FAStepper(
      titleHeight: 80.0,
      stepNumberColor: HexColor('#F2B741'),
      titleIconArrange: FAStepperTitleIconArrange.column,
      physics: ClampingScrollPhysics(),
      steps: [
        for (var i = 0; i < 4; ++i)
          _buildStep(
            title: Text(titles[i],
                style: TextStyle(
                    fontFamily: 'DB-Adman-X',
                    fontSize: i == _currentStep ? 20 : 18,
                    fontWeight:
                        i == _currentStep ? FontWeight.bold : FontWeight.normal,
                    color: HexColor('#707070'))),
            isActive: i == _currentStep,
            state: i == _currentStep
                ? FAStepstate.editing
                : i < _currentStep
                    ? FAStepstate.complete
                    : FAStepstate.indexed,
            content: _formStep(),
          ),
      ],
      type: FAStepperType.horizontal,
      currentStep: _currentStep,
      onStepContinue: () {
        _validateStep();
      },
      onStepCancel: canCancel ? () => setState(() => --_currentStep) : null,
      controlsBuilder: (BuildContext context,
          {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
        return Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: _currentStep == 3
              ? MainAxisAlignment.center
              : MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _currentStep == 3
                ? Text('')
                : OutlinedButton(
                    onPressed: onStepCancel,
                    child: Text(
                      'ย้อนกลับ',
                      style: Theme.of(context).textTheme.headline1,
                    ),
                  ),
            ElevatedButton(
              onPressed: onStepContinue,
              child: Text(
                buttonWordStep[_currentStep],
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
          ],
        );
      },
    );
  }
}
