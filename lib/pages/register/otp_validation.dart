import 'package:SmartLockerApp/bussiness-logic/function/otp/otp_function.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class OTPValidation extends StatelessWidget {
  final Map<RegisterTopic, TextEditingController> registerController;
  final Map<RegisterTopic, bool> registerInvalid;
  final Map<RegisterTopic, bool> registerNotPattern;
  OTPValidation(
      this.registerController, this.registerInvalid, this.registerNotPattern);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#707070'), width: 1)),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
            margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 40),
            child: Column(
              children: [
                Text(
                  'ระบบได้ส่งรหัส OTP ไปยังเบอร์โทรศัพท์\n${registerController[RegisterTopic.TEL].text}\n กรุณากรอกรหัสเพื่อยืนยันตัวตน',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'หากยังไม่ได้รับรหัสให้กด',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18),
                      ),
                      TextButton(
                        onPressed: () {
                          String newNumber = "+66" +
                              registerController[RegisterTopic.TEL]
                                  .text
                                  .substring(1);
                          requestVerifyCode(newNumber);
                        },
                        child: Text(
                          'ส่งอีกครั้ง',
                          style: TextStyle(
                              fontSize: 15,
                              color: Colors.red,
                              decoration: TextDecoration.underline,
                              decorationColor: Colors.red,
                              decorationThickness: 2),
                        ),
                      )
                    ],
                  ),
                ),
                TextInputTemplate(
                  versionBorder: 2,
                  controller: registerController[RegisterTopic.OTP],
                  errorTextPattern: 'รหัส OTP ไม่ถูกต้อง',
                  errorTextInvalid: 'กรุณากรอก OTP',
                  icon: null,
                  labelText: 'รหัส OTP ยืนยันตัวตน',
                  validatePattern: registerNotPattern[RegisterTopic.OTP],
                  validatevalid: registerInvalid[RegisterTopic.OTP],
                  keyboardType: TextInputType.number,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
