import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class CompleteRegister extends StatefulWidget {
  final Map<RegisterTopic, TextEditingController> registerController;

  const CompleteRegister({Key key, this.registerController}) : super(key: key);
  @override
  _CompleteRegisterState createState() =>
      _CompleteRegisterState(registerController);
}

class _CompleteRegisterState extends State<CompleteRegister> {
  final Map<RegisterTopic, TextEditingController> registerController;
  final Account accountRegister = new Account();
  _CompleteRegisterState(this.registerController);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future:
            accountRegister.register(registerController: registerController),
        builder: (context, snapshop) {
          if (snapshop.connectionState == ConnectionState.waiting) {
            return Container(
                height: 500,
                margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                padding: EdgeInsets.only(bottom: 40),
                decoration: BoxDecoration(
                    border: Border.all(color: HexColor('#707070'), width: 1)),
                child: Center(child: CircularProgressIndicator()));
          } else if (snapshop.connectionState == ConnectionState.done) {
            return Container(
              height: 500,
              margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              padding: EdgeInsets.only(bottom: 40),
              decoration: BoxDecoration(
                  border: Border.all(color: HexColor('#707070'), width: 1)),
              child: Column(
                children: [
                  Container(
                      padding: EdgeInsets.only(
                          top: 10, left: 20, right: 20, bottom: 10),
                      width: 200,
                      height: 200,
                      child: Image.asset(
                        'assets/images/done.png',
                      )),
                  Text(
                    'ยืนยันตัวตนเรียบร้อย \nยินดีต้อนรับสู่ Smart Locker \n',
                    style: TextStyle(fontSize: 25, color: HexColor('#756E6E')),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            );
          } else if (snapshop.error == APIStatus.TIME_OUT) {
            errorAlert(
                wording: 'ไม่สามารถเชื่อมต่อกับระบบและดำเนินการได้',
                function: () => NavigationRoute.navigateAndRemoveUntil(
                    routePath.LoginRoute));
            return Container();
          } else {
            errorAlert(
                wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                function: () => NavigationRoute.navigateAndRemoveUntil(
                    routePath.LoginRoute));
            return Container();
          }
        });
  }
}
