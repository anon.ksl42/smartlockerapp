import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/pages/register/register-stepper.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'สมัครสมาชิก',
        function: () => NavigationRoute.navigatorKey.currentState.pop(),
        functionLabel: 'หน้าแรก',
      ).build(context),
      body: RegisterStepper(),
    );
  }
}
