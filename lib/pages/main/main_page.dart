import 'package:SmartLockerApp/bussiness-logic/function/main_bl.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  MainNav _mainNav = MainNav();
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AnimatedSwitcher(
          duration: Duration(milliseconds: 100),
          switchInCurve: Curves.linear,
          child: _mainNav.pageNav(_selectedIndex),
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
          ]),
          child: SafeArea(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
              child: GNav(
                  gap: 5,
                  activeColor: Colors.white,
                  iconSize: 24,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  duration: Duration(milliseconds: 100),
                  tabBackgroundColor:
                      Theme.of(context).primaryColor.withOpacity(0.7),
                  tabs: [
                    GButton(
                      icon: LineIcons.home,
                      text: 'หน้าหลัก',
                    ),
                    GButton(
                      icon: Icons.border_all,
                      text: 'ล็อคเกอร์ของฉัน',
                    ),
                    GButton(
                      icon: LineIcons.search,
                      text: 'ค้นหาล็อคเกอร์',
                    ),
                    GButton(
                      icon: LineIcons.bell,
                      text: 'แจ้งเตือน',
                    ),
                    GButton(
                      icon: LineIcons.dashboard,
                      text: 'dashboard',
                    ),
                  ],
                  selectedIndex: _selectedIndex,
                  onTabChange: (index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  }),
            ),
          ),
        ));
  }
}
