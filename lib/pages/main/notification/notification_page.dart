import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final NotificationData _notificationData = NotificationData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(title: 'แจ้งเตือน').build(context),
      body: Container(
          padding: EdgeInsets.all(15),
          child: FutureBuilder(
            future: _notificationData.updateNotification(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Loader();
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data == 200) {
                  return NotificationList();
                } else if (snapshot.data == 401) {
                  Future.delayed(Duration.zero, () async {
                    errorAlert(
                        wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                        function: () => NavigationRoute.navigateAndRemoveUntil(
                            routePath.LoginRoute));
                  });
                  return Container();
                } else if (snapshot.data == 408) {
                  Future.delayed(Duration.zero, () async {
                    errorAlert(
                        wording:
                            'ไม่สามารถอัพเดทข้อมูลได้เนื่องจากการเชื่อมต่อกับระบบผิดพลาด');
                  });

                  return Container();
                } else if (snapshot.data == 500) {
                  Future.delayed(Duration.zero, () async {
                    errorAlert(
                      wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                    );
                  });
                  return Container();
                }
              }
              return Container();
            },
          )),
    );
  }
}
