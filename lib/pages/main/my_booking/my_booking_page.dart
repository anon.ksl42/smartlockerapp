import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/main/my_booking/component/booking_list.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class MyBookingPage extends StatefulWidget {
  @override
  _MyBookingPageState createState() => _MyBookingPageState();
}

class _MyBookingPageState extends State<MyBookingPage> {
  final Booking _booking = Booking();
  @override
  Widget build(BuildContext context) {
    Future<List<BookingModel>> _bookingFunction =
        _booking.getAllBookingByAccountId();
    return Scaffold(
      appBar: AppbarCommon(title: 'ล็อคเกอร์ของฉัน').build(context),
      body: FutureBuilder(
        future: _bookingFunction,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            if (snapshot.error == APIStatus.TIME_OUT) {
              Future.delayed(Duration.zero, () async {
                errorAlert(
                    wording:
                        'ไม่สามารถอัพเดทข้อมูลได้เนื่องจากการเชื่อมต่อกับระบบผิดพลาด');
              });
            } else if (snapshot.error == APIStatus.UNAUTHRIZE) {
              Future.delayed(Duration.zero, () async {
                errorAlert(
                    wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                    function: () => NavigationRoute.navigateAndRemoveUntil(
                        routePath.LoginRoute));
              });
            } else {
              Future.delayed(Duration.zero, () async {
                errorAlert(wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้');
              });
            }
            return Container();
          } else if (snapshot.connectionState == ConnectionState.done) {
            return RefreshIndicator(
                onRefresh: () async {
                  setState(() {});
                },
                child: BookingList(bookingHistory: snapshot.data));
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
