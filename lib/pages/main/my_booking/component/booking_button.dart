import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/route-paths.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/models/transfer_update_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class BookingButton extends StatelessWidget {
  final BookingModel bookingHistory;

  const BookingButton({Key key, @required this.bookingHistory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _buttonGenerate(
        {@required String label,
        Color colorText = Colors.black,
        @required Function function,
        @required String buttonHexColor}) {
      return ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: HexColor(buttonHexColor),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        child: Container(
          child: Text(
            label,
            style: TextStyle(fontSize: 14, color: colorText),
          ),
        ),
        onPressed: () => function(),
        
      );
    }

    switch (bookingHistory.status) {
      case 'WP':
        return _buttonGenerate(
            label: 'passcode ในการใช้งาน',
            function: () => showPasscode(bookingHistory),
            buttonHexColor: '#F5EE38');
        break;
      case 'P':
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buttonGenerate(
                label: 'ขอ Passcode สำหรับปลดล็อค',
                function: () => NavigationRoute.navigateTo(
                    FinishBookingRequestRoute,
                    arguments: bookingHistory),
                buttonHexColor: '#F5EE38'),
            _buttonGenerate(
                label: 'โอนสิทธิ์การใช้งาน',
                function: () => NavigationRoute.navigateTo(TransferRoute,
                    arguments: bookingHistory),
                buttonHexColor: '#F31515',
                colorText: Colors.white),
          ],
        );
        break;
      case 'WF':
        return _buttonGenerate(
            label: 'แสดง Passcode สำหรับปลดล็อค',
            function: () {
              showPasscodeForFinish(bookingHistory);
            },
            buttonHexColor: '#F2B741');
        break;
      case 'WT':
        return _buttonGenerate(
            label: 'ยกเลิกการโอนสิทธิ์',
            function: () => NavigationRoute.navigateTo(TransferUpdateRoute,
                arguments: new TransferUpdateModel(bookingHistory, 'C')),
            buttonHexColor: '#F31515',
            colorText: Colors.white);
        break;
      case 'WR':
        return _buttonGenerate(
            label: 'ยืนยันการรับสิทธิ',
            function: () => NavigationRoute.navigateTo(TransferUpdateRoute,
                arguments: new TransferUpdateModel(bookingHistory, 'A')),
            buttonHexColor: '#26B73F',
            colorText: Colors.white);
        break;
      default:
        return Container();
    }
  }
}
