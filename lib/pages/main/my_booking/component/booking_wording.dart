import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/widgets.dart';

class BookingWording {
  String statusName;
  String titleTime;
  String timeRemain;

  BookingWording({@required BookingModel bookingHistory}) {
    switch (bookingHistory.status) {
      case 'WP':
        this.statusName = 'จองแล้ว';
        this.titleTime = 'เวลาการจองคงเหลือ';
        this.timeRemain =
            bookingRemain(bookingHistory.createDate, timeExpire: 60);
        break;
      case 'P':
        this.statusName = 'กำลังใช้งาน';
        this.titleTime = 'เวลาการใช้งาน';
        this.timeRemain = takeTimeUse(bookingHistory.startDate);
        break;
      case 'WF':
        this.statusName = 'รอการสิ้นสุดหการใช้งาน';
        this.titleTime = 'เวลาการปลดล็อคคงเหลือ';
        this.timeRemain = bookingRemain(bookingHistory.endDate);
        break;
      case 'F':
        this.statusName = 'สิ้นสุดการใช้งาน';
        this.titleTime = '';
        this.timeRemain = '';
        break;
      case 'T':
        this.statusName = 'โอนสิทธิ์เรียบร้อย';
        this.titleTime = '';
        this.timeRemain = '';
        break;
      case 'C':
        this.statusName = 'ยกเลิกการจอง';
        this.titleTime = '';
        this.timeRemain = '';
        break;
      case 'WT':
        this.statusName = 'รอการโอนสิทธิ์';
        this.titleTime = 'เวลาการใช้งาน';
        this.timeRemain = takeTimeUse(bookingHistory.startDate);
        break;
      case 'WR':
        this.statusName = 'รอยืนยันการรับสิทธิ์';
        this.titleTime = 'เวลาการใช้งาน';
        this.timeRemain = takeTimeUse(bookingHistory.startDate);
        break;
      default:
    }
  }
  String bookingRemain(DateTime createDate, {int timeExpire = 15}) {
    DateTime expired = createDate.add(Duration(minutes: timeExpire));
    int remain = expired.difference(DateTime.now()).inMinutes;
    return '$remain นาที';
  }

  String takeTimeUse(DateTime startDate) {
    int time = DateTime.now().difference(startDate).inMinutes;
    int hour = (time / 60).floor();
    int minute = (time % 60).floor();
    return hour <= 0 ? '$minute นาที' : '$hour ชั่วโมง $minute นาที';
  }

  Widget isHaveTitleTime() {
    TextStyle textStyle = TextStyle(fontSize: 16);
    return titleTime.isEmpty
        ? Container()
        : Text(
            '$titleTime : $timeRemain',
            style: textStyle,
          );
  }
}
