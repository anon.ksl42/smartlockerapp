import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/main/my_booking/component/booking_button.dart';
import 'package:SmartLockerApp/pages/main/my_booking/component/booking_wording.dart';
import 'package:flutter/material.dart';

class BookingTile extends StatelessWidget {
  final BookingModel bookingHistory;
  const BookingTile({Key key, @required this.bookingHistory}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 16);
    BookingWording _bookingWording =
        BookingWording(bookingHistory: bookingHistory);
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'ชื่อสถานที่ : ${bookingHistory.locateName}',
            style: textStyle,
          ),
          Text(
            'หมายเลขล็อคเกอร์ : ${bookingHistory.lkRoomCode}',
            style: textStyle,
          ),
          Text(
            'สถานะการจอง : ${_bookingWording.statusName}',
            style: textStyle,
          ),
          _bookingWording.isHaveTitleTime(),
          Text(
            'วันดำเนินการ : ${dateTimeToThai(bookingHistory.createDate)}',
            style: textStyle,
          ),
          BookingButton(bookingHistory: bookingHistory),
          Divider(
            thickness: 2,
          )
        ]);
  }
}
