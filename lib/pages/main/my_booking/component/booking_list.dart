import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/pages/main/my_booking/component/booking_tile.dart';
import 'package:flutter/material.dart';

class BookingList extends StatelessWidget {
  final List<BookingModel> bookingHistory;

  const BookingList({Key key, @required this.bookingHistory}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: EdgeInsets.all(10),
        itemCount: bookingHistory.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.only(top: 10, left: 10, right: 10),
            child: BookingTile(
              bookingHistory: bookingHistory[index],
            ),
          );
        });
  }
}
