import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/data/global/global.dart';
import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/pages/main/home/profile_page/component/edit_name_dialog.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  bool isInvalidName;
  bool isInvalidSurName;
  bool isChange;

  _updateFirstAndLastName(String isUpdate, {bool isWillPopScope = false}) {
    String _nameUpdate = nameController.text.replaceAll(' ', '');
    String _surnameUpdate = surnameController.text.replaceAll(' ', '');

    if (isUpdate == 'S') {
      isInvalidName = _nameUpdate.isEmpty ? true : false;
      isInvalidSurName = _surnameUpdate.isEmpty ? true : false;
      isChange = true;
    } else if (isUpdate == 'C') {
      isInvalidName = false;
      isInvalidSurName = false;
      isChange = false;
      nameController.text = AccountGlobal.getFirstName();
      surnameController.text = AccountGlobal.getLastName();
    }
    if (!isWillPopScope) {
      Navigator.of(context).pop();
    }

    setState(() {
      nameController.text = nameController.text.replaceAll(' ', '');
      surnameController.text = surnameController.text.replaceAll(' ', '');
    });
  }

  _updateInDatabase(
      {bool isWillPopScope = false, bool isLogOut = false}) async {
    Account _account = new Account();
    AccountModel accountModel = AccountModel(
        firstName: nameController.text.replaceAll(' ', ''),
        lastName: surnameController.text.replaceAll(' ', ''),
        accountId: AccountGlobal.getAccountId());
    if (!isLogOut) {
      SweetAlert.show(context,
          subtitle: 'กำลังอัพเดทข้อมูล', style: SweetAlertStyle.loading);
    }
    await _account.updateFullName(
        account: accountModel,
        isWillPopScope: isWillPopScope,
        isLogout: isLogOut);
  }

  _logout() {
    SweetAlert.show(context,
        title: 'ยืนยันออกจากระบบ',
        confirmButtonText: 'ออกจากระบบ',
        showCancelButton: true,
        style: SweetAlertStyle.confirm,
        cancelButtonText: 'ยกเลิก', onPress: (bool isconfirm) {
      if (isconfirm) {
        _updateInDatabase(isLogOut: true);
        Future.delayed(Duration.zero).then((value) =>
            NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute));
        return true;
      } else {
        return true;
      }
    });
  }

  @override
  void initState() {
    isInvalidName = false;
    isInvalidSurName = false;
    nameController.text = AccountGlobal.getFirstName();
    surnameController.text = AccountGlobal.getLastName();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isInvalidName || isInvalidSurName) {
      Future.delayed(Duration.zero).then((value) => editNameDialog(
          context: context,
          nameController: nameController,
          isInvalidName: isInvalidName,
          surnameController: surnameController,
          isInvalidSurname: isInvalidSurName,
          submitFunction: _updateFirstAndLastName));
    }
    return WillPopScope(
      onWillPop: () => _updateInDatabase(isWillPopScope: true),
      child: Scaffold(
        appBar: AppbarCommon(
          title: 'โปรไฟล์',
          function: () => _updateInDatabase(),
        ).build(context),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, border: Border.all(width: 1)),
                  child: Icon(
                    Icons.account_circle,
                    size: 150,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${nameController.text + ' ' + surnameController.text}',
                      style: TextStyle(fontSize: 30),
                    ),
                    IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          editNameDialog(
                              context: context,
                              nameController: nameController,
                              isInvalidName: isInvalidName,
                              surnameController: surnameController,
                              isInvalidSurname: isInvalidSurName,
                              submitFunction: _updateFirstAndLastName);
                        }),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              GestureDetector(
                onTap: () => _logout(),
                child: Container(
                  padding: EdgeInsets.only(top: 10),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 15.0),
                        child: Icon(
                          Icons.logout,
                          size: 24,
                        ),
                      ),
                      Text(
                        'ออกจากระบบ',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
