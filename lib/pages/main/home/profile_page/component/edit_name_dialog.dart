import 'package:SmartLockerApp/component/component.dart';
import 'package:flutter/material.dart';

void editNameDialog({
  @required BuildContext context,
  @required TextEditingController nameController,
  @required bool isInvalidName,
  @required TextEditingController surnameController,
  @required bool isInvalidSurname,
  @required Function submitFunction,
}) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () => submitFunction("C", isWillPopScope: true),
          child: AlertDialog(
            title: RichText(
              text: TextSpan(
                text: 'เปลี่ยนชื่อ - นามสกุล\n',
                style: TextStyle(color: Colors.black, fontSize: 18),
                children: <TextSpan>[
                  TextSpan(
                      text: '* ชื่อ-นามสกุลไม่สามารถว่างได้',
                      style: TextStyle(color: Colors.red, fontSize: 10)),
                ],
              ),
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  TextInputTemplate(
                    versionBorder: 2,
                    controller: nameController,
                    errorTextPattern: null,
                    errorTextInvalid: 'กรุณากรอกชื่อ',
                    icon: null,
                    labelText: 'ชื่อ *',
                    validatePattern: false,
                    validatevalid: isInvalidName,
                  ),
                  SizedBox(height: 15),
                  TextInputTemplate(
                    versionBorder: 2,
                    controller: surnameController,
                    errorTextPattern: null,
                    errorTextInvalid: 'กรุณากรอกนามสกุล',
                    icon: null,
                    labelText: 'นามสกุล *',
                    validatePattern: false,
                    validatevalid: isInvalidSurname,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                  child: Text('ยืนยัน'), onPressed: () => submitFunction("S")),
              TextButton(
                  child: Text(
                    'ยกเลิก',
                    style: TextStyle(color: Colors.red),
                  ),
                  onPressed: () => submitFunction("C")),
            ],
          ),
        );
      });
}
