import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

class FunctionIconList extends StatelessWidget {
  Widget iconMain({
    IconData icon,
    String title,
    String path,
  }) {
    return InkWell(
      onTap: () {
        NavigationRoute.navigateTo(path);
      },
      child: Container(
        height: 70,
        child: Column(
          children: [
            Icon(
              icon,
              size: 40,
              color: Colors.black,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 18, color: Colors.black),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      padding: EdgeInsets.all(10),
      width: 370,
      height: 130,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'บริการของเรา',
            textAlign: TextAlign.start,
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              iconMain(
                icon: Icons.bookmark,
                title: 'จองล็อคเกอร์',
                path: routePath.SearchingOptionRoute,
              ),
              iconMain(
                icon: Icons.attach_money,
                title: 'ค่าบริการ',
                path: routePath.ServiCeRateRoute,
              ),
              iconMain(
                icon: Icons.phone,
                title: 'ติดต่อเรา',
                path: routePath.ContractRoute,
              ),
            ],
          )
        ],
      ),
    );
  }
}
