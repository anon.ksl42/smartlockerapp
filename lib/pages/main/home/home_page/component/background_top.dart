import 'package:flutter/material.dart';

class BackgroundTop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      color: Theme.of(context).primaryColor,
    );
  }
}
