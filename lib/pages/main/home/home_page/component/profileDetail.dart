import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ProfileDetail extends StatelessWidget {
  final String fullname = AccountGlobal.getFullName();
  final String userId = AccountGlobal.getAccountCode();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 55,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(24)),
        color: HexColor('#8C7A7A').withOpacity(0.5),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(children: [
          Icon(
            Icons.account_circle,
            size: 40,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(fullname,
                    style: TextStyle(fontSize: 14, color: Colors.white)),
                Text(
                  'UserId : ' + userId,
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
