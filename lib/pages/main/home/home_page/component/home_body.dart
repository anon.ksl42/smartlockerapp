import 'package:SmartLockerApp/component/notification/notification.dart';
import 'package:SmartLockerApp/pages/main/home/home_page/component/background_top.dart';
import 'package:SmartLockerApp/pages/main/home/home_page/component/function_icon_list.dart';
import 'package:SmartLockerApp/pages/main/home/home_page/component/profileDetail.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;

import 'package:flutter/material.dart';

class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          clipBehavior: Clip.hardEdge,
          children: [
            Container(
              width: double.infinity,
              height: 270,
              color: Colors.transparent,
            ),
            BackgroundTop(),
            Positioned(
                top: 60,
                left: 200,
                child: GestureDetector(
                    onTap: () {
                      NavigationRoute.navigateTo(routePath.ProfileRoute);
                    },
                    child: ProfileDetail())),
            Positioned(top: 130, right: 1, left: 1, child: FunctionIconList())
          ],
        ),
        Spacer(),
        Container(
          child: Text(
            'การแจ้งเตือนล่าสุด',
            style: TextStyle(fontSize: 18),
          ),
        ),
        Container(
          height: 300,
          margin: EdgeInsets.only(top: 0, left: 20),
          padding: EdgeInsets.all(0),
          child: NotificationList(),
        ),
        Spacer(),
      ],
    );
  }
}
