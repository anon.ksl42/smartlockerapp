
import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/pages/main/home/home_page/component/home_body.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:flutter/services.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final NotificationData _notificationData = NotificationData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _notificationData.updateNotification(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == 200) {
              return HomeBody();
            } else if (snapshot.data == 401) {
              Future.delayed(Duration.zero, () async {
                NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute);
              });
              return Container();
            } else if (snapshot.data == 408) {
              Future.delayed(Duration.zero, () async {
                errorAlert(
                    wording:
                        'ไม่สามารถอัพเดทข้อมูลได้เนื่องจากการเชื่อมต่อกับระบบผิดพลาด');
              });

              return Container();
            } else if (snapshot.data == 500) {
              Future.delayed(Duration.zero, () async {
                errorAlert(
                    wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                    function: () => SystemNavigator.pop());
              });

              return Container();
            }
          }
          return Container();
        },
      ),
    );
  }
}
