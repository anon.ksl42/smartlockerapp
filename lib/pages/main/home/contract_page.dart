import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:flutter/material.dart';

class ContractPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppbarCommon(
            title: 'ติดต่อเรา',
            function: () => Navigator.of(context).pop()).build(context),
        body: Container(
          height: 250,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.grey.shade400),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'ที่ตั้งสำนักงาน',
                style: TextStyle(color: Colors.grey, fontSize: 20),
              ),
              Text(
                'เลขที่ 5/133 ถ.บางระนาด แขวงศาลาธรรมสพน์ \nเขตวัฒนา กรุงเทพมหานคร 10170',
                style: TextStyle(fontSize: 16),
              ),
              Divider(
                thickness: 2,
              ),
              Text(
                'เบอร์โทรศัพท์',
                style: TextStyle(color: Colors.grey, fontSize: 20),
              ),
              Text(
                '02 248 3975',
                style: TextStyle(fontSize: 16),
              ),
              Divider(
                thickness: 2,
              ),
              Text(
                'เบอร์โทรศัพท์มือถือ',
                style: TextStyle(color: Colors.grey, fontSize: 20),
              ),
              Text(
                '089 505 4123  ',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
        ));
  }
}
