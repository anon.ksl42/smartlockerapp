import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ServiceRatePage extends StatefulWidget {
  @override
  _ServiceRatePageState createState() => _ServiceRatePageState();
}

class _ServiceRatePageState extends State<ServiceRatePage> {
  List<List<int>> _sizeDetailList = [
    [55, 45, 27],
    [55, 45, 56],
    [55, 45, 86],
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'ค่าบริการ',
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: FutureBuilder(
        future: ServiceRate.initialServiceRateData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else {
            return ListView.builder(
                itemCount: ServiceRate.serviceRateNormalTypeList.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                    ),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: HexColor('#F2B741').withOpacity(0.5),
                          ),
                          child: Row(
                            children: [
                              Container(
                                  height: 30,
                                  child: Image.asset(
                                    'assets/images/Sqare.png',
                                  )),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'ตู้ขนาดเล็ก (${ServiceRate.serviceRateNormalTypeList[index].lkSizeName})',
                                      style: TextStyle(fontSize: 16),
                                    ),
                                    Text(
                                      'ขนาดตู้ : กว้าง ${_sizeDetailList[index][0]} ซม. ยาว ${_sizeDetailList[index][1]} ซม. สูง ${_sizeDetailList[index][2]} ซม. ',
                                      style: TextStyle(fontSize: 16),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  ServiceRate.serviceRateNormalTypeList[index]
                                      .typeName,
                                  style: TextStyle(fontSize: 22),
                                ),
                                Text(
                                  ServiceRate.serviceRateNormalTypeList[index]
                                      .description
                                      .replaceAll('\\n', '\n'),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Divider(
                                  thickness: 2,
                                ),
                                Text(
                                  ServiceRate.serviceRateAllDayTypeList[index]
                                      .typeName,
                                  style: TextStyle(fontSize: 22),
                                ),
                                Text(
                                  ServiceRate.serviceRateAllDayTypeList[index]
                                      .description
                                      .replaceAll('\\n', '\n'),
                                  style: TextStyle(fontSize: 18),
                                )
                              ],
                            ))
                      ],
                    ),
                  );
                });
          }
        },
      ),
    );
  }
}
