import 'package:SmartLockerApp/component/appbar-common.dart';
import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/user-dashboard-model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/user-dashboard-service.dart';
import 'package:flutter/material.dart';

class DashBoardPage extends StatefulWidget {
  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  String fullname;
  Future<void> getUserDashboard() async {}
  int isSelected = 0;
  String rangeType = "W";
  TextStyle test = TextStyle(fontSize: 40, color: Colors.black);
  TextStyle testClick = TextStyle(fontSize: 40, color: Colors.white);
  UserDashboardService userDashboardService = UserDashboardService();
  @override
  void initState() {
    // TODO: implement initState
    userDashboardService = UserDashboardService();
    fullname = AccountGlobal.getFullName();
    rangeType = "W";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'Dashboard',
      ).build(context),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, border: Border.all(width: 1)),
                child: Icon(
                  Icons.account_circle,
                  size: 150,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      fullname,
                      style: TextStyle(fontSize: 30),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        isSelected = 0;
                        rangeType = 'W';
                      });
                    },
                    child: Container(
                        color: isSelected == 0 ? Colors.red : Colors.white,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'สัปดาห์',
                          style: test,
                        )),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  child: Container(
                    child: TextButton(
                      onPressed: () {
                        setState(() {
                          isSelected = 1;
                          rangeType = 'M';
                        });
                      },
                      child: Container(
                        color: isSelected == 1 ? Colors.red : Colors.white,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'เดือน',
                          style: test,
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        isSelected = 2;
                        rangeType = 'Y';
                      });
                    },
                    child: Container(
                      color: isSelected == 2 ? Colors.red : Colors.white,
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'ปี',
                        style: test,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              child: FutureBuilder<UserDashboard>(
                future: userDashboardService.getDashboard(
                    AccountGlobal.getAccountId(), rangeType),
                builder: (BuildContext context,
                    AsyncSnapshot<UserDashboard> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (snapshot.hasError) {
                      if (snapshot.error == APIStatus.UNAUTHRIZE) {
                        errorAlert(
                            wording:
                                'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
                            function: () =>
                                NavigationRoute.navigateAndRemoveUntil(
                                    LoginRoute));
                      } else if (snapshot.error == APIStatus.SYETEM_ERROR) {
                        errorAlert(
                          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                        );
                      }
                      return Container();
                    } else {
                      var data = snapshot.data;
                      return Container(
                        width: double.infinity,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Flexible(
                                    child: Card(
                                      child: Container(
                                        width: 150,
                                        height: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Text(
                                              'การใช้งาน',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            Text(
                                              data.finishUsing.toString(),
                                              style: TextStyle(fontSize: 50),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Card(
                                      child: Container(
                                        width: 150,
                                        height: 150,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Text(
                                              'การโอนสิทธิ์ทั้งหมด',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            Text(
                                              data.totalTransfer.toString(),
                                              style: TextStyle(fontSize: 50),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Card(
                                    child: Container(
                                      width: 150,
                                      height: 150,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Text(
                                            'รับการโอนสิทธิ์',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Text(
                                            data.successTransfer.toString(),
                                            style: TextStyle(fontSize: 50),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Card(
                                    child: Container(
                                      width: 150,
                                      height: 150,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          Text(
                                            'ถูกปฎิเสธการโอนสิทธิ์',
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Text(
                                            data.rejectTransfer.toString(),
                                            style: TextStyle(fontSize: 50),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ]),
                      );
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
