import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/route-paths.dart';
import 'package:SmartLockerApp/models/location_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/location_service.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class SearchingPage extends StatefulWidget {
  @override
  _SearchingPageState createState() => _SearchingPageState();
}

class _SearchingPageState extends State<SearchingPage> {
  final List<Marker> markerss = [];
  GoogleMapController mapController;
  List<LocationModel> locateItems = [];
  var detailLocation;
  double latitude;
  double longtitude;
  bool loading = true;

  void initState() {
    super.initState();

    fetchLocation();
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
    });
  }

  Future fetchLocation() async {
    try {
      await getLocation();
      final response = await LocationService.getLocations();
      setState(() {
        locateItems = response;
        loading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  Future getLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        latitude = 13.8708078;
        longtitude = 100.5493794;
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        latitude = 13.8708078;
        longtitude = 100.5493794;
        return;
      }
    }

    _locationData = await location.getLocation();
    latitude = _locationData.latitude;
    longtitude = _locationData.longitude;
  }

  Future fetchLocationById(int id) async {
    try {
      var response = await LocationService.getLocationByLocationId(id);
      setState(() {
        detailLocation = response;
      });
    } catch (e) {
      print(e);
    }
  }

  Set<Marker> _createMarkers() {
    for (var location in locateItems) {
      var lcationName = location.locateName;
      int id = location.locateId;
      markerss.add(Marker(
        markerId: MarkerId(location.locateId.toString()),
        position: LatLng(
            double.parse(location.latitude), double.parse(location.longtitude)),
        icon: BitmapDescriptor.defaultMarker,
        infoWindow: InfoWindow(
            title: "Locker-$lcationName",
            onTap: () async {
              await fetchLocationById(id);
              NavigationRoute.navigateTo(LocationDetailRoute,
                  arguments: detailLocation);
              // Navigator.of(context)
              //     .pushNamed("/locationDetail", arguments: detailLocation);
            }),
      ));
    }
    return markerss.toSet();
  }

  Widget mapWidget() {
    return GoogleMap(
      myLocationEnabled: true,
      mapType: MapType.normal,
      markers: _createMarkers(),
      initialCameraPosition: CameraPosition(
        target: LatLng(latitude, longtitude),
        zoom: 13,
      ),
      onMapCreated: _onMapCreated,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppbarCommon(
        title: 'ล็อคเกอร์ของฉัน',
        function: () => NavigationRoute.goBack(),
      ).build(context),
      body: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : mapWidget(),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child: Icon(
            Icons.search,
            color: Colors.black87,
          ),
          onPressed: () {
            NavigationRoute.navigateTo(TextSearchRoute);
          }),
    );
  }
}
