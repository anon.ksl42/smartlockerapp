import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/location_service.dart';
import 'package:flutter/material.dart';

class TextSearchPage extends StatefulWidget {
  @override
  _TextSearchPageState createState() => _TextSearchPageState();
}

class _TextSearchPageState extends State<TextSearchPage> {
  final TextEditingController _filter = new TextEditingController();
  String _searchText = "";
  bool _isError = false;
  List<LocationModel> locationAll = []; // names we get from API
  List<LocationModel> locationFiltered = []; // names filtered by search text
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('ค้นหาล็อคเกอร์');

  void _getNames() async {
    await LocationService.getLocations().then((value) {
      setState(() {
        locationAll = value;
        locationFiltered = locationAll;
      });
    }).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ไม่สามารถทำเรื่องจองได้เนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
      setState(() {
        _isError = true;
      });
    });
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = TextField(
          keyboardType: TextInputType.name,
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text('ค้นหาล็อคเกอร์');
        locationFiltered = locationAll;
        _filter.clear();
      }
    });
  }

  Widget _buildList() {
    if (_searchText.isNotEmpty) {
      List<LocationModel> tempList = [];
      for (int i = 0; i < locationFiltered.length; i++) {
        if (locationFiltered[i]
                .district
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            locationFiltered[i]
                .locateName
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            locationFiltered[i]
                .province
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            locationFiltered[i]
                .postalCode
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            locationFiltered[i]
                .subDistrict
                .toLowerCase()
                .contains(_searchText.toLowerCase())) {
          tempList.add(locationFiltered[i]);
        }
      }
      locationFiltered = tempList;
    }
    return ListView.builder(
      itemCount: locationAll == null ? 0 : locationFiltered.length,
      itemBuilder: (BuildContext context, int index) {
        return new ListTile(
            title: Text(locationFiltered[index].locateName),
            subtitle: Text(locationFiltered[index].subDistrict +
                ' ' +
                locationFiltered[index].district +
                ' ' +
                locationFiltered[index].province +
                ' ' +
                locationFiltered[index].postalCode),
            onTap: () => NavigationRoute.navigateTo(LocationDetailRoute,
                arguments: locationFiltered[index]));
      },
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  _TextSearchPageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          locationFiltered = locationAll;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _isError ? Container() : _buildList(),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
