
import 'package:SmartLockerApp/bussiness-logic/bussiness-logic.dart';
import 'package:SmartLockerApp/component/my_alert.dart';
import 'package:SmartLockerApp/constant/api-status.dart';
import 'package:SmartLockerApp/constant/route-paths.dart' as routePath;
import 'package:SmartLockerApp/pages/preparedata/component/prepare_loading.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PrepareDataPage extends StatefulWidget {
  @override
  _PrepareDataPageState createState() => _PrepareDataPageState();
}

class _PrepareDataPageState extends State<PrepareDataPage> {
  MyAlert _myAlert = MyAlert();
  PrepareDataBussinessLogic _prepareDataBussinessLogic   = PrepareDataBussinessLogic();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: _prepareDataBussinessLogic.prepareData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              if (snapshot.error == APIStatus.UNAUTHRIZE) {
                Future.delayed(Duration.zero, () async {
                  NavigationRoute.navigateAndRemoveUntil(routePath.LoginRoute);
                });
              } else if (snapshot.error == APIStatus.CONFLICT) {
                Future.delayed(Duration.zero, () async {
                  _myAlert.errorAlert(
                      wording: 'ไม่สามารถดำเนินการได้ขณะนี้',
                      function: () => SystemNavigator.pop());
                });
              } else {
                Future.delayed(Duration.zero, () async {
                  _myAlert.errorAlert(
                      wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
                      function: () => SystemNavigator.pop());
                });
              }
              return Container();
            } else {
              Future.delayed(Duration.zero, () async {
                NavigationRoute.navigateAndRemoveUntil(routePath.MainRoute);
              });
            }
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return PrepareLoading();
          }
          return Container();
        },
      ),
    );
  }
}
