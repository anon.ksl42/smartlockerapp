import 'package:flutter/material.dart';

class PrepareLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset('assets/images/Logo.png'),
        Container(margin: EdgeInsets.only(top:20), child: CircularProgressIndicator()),
      ],
    );
  }
}
