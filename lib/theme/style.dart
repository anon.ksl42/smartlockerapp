import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

ThemeData appTheme() {
  return ThemeData(
      primaryColor: HexColor('#F2B741'),
      accentColor: HexColor('#F2B741').withOpacity(0.5),
      fontFamily: 'DB-Adman-X',
      textTheme: TextTheme(headline1: TextStyle(fontSize: 18 , color: Colors.black)),
      buttonColor: HexColor('#F2B741'),
      buttonTheme: ButtonThemeData(buttonColor: HexColor('#F2B741') ,));
}
