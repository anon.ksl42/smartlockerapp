import 'dart:async';

import 'package:SmartLockerApp/data/datas.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/booking_service.dart';
import 'package:SmartLockerApp/services/service.dart';

class ValidateBookingAuto {
  static Map<int, Timer> timerList;
  static Map<int, Timer> standbyTimerList;

  static void initialTimer() {
    if (timerList == null) {
      timerList = new Map<int, Timer>();
    }
    if (standbyTimerList == null) {
      standbyTimerList = new Map<int, Timer>();
    }
  }

  static Future<void> addUse30MinutesLeftNotification(
      BookingModel bookingHistory) async {
    int bookingId = bookingHistory.bookingId;
    int rateTypeId = bookingHistory.rateTypeId;
    int lkRoomId = bookingHistory.lkRoomId;
    if (timerList[bookingId] == null && standbyTimerList[bookingId] == null) {
      ServiceRateModel serviceChargeResult =
          await ServiceRateService.getServiceChargeByLockerRoomIdAndTypeId(
              lkRoomId: lkRoomId, typeId: rateTypeId);

      DateTime expire = bookingHistory.startDate
          .add(Duration(hours: serviceChargeResult.time));
      if (expire.isBefore(DateTime.now())) {
        int diff = DateTime.now().difference(expire).inHours.floor();

        expire = expire.add(Duration(hours: diff + 1));
      }
      DateTime expireNew = expire.subtract(Duration(minutes: 30));
      int minutesToTrickTimer = expireNew.difference(DateTime.now()).inMinutes;

      Timer almostExpireDate =
          Timer(Duration(minutes: minutesToTrickTimer), () async {
        BookingModel booking =
            await BookingService.getBookingByBookingId(bookingId);
        if (booking.status == 'P') {
          print('Booking:almost expire begin $bookingId');
          MyNotification.almostNotification(null,
              bookingHistory: bookingHistory);
          print('Booking:almost expire complete $bookingId');
          standbyTimerList[bookingId] = standbyTimer(bookingId);
        } else {
          print('Booking: can\'t cancel');
        }
        timerList.remove(bookingId);
      });
      timerList[bookingId] = almostExpireDate;
    }
  }

  static Timer standbyTimer(int bookingId) {
    return Timer(Duration(minutes: 30), () {
      standbyTimerList.remove(bookingId);
    });
  }

  static void bookingTimer(int bookingId) {
    Timer.periodic(Duration(minutes: 60), (Timer t) async {
      print("Booking validate begin");
      var booking = await BookingService.getBookingByBookingId(bookingId);
      if (booking.status == 'WP') {
        print('Booking:cancel begin');
        await BookingService.updateBooking(
          new BookingModel(bookingId: booking.bookingId, status: 'C'),
        );
        print('Booking:cancel complete');
        MyNotification.cancelBookingNotification(booking);
      } else {
        print('Booking: can\'t cancel');
      }
      t.cancel();
    });
  }

  static void finishUsingTimer(int bookingId) {
    Timer.periodic(Duration(minutes: 15), (Timer t) async {
      print("finish validate begin");
      var booking = await BookingService.getBookingByBookingId(bookingId);

      if (booking.status == 'WF') {
        print('finish:cancel finish begin');
        await BookingService.updateBooking(
            new BookingModel(bookingId: booking.bookingId, status: 'P'));
        print('finish:cancel finish complete');
      } else {
        print('finish: can\'t cancel');
      }
      t.cancel();
    });
  }

  static void transferTimer(int bookingId) {
    Timer.periodic(Duration(minutes: 10), (Timer t) async {
      print("Transfer validate begin");
      var booking = await BookingService.getBookingByBookingId(bookingId);
      if (booking.status == 'WT') {
        print('Transfer:cancel finish begin');
        await BookingService.cancelTransfer(booking);
        print('Transfer:cancel finish complete');
        MyNotification.cancelTransferNotification(booking);
      } else {
        print('Transfer: can\'t cancel');
      }
      t.cancel();
    });
  }
}
