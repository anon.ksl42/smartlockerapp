import 'package:SmartLockerApp/Route/router.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/schedule/validate_auto.dart';
import 'package:SmartLockerApp/theme/style.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'constant/route-paths.dart' as routePath;
import 'data/global/global.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Global.init();
  Intl.defaultLocale = 'th';
  ValidateBookingAuto.initialTimer();
  initializeDateFormatting();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: appTheme(),
      navigatorKey: NavigationRoute.navigatorKey,
      onGenerateRoute: generateRoute,
      initialRoute: routePath.PrepareDataRoute,
    );
  }
}
