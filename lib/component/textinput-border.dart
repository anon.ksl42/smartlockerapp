import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

InputBorder focusedBorder(BuildContext context) {
  return UnderlineInputBorder(
    borderSide: BorderSide(
      color: Theme.of(context).primaryColor,
      width: 2,
    ),
  );
}

InputBorder enableBorder() {
  return UnderlineInputBorder(
    borderSide: BorderSide(
      color: HexColor('#707070'),
    ),
  );
}

InputBorder errorBorder() {
  return UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.redAccent[200],
    ),
  );
}

InputBorder formFocusedBorder(BuildContext context) {
  return OutlineInputBorder(
    borderSide: BorderSide(
      color: Theme.of(context).primaryColor,
      width: 2,
    ),
  );
}

InputBorder formEnableBorder() {
  return OutlineInputBorder(
    borderSide: BorderSide(
      color: HexColor('#707070'),
    ),
  );
}

InputBorder formErrorBorder() {
  return OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.redAccent[200],
    ),
  );
}
