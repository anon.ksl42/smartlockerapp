import 'package:SmartLockerApp/component/dateformat_thai.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:sweetalert/sweetalert.dart';

void errorAlert({@required wording, Function function}) {
  SweetAlert.show(
    NavigationRoute.navigatorKey.currentContext,
    subtitle: wording,
    style: SweetAlertStyle.error,
    onPress: (isConfirm) {
      function != null ? function() : NavigationRoute.goBack();
      return false;
    },
  );
}

void sucessAlert({@required wording, Function function}) {
  SweetAlert.show(
    NavigationRoute.navigatorKey.currentContext,
    subtitle: wording,
    style: SweetAlertStyle.success,
    onPress: (isConfirm) {
      function != null ? function() : NavigationRoute.goBack();
      return false;
    },
  );
}

showPasscodeForFinish(BookingModel bookingHistory) {
  return showDialog<void>(
      context: NavigationRoute.navigatorKey.currentContext,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    bookingHistory.passcode,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor, fontSize: 80),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    'สำหรับสิ้นสุดการใช้งานล็อคเกอร์หมายเลข ${bookingHistory.lkRoomCode}',
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    'ใช้งานก่อน ${dateTimeToThai(bookingHistory.endDate.add(Duration(minutes: 15)))}',
                    style: TextStyle(fontSize: 14),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Passcode นี้มีอายุการใช้งาน 15 นาที Passcode นี้ควรเก็บความลับ',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: HexColor('#FC6767'),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                    child: Container(
                      child: Text(
                        'ย้อนกลับ',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      });
}

Future<void> showPasscode(BookingModel bookingHistory) async {
  return showDialog<void>(
    context: NavigationRoute.navigatorKey.currentContext,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Text(
                  '${bookingHistory.passcode}',
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 80),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'สำหรับล็อคเกอร์หมายเลข ${bookingHistory.lkRoomCode}',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  'ใช้งานก่อน ${dateTimeToThai(bookingHistory.createDate)}',
                  style: TextStyle(fontSize: 14),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Passcode นี้มีอายุการใช้งาน 60 นาที หากไม่ได้ใช้งานจน Passcode หมดอายุ ระบบจะทำการยกเลิกการจองและจะคืนเงินให้บางส่วน และ Passcode นี้ควรเก็บความลับ',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: HexColor('#FC6767'),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  child: Container(
                    child: Text(
                      'ย้อนกลับ',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
