class AppException implements Exception {
  final _message;
  final _prefix;

  AppException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message, "Unauthorised: ");
}

class ConflitException extends AppException {
  ConflitException([String message]) : super(message, "Conflit : ");
}

class NoContentException extends AppException {
  NoContentException([String message]) : super(message, "No content: ");
}
class InternalServerException extends AppException {
  InternalServerException([String message]) : super(message, "Server error: ");
}
