import 'package:SmartLockerApp/component/dateformat_thai.dart';
import 'package:SmartLockerApp/component/notification/notification_icon.dart';
import 'package:SmartLockerApp/data/notification.dart';
import 'package:SmartLockerApp/models/models.dart';
import 'package:flutter/material.dart';

class NotificationList extends StatelessWidget {
  final int displayAmount;

  const NotificationList({Key key, this.displayAmount}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 14);
    final List<NotificationModel> notificationList = MyNotification.listAllNoti;
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: notificationList.length == 0
          ? Center(
              child: Text('no Notification now'),
            )
          : ListView.builder(
              itemCount: displayAmount != null
                  ? displayAmount
                  : notificationList.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Row(
                      children: [
                        NotificationIcon(
                            eventId: notificationList[index].eventId),
                        SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                notificationList[index].eventName,
                                style: textStyle,
                              ),
                              Text(
                                notificationList[index].description,
                                style: textStyle,
                              ),
                              Text(
                                'วันที่ดำเนินการ : ${dateTimeToThai(notificationList[index].createDate)}',
                                style: textStyle,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      thickness: 2,
                    )
                  ],
                );
              }),
    );
  }
}
