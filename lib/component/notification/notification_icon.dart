import 'package:SmartLockerApp/data/datas.dart';
import 'package:flutter/material.dart';

class NotificationIcon extends StatelessWidget {
  final int eventId;

  const NotificationIcon({Key key, this.eventId}) : super(key: key);

  Image _icon() {
    if (eventId == EventType.eventTypeForData[EventType.booking] ||
        eventId == EventType.eventTypeForData[EventType.cancelBooking]) {
      return Image.asset('assets/images/booking.png');
    } else if ((eventId == EventType.eventTypeForData[EventType.transfer] ||
            eventId == EventType.eventTypeForData[EventType.approveTransfer] ||
            eventId == EventType.eventTypeForData[EventType.rejectTransfer]) ||
        eventId == EventType.eventTypeForData[EventType.cancelTransfer]) {
      return Image.asset('assets/images/transfer.png');
    } else if (eventId ==
        EventType.eventTypeForData[EventType.almostExpireUse]) {
      return Image.asset('assets/images/alert.png');
    } else {
      return Image.asset('assets/images/other.png');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 30,
      height: 30,
      child: _icon(),
    );
  }
}
