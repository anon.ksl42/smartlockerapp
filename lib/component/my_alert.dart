import 'package:SmartLockerApp/route/navigation.dart';
import 'package:flutter/material.dart';
import 'package:sweetalert/sweetalert.dart';

class MyAlert {
  void errorAlert({@required wording, Function function}) {
    SweetAlert.show(
      NavigationRoute.navigatorKey.currentContext,
      subtitle: wording,
      style: SweetAlertStyle.error,
      onPress: (isConfirm) {
        function != null ? function() : NavigationRoute.goBack();
        return false;
      },
    );
  }

  void sucessAlert({@required wording, Function function}) {
    SweetAlert.show(
      NavigationRoute.navigatorKey.currentContext,
      subtitle: wording,
      style: SweetAlertStyle.success,
      onPress: (isConfirm) {
        function != null ? function() : NavigationRoute.goBack();
        return false;
      },
    );
  }

  void waitingDialog() {
    SweetAlert.show(
      NavigationRoute.navigatorKey.currentContext,
      subtitle: 'loading',
      style: SweetAlertStyle.loading,
    );
  }


  // void errorResponseAlert(APIStatus apiStatus) {
  //   if (apiStatus == APIStatus.UNAUTHRIZE) {
  //     throw UnauthorisedException();
  //   } else if (apiStatus == APIStatus.CONFLICT) {
  //     throw ConflitException();
  //   } else if (apiStatus == APIStatus.NOCONTENT) {
  //     throw NoContentException();
  //   } else if (apiStatus == APIStatus.BAD_REQUEST) {
  //     throw BadRequestException();
  //   } else {
  //     throw InternalServerException();
  //   }
  // }
}
