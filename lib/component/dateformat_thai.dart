import 'package:intl/intl.dart';
import 'package:buddhist_datetime_dateformat/buddhist_datetime_dateformat.dart';

String dateTimeToThai(DateTime dateTime) {
  return DateFormat('EEEE d MMMM y HH:mm น.')
      .formatInBuddhistCalendarThai(dateTime.toLocal());
}
