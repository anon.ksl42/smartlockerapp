import 'package:flutter/material.dart';

class TextInformation extends StatelessWidget {
  final String detail;
  final Color color;
  final double fontsize;

  TextInformation(
      {@required this.detail, this.fontsize = 20, this.color = Colors.black});
  @override
  Widget build(BuildContext context) {
    return Text(
      detail,
      style: TextStyle(fontSize: fontsize, color: color),
    );
  }
}
