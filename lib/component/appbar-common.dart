import 'package:flutter/material.dart';

class AppbarCommon extends StatelessWidget {
  final String title;
  final Function function;
  final String functionLabel;

  const AppbarCommon(
      {Key key,
      @required this.title,
      this.function,
      this.functionLabel = 'ย้อนกลับ'})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      title: Text(title, style: TextStyle(fontSize: 30 )),
      actions: [
        function != null
            ? TextButton(
                onPressed: () {
                  function();
                },
                child: Text(
                  functionLabel,
                  style: TextStyle(fontSize: 20 , color: Colors.black),
                ),
              )
            : Text(''),
      ],
    );
  }
}
