import 'package:SmartLockerApp/component/textinput-border.dart';
import 'package:flutter/material.dart';

class TextInputTemplate extends StatelessWidget {
  final TextEditingController controller;
  final String errorTextPattern;
  final String errorTextInvalid;
  final String labelText;
  final IconData icon;
  final bool validatePattern;
  final bool validatevalid;
  final bool obscureText;
  final int versionBorder;
  final TextInputType keyboardType;
  final String hintText;

  const TextInputTemplate({
    this.obscureText = false,
    this.versionBorder = 1,
    this.keyboardType = TextInputType.text,
    @required this.controller,
    @required this.errorTextPattern,
    @required this.errorTextInvalid,
    @required this.labelText,
    @required this.icon,
    @required this.validatePattern,
    @required this.validatevalid,
    this.hintText = '',
  });

  @override
  Widget build(BuildContext context) {
    InputBorder _enableBorder;
    InputBorder _focusedBorder;
    InputBorder _errorBorder;
    if (versionBorder == 1) {
      _enableBorder = enableBorder();
      _focusedBorder = focusedBorder(context);
      _errorBorder = errorBorder();
    } else if (versionBorder == 2) {
      _enableBorder = formEnableBorder();
      _focusedBorder = formFocusedBorder(context);
      _errorBorder = formEnableBorder();
    }
    return Container(
      height: 100,
      child: TextField(
        keyboardType: keyboardType,
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
          enabledBorder: _enableBorder,
          focusedErrorBorder: _focusedBorder,
          focusedBorder: _focusedBorder,
          errorBorder: _errorBorder,
          hintText: hintText,
          errorText: validatevalid
              ? errorTextInvalid
              : validatePattern
                  ? errorTextPattern
                  : null,
          labelText: labelText,
          labelStyle: TextStyle(fontSize: 20),
          prefixIcon: icon == null
              ? null
              : Icon(
                  icon,
                  color: Colors.black,
                  size: 30,
                ),
        ),
      ),
    );
  }
}
