import 'package:flutter/material.dart';

class DividerCustom extends StatelessWidget {
  final double width;
  final double endIndent;

  const DividerCustom({Key key, this.width = 200, this.endIndent = 0})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: Divider(
        thickness: 2,
        endIndent: endIndent,
      ),
    );
  }
}
