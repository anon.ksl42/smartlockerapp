class TextInputPattern {
  static final RegExp emailPattern = RegExp(r"@(?!.*?\.\.)[^@]+$");
  static final phonePattern = RegExp(r'(^(0)[0-9]{9}$)');
  static final otpPattern = RegExp(r'^[0-9]{6}$');
  static final idCardPattern = RegExp(r'/^[0-9]\d+$/');
  static final cvvpattern = RegExp(r'^[0-9]{3}$');
  static final expDatePattern = RegExp("^(0[0-9]|1[0-2])/[2-4][0-9]{3}");
  static final mastercardPattern = RegExp(
      "^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))");
  static final visaPattern = RegExp("^4[0-9]{12}(?:[0-9]{3})?");
}
