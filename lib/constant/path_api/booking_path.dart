import 'package:SmartLockerApp/data/global/global.dart';

class BookingPathAPI {
  static String getAllBookingByAccountIdPath() {
    return '/booking/accountid?accountId=${AccountGlobal.getAccountId()}';
  }

  static String getAllBookingByBookingIdPath(int bookingId) {
    return '/booking/bookingId?bookingId=$bookingId';
  }

  static const BOOKING = '/booking';
  static const BOOKING_TRANSFER_CANCEL = '/booking/transfer/cancel';
  static const BOOKING_TRANSFER = '/booking/transfer';
}
