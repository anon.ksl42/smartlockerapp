import 'package:SmartLockerApp/data/global/global.dart';

class NotificationPathAPI {
  static const String EVENT_TYPE = '/notification/eventtype';
  static const String NOTIFICATION = '/notification';

  static String getNotificationPath(int page, int perPage) {
    return '/notification?page=$page&perpage=$perPage&accountId=${AccountGlobal.getAccountId()}';
  }
}
