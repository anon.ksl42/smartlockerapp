import 'package:flutter/material.dart';

class ServiceRatePathAPI {
  static const String RATE_TYPE = '/locker/type'; //wrong path in mind
  static const String SERVICE_CHARGE =
      '/locker/servicecharge'; //wrong path in mind

  static String getServiceChargeByLockerRoomIdAndTypeIdPath(
      {@required int lkRoomId, @required int typeId}) {
    return '/locker/servicecharge/lockerroomid?lkroomId=$lkRoomId&typeId=$typeId';
  }
}
