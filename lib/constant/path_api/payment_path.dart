import 'package:SmartLockerApp/data/global/account_global.dart';

class PaymentPathAPi {
  static String getAllPaymentInfoByAccountIdPath() {
    return '/paymentinfo/${AccountGlobal.getAccountId()}';
  }

  static const String PAYMENT = '/paymentinfo/payment';
  static const String PAYMENT_INFO = '/paymentinfo';
}
