class LocationPathAPi {
  static const String LOCATION = '/locations';
  static String getLocationByLocationIdPath(int locationId) {
    return '/locations/$locationId';
  }
}
