class LockerPathAPI {
  static String getLockerByLocateIdPath(int locateId) {
    return '/locker/$locateId';
  }

  static String getAllLockerRoomByLockerId(int lockerId) {
    return '/locker/lockerroom/$lockerId';
  }
}
