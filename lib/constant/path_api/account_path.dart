class AccountPathAPI {
  static const String ACCOUNT_LOGIN = '/account/login';
  static const String ACCOUNT_REGISTER = '/account/register';
  static const String ACCOUNT = '/account';
  static String getAccountByEmailPath(String email) {
    return '/account/email/?email=$email';
  }

  static String getAccountByResBookingIdPath(int resBookingId) {
    return '/booking/transbookingid?bookingId=$resBookingId';
  }

  static String getAccountByTransfersBookingIdPath(int bookingId) {
    return '/booking/resbookingid?bookingId=$bookingId';
  }

  static String getAcccountByAccountCodePath(String accountCode) {
    return '/account/accountcode?accountcode=$accountCode';
  }
}
