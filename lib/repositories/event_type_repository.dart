import 'package:SmartLockerApp/repositories/base_repository.dart';
import 'package:http/http.dart' as http;

class EventTypeRepository extends BaseRepository{
  Future<http.Response> getAllEventType() async{
    String path = '/notification/eventtype';
    var result = await httpGet(path:path).timeout(Duration(seconds: 15));
    return result;
  }
}