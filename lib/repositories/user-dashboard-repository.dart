import 'package:SmartLockerApp/repositories/base_repository.dart';
import 'package:http/http.dart';

class UserDashboardRepository extends BaseRepository{
  Future<Response> getUserDashboard(int accountId , String rangeType) async{
    String path = '/dashboardUser?accountId=$accountId&rangeType=$rangeType';
    var result = await httpGet(path:path).timeout(Duration(seconds: 15));
    return result;
  }
}