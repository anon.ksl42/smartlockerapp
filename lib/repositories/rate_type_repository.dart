import 'package:SmartLockerApp/repositories/base_repository.dart';
import 'package:http/http.dart' as http;

class RateTypeRepository extends BaseRepository{
  Future<http.Response> getAllRateType() async{
    String path = '/locker/type';
    var result = await httpGet(path:path).timeout(Duration(seconds: 15));
    return result;
  }
}