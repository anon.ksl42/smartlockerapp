import 'dart:convert';

import 'package:SmartLockerApp/models/account_model.dart';
import 'package:SmartLockerApp/repositories/base_repository.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AccountRepository extends BaseRepository {
  Future<http.Response> loginAccount({@required AccountModel loginForm}) async {
    String path = '/account/login';
    String formJson = json.encode(loginForm.toJson());
    var result = await httpPost(path: path, body: formJson)
        .timeout(Duration(seconds: 15));
    return result;
  }
}
