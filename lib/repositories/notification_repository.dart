import 'package:SmartLockerApp/data/global/account_global.dart';
import 'base_repository.dart';
import 'package:http/http.dart' as http;

class NotificationRepository extends BaseRepository{
  Future<http.Response> getAllNotificationByAccountId() async{
    String path = '/notification?page=1&perpage=20&accountId=${AccountGlobal.getAccountId()}';
    var result = await httpGet(path:path).timeout(Duration(seconds: 15));
    return result;
  }
}