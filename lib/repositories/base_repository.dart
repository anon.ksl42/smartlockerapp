import 'dart:async';
import 'dart:io';

import 'package:SmartLockerApp/component/exception.dart';
import 'package:SmartLockerApp/data/localstorage/account_localstorage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:retry/retry.dart';

class BaseRepository {
  final r = RetryOptions(maxAttempts: 4);
  AccountLocalStorage accountLocalStorage = AccountLocalStorage();
  String baseUri = 'https://webservice-locker.ml/api';
  // String baseUri = 'https://fc40a115505b.ngrok.io//api';

  Future<http.Response> httpGet({@required String path}) async {
    await accountLocalStorage.init();
    Map<String, String> header = {
      "Content-type": "application/json; charset=utf-8 ",
      "Authorization": "Bearer ${accountLocalStorage.getToken() ?? ''}"
    };
    return await r.retry(() async {
      var response = await http.get(
        '$baseUri$path',
        headers: header,
      );
      if (response.statusCode == 500)
        throw InternalServerException();
      else
        return response;
    },
        retryIf: (e) =>
            e is SocketException ||
            e is TimeoutException ||
            e is InternalServerException);
  }

  Future<http.Response> httpPost(
      {@required String path, @required String body}) async {
    await accountLocalStorage.init();
    Map<String, String> header = {
      "Content-type": "application/json; charset=utf-8 ",
      "Authorization": "Bearer ${accountLocalStorage.getToken() ?? ''}"
    };
    return await r.retry(() async {
      var response =
          await http.post('$baseUri$path', headers: header, body: body);
      if (response.statusCode == 500)
        throw TimeoutException("");
      else
        return response;
    }, retryIf: (e) => e is SocketException || e is TimeoutException);
  }

  Future<http.Response> httpPut(
      {@required String path, @required String body}) async {
    await accountLocalStorage.init();
    Map<String, String> header = {
      "Content-type": "application/json; charset=utf-8 ",
      "Authorization": "Bearer ${accountLocalStorage.getToken() ?? ''}"
    };
    return await r.retry(() async {
      var response =
          await http.put('$baseUri$path', headers: header, body: body);
      if (response.statusCode == 500)
        throw TimeoutException("");
      else
        return response;
    }, retryIf: (e) => e is SocketException || e is TimeoutException);
  }
}
