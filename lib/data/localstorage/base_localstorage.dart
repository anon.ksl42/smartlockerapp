import 'package:shared_preferences/shared_preferences.dart';

class BaseLocalStorage {
  SharedPreferences localStorage;
  Future init() async {
    localStorage = await SharedPreferences.getInstance();
  }
}