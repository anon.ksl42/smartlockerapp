import 'package:SmartLockerApp/data/localstorage/base_localstorage.dart';

class AccountLocalStorage extends BaseLocalStorage {

  int getAccountId() {
    return localStorage.getInt('accountId');
  }

  void setAccountId(int accountId) {
    localStorage.setInt('accountId', accountId);
  }

  String getAccountCode() {
    return localStorage.getString('accountCode');
  }

  void setAccountCode(String accountCode) {
    localStorage.setString('accountCode', accountCode);
  }

  String getFirstName() {
    return localStorage.getString('firstName');
  }

  void setFirstName(String firstName) {
    localStorage.setString('firstName', firstName);
  }

  String getLastName() {
    return localStorage.getString('lastName');
  }

  void setLastName(String lastName) {
    localStorage.setString('lastName', lastName);
  }

  String getFullName() {
    return getFirstName() + ' ' + getLastName();
  }

  String getToken() {
    return localStorage.getString('token');
  }

  void setToken(String token) {
    localStorage.setString('token', token);
  }

  void logoutRemoveToken() {
    localStorage.remove('token');
    localStorage.remove('firstName');
    localStorage.remove('lastName');
    localStorage.remove('accountCode');
    localStorage.remove('accountId');
  }
}
