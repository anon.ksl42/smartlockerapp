import 'package:SmartLockerApp/models/models.dart';

class EventType {
  static Map<String, int> eventTypeForData;
  static String booking = 'การจองล็อคเกอร์';
  static String cancelBooking = 'การยกเลิกจองล็อคเกอร์';
  static String transfer = 'การทำเรื่องโอนสิทธิ์การใช้งาน';
  static String cancelTransfer = 'การยกเลิกการโอนสิทธิ์การใช้งาน';
  static String approveTransfer = 'การยืนยันการใช้งาน';
  static String rejectTransfer = 'การปฏิเสธการใช้งาน';
  static String almostExpireUse = 'ใกล้หมดเวลาการใช้งาน';

  static void initial(List<EventTypeModel> eventTypeList){
    EventType.eventTypeForData = new Map<String, int>();
    for (EventTypeModel eventType in eventTypeList) {
      EventType.eventTypeForData[eventType.eventName] = eventType.eventId;
    }
  }
}
