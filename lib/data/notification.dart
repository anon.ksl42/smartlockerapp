import 'package:SmartLockerApp/component/component.dart';
import 'package:SmartLockerApp/constant/constant.dart';
import 'package:SmartLockerApp/data/global/account_global.dart';
import 'package:SmartLockerApp/models/booking_model.dart';
import 'package:SmartLockerApp/models/notification_model.dart';
import 'package:SmartLockerApp/route/navigation.dart';
import 'package:SmartLockerApp/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'datas.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class MyNotification {
  static String message;
  static bool isUpdate = false;
  static List<NotificationModel> listAllNoti = [];

  static initStateNoti() {
    message = "No message.";
    var initializationSettingsAndroid = AndroidInitializationSettings('test');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) async {
      print("onDidReceiveLocalNotification called.");
    });
    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
      // when user tap on notification.
      print("onSelectNotification called.");
      return payload;
    });
  }

  static sendNotification(String header, String content, int id) async {
    isUpdate = true;
    var androidPlatformChannelSpecifics = AndroidNotificationDetails('10000',
        'FLUTTER_NOTIFICATION_CHANNEL', 'FLUTTER_NOTIFICATION_CHANNEL_DETAIL',
        importance: Importance.max, priority: Priority.high);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        id, header, content, platformChannelSpecifics,
        payload: 'I just haven\'t Met You Yet');
  }

  static bookingNotification(BookingModel bookingDetail) async {
    initStateNoti();
    sendNotification(EventType.booking, 'ทำเรื่องจองเรียบร้อย', 111);

    NotificationModel noti = new NotificationModel();
    noti.eventId = EventType.eventTypeForData[EventType.booking];
    noti.accountId = AccountGlobal.getAccountId();
    noti.description =
        "ทำเรื่องจองช่องล็อคเกอร์ ${bookingDetail.lkRoomCode} เรียบร้อย";
    await NotificationService.addNotification(noti).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }

  static cancelBookingNotification(BookingModel booking) async {
    initStateNoti();
    sendNotification(
        'การยกเลิกจองล็อคเกอร์',
        'ระบบทำการยกเลิกการจองอัตโนมัติของช่องล็อคเกอร์ ${booking.lkRoomCode ?? ''} เรียบร้อย',
        111);
    NotificationModel noti = new NotificationModel();
    noti.eventId = EventType.eventTypeForData[EventType.cancelBooking];
    noti.accountId = AccountGlobal.getAccountId();
    noti.description =
        "ระบบทำการยกเลิกการจองอัตโนมัติของช่องล็อคเกอร์ ${booking.lkRoomCode ?? ''} เรียบร้อย ";
    await NotificationService.addNotification(noti)
        .then((_) => isUpdate = true)
        .catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }

  static cancelTransferNotification(BookingModel bookingModel) async {
    initStateNoti();
    sendNotification(
        'การยกเลิกการโอนสิทธิ์การใช้งาน',
        'การยกเลิกการโอนสิทธิ์การใช้งานล็อคเกอร์ ${bookingModel.lkRoomCode} เรียบร้อย',
        111);
    NotificationModel noti = new NotificationModel();
    noti.eventId = EventType.eventTypeForData[EventType.cancelTransfer];
    noti.accountId = AccountGlobal.getAccountId();
    noti.description =
        "การยกเลิกการโอนสิทธิ์การใช้งานล็อคเกอร์ ${bookingModel.lkRoomCode} เรียบร้อย";
    await NotificationService.addNotification(noti)
        .then((_) => isUpdate = true)
        .catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }

  static transferNotification() async {
    sendNotification(EventType.transfer, 'ทำเรื่องโอนสิทธิ์เรียบร้อย', 111);
    NotificationModel noti = new NotificationModel();
    noti.eventId = EventType.eventTypeForData[EventType.transfer];
    noti.accountId = AccountGlobal.getAccountId();
    noti.description = "ทำเรื่องโอนสิทธิ์เรียบร้อย";
    await NotificationService.addNotification(noti).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }

  static updateTransferNotification(
      {@required String textHeader,
      @required String textDetail,
      @required int eventId}) async {
    sendNotification(textHeader, '$textDetail เรียบร้อย', 111);
    NotificationModel noti = new NotificationModel();
    noti.eventId = eventId;
    noti.accountId = AccountGlobal.getAccountId();
    noti.description = '$textDetail เรียบร้อย';
    await NotificationService.addNotification(noti).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }

  static almostNotification(BuildContext context,
      {@required BookingModel bookingHistory}) async {
    initStateNoti();
    sendNotification(
        EventType.almostExpireUse,
        'เหลือเวลา 30 นาที สำหรับช่อง ${bookingHistory.lkRoomCode}',
        bookingHistory.lkRoomId);
    NotificationModel noti = new NotificationModel();
    noti.eventId = EventType.eventTypeForData[EventType.almostExpireUse];
    noti.accountId = AccountGlobal.getAccountId();
    noti.description =
        "เหลือเวลา 30 นาที สำหรับช่อง ${bookingHistory.lkRoomCode}";
    await NotificationService.addNotification(noti).catchError((error) {
      if (error == APIStatus.UNAUTHRIZE) {
        errorAlert(
            wording: 'หมดอายุการใช้งาน กรุณาเข้าสู่ระบบอีกครั้ง',
            function: () => NavigationRoute.navigateAndRemoveUntil(LoginRoute));
      } else if (error == APIStatus.TIME_OUT) {
        errorAlert(
          wording: 'ระบบแจ้งเตือนมีปัญหาเนื่องจากขาดการเชื่อมต่อ',
        );
      } else if (error == APIStatus.SYETEM_ERROR) {
        errorAlert(
          wording: 'ระบบขัดข้องไม่สามารถดำเนินการได้ในขณะนี้',
        );
      }
    });
  }
}
