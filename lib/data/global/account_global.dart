import 'package:SmartLockerApp/data/global/baseglobal.dart';

class AccountGlobal {
  static int getAccountId() {
    return Global.localStorage.getInt('accountId');
  }

  static void setAccountId(int accountId) {
    Global.localStorage.setInt('accountId', accountId);
  }

  static String getAccountCode() {
    return Global.localStorage.getString('accountCode');
  }

  static void setAccountCode(String accountCode) {
    Global.localStorage.setString('accountCode', accountCode);
  }

  static String getFirstName() {
    return Global.localStorage.getString('firstName');
  }

  static void setFirstName(String firstName) {
    Global.localStorage.setString('firstName', firstName);
  }

  static String getLastName() {
    return Global.localStorage.getString('lastName');
  }

  static void setLastName(String lastName) {
    Global.localStorage.setString('lastName', lastName);
  }

  static String getFullName() {
    return getFirstName() + ' ' + getLastName();
  }

  static String getToken() {
    return Global.localStorage.getString('token');
  }

  static void setToken(String token) {
    Global.localStorage.setString('token', token);
  }

  static void logoutRemoveToken() {
    Global.localStorage.remove('token');
    Global.localStorage.remove('firstName');
    Global.localStorage.remove('lastName');
    Global.localStorage.remove('accountCode');
    Global.localStorage.remove('accountId');
  }
}
