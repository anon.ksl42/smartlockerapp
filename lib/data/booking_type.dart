import 'package:SmartLockerApp/models/models.dart';

class RateType {
  static Map<String, int> bookingTypeForData;

  static void initial(List<RateTypeModel> rateTypeList ) async {
      RateType.bookingTypeForData = new Map<String, int>();
      for (RateTypeModel bookingType in rateTypeList) {
        RateType.bookingTypeForData[bookingType.typeName] =
            bookingType.typeId;
      }
  }

  static String findTypeNameByTypeId(int typeId) {
    String typeName = bookingTypeForData.keys
        .firstWhere((key) => bookingTypeForData[key] == typeId);
    return typeName;
  }
}
