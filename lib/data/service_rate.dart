import 'package:SmartLockerApp/models/models.dart';
import 'package:SmartLockerApp/services/service.dart';

class ServiceRate {
  static List<ServiceRateModel> serviceRateList;
  static List<ServiceRateModel> serviceRateNormalTypeList;
  static List<ServiceRateModel> serviceRateAllDayTypeList;
  static Future<void> initialServiceRateData() async {
    if (serviceRateList == null) {
      serviceRateList = await ServiceRateService.getAllServiceRate();
      serviceRateList.sort((a, b) => a.srid.compareTo(b.srid));
      serviceRateNormalTypeList = serviceRateList
          .where((element) => element.typeName == 'จองปกติ')
          .toList();
      serviceRateAllDayTypeList = serviceRateList
          .where((element) => element.typeName == 'จองเหมา 24 ชั่วโมง')
          .toList();
      serviceRateNormalTypeList.sort((a, b) => a.srid.compareTo(b.srid));
      serviceRateAllDayTypeList.sort((a, b) => a.srid.compareTo(b.srid));
    }
  }
}
